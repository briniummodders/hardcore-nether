package mc.Mitchellbrine.HardcoreNether.common.block;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import mc.Mitchellbrine.HardcoreNether.dungeon.EntityScientist;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.Icon;
import net.minecraft.world.World;

public class BlockPhonyChest extends Block
{
    @SideOnly(Side.CLIENT)
    private Icon furnaceIconFront;

    public BlockPhonyChest(int par1)
    {
        super(par1, Material.wood);
    }

    @SideOnly(Side.CLIENT)

    /**
     * When this method is called, your block should register all the icons it needs with the given IconRegister. This
     * is the only chance you get to register icons.
     */
    public void registerIcons(IconRegister par1IconRegister)
    {
        this.blockIcon = par1IconRegister.registerIcon("hardcoreNether:chest");
        this.furnaceIconFront = par1IconRegister.registerIcon("hardcoreNether:chest_front");
    }

    /**
     * Called upon block activation (right click on the block.)
     */
    public boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9)
    {
        if (!par1World.isRemote)
        {
            par1World.setBlockToAir(par2, par3, par4);
            EntityScientist scientist = new EntityScientist(par1World);
            scientist.setLocationAndAngles((double)par2, (double)par3, (double)par4, 0.0F, 0.0F);
            par1World.spawnEntityInWorld(scientist);
            return true;
        }
        else
        {
            return false;
        }
    }
}

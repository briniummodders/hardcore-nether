package mc.Mitchellbrine.HardcoreNether.common.block;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class CorruptedOre extends Item
{
    public CorruptedOre(int par1)
    {
        super(par1);
    }

    /**
     * Called whenever this item is equipped and the right mouse button is pressed. Args: itemStack, world, entityPlayer
     */
    public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
        if (par2World.getBlockId(par3EntityPlayer.getPlayerCoordinates().posX, par3EntityPlayer.getPlayerCoordinates().posY - 1, par3EntityPlayer.getPlayerCoordinates().posZ) == 2)
        {
            par2World.setBlock(par3EntityPlayer.getPlayerCoordinates().posX, par3EntityPlayer.getPlayerCoordinates().posY - 1, par3EntityPlayer.getPlayerCoordinates().posZ, MainMod.corruptedGrass.blockID);
        }

        return par1ItemStack;
    }
}

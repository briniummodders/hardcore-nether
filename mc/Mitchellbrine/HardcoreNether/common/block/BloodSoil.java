package mc.Mitchellbrine.HardcoreNether.common.block;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import java.util.Random;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.world.World;

public class BloodSoil extends Block
{
    @SideOnly(Side.CLIENT)
    private Icon field_94393_a;
    @SideOnly(Side.CLIENT)
    private Icon field_94392_b;

    public BloodSoil(int par1, Material par2Material)
    {
        super(par1, par2Material);
    }

    /**
     * From the specified side and block metadata retrieves the blocks texture. Args: side, metadata
     */
    public Icon getIcon(int par1, int par2)
    {
        return par1 == 0 ? this.field_94392_b : (par1 == 1 ? this.field_94393_a : this.blockIcon);
    }

    /**
     * When this method is called, your block should register all the icons it needs with the given IconRegister. This
     * is the only chance you get to register icons.
     */
    public void registerIcons(IconRegister par1IconRegister)
    {
        this.blockIcon = par1IconRegister.registerIcon("hardcoreNether:corruptedGrass_side");
        this.field_94393_a = par1IconRegister.registerIcon("hardcoreNether:corruptedGrass_top");
        this.field_94392_b = par1IconRegister.registerIcon("dirt");
    }

    /**
     * Ticks the block if it's been scheduled
     */
    public void updateTick(World par1World, int par2, int par3, int par4, Random par5Random)
    {
        if (!par1World.isRemote)
        {
            for (int l = 0; l < 4; ++l)
            {
                int i1 = par2 + par5Random.nextInt(3) - 1;
                int j1 = par3 + par5Random.nextInt(5) - 3;
                int k1 = par4 + par5Random.nextInt(3) - 1;
                par1World.getBlockId(i1, j1 + 1, k1);

                if (par1World.getBlockId(i1, j1, k1) == Block.grass.blockID && par1World.getBlockLightValue(i1, j1 + 1, k1) <= 4 && par1World.getBlockLightOpacity(i1, j1 + 1, k1) >= 2)
                {
                    par1World.setBlock(i1, j1, k1, MainMod.corruptedGrass.blockID);
                }
            }
        }
    }

    /**
     * Called upon block activation (right click on the block.)
     */
    public boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9)
    {
        if (!par1World.isRemote && par1World.getBlockId(par2, par3, par4) == MainMod.corruptedGrass.blockID && par5EntityPlayer.inventory.getCurrentItem() == null)
        {
            EntityItem ei = new EntityItem(par1World, (double)par2, (double)(par3 + 1), (double)par4, new ItemStack(MainMod.blood, 1));
            par1World.spawnEntityInWorld(ei);
            par1World.setBlock(par2, par3, par4, Block.grass.blockID);
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Returns the ID of the items to drop on destruction.
     */
    public int idDropped(int par1, Random par2Random, int par3)
    {
        return Block.dirt.blockID;
    }
}

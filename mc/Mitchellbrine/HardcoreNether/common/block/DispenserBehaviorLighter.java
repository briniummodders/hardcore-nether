package mc.Mitchellbrine.HardcoreNether.common.block;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.block.BlockDispenser;
import net.minecraft.dispenser.IBehaviorDispenseItem;
import net.minecraft.dispenser.IBlockSource;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;

public final class DispenserBehaviorLighter implements IBehaviorDispenseItem
{
    private boolean field_96466_b = true;

    protected ItemStack dispenseStack(IBlockSource par1IBlockSource, ItemStack par2ItemStack)
    {
        EnumFacing enumfacing = BlockDispenser.getFacing(par1IBlockSource.getBlockMetadata());
        World world = par1IBlockSource.getWorld();
        int i = par1IBlockSource.getXInt() + enumfacing.getFrontOffsetX();
        int j = par1IBlockSource.getYInt() + enumfacing.getFrontOffsetY();
        int k = par1IBlockSource.getZInt() + enumfacing.getFrontOffsetZ();

        if (world.getBlockId(i, j, k) == MainMod.corruptTorchOff.blockID)
        {
            world.setBlock(i, j, k, MainMod.corruptTorchOn.blockID);

            if (par2ItemStack.attemptDamageItem(1, world.rand))
            {
                par2ItemStack.stackSize = 0;
            }
        }
        else if (world.getBlockId(i, j, k) == MainMod.corruptTorchOn.blockID)
        {
            world.setBlock(i, j, k, MainMod.corruptTorchOff.blockID);

            if (par2ItemStack.attemptDamageItem(-1, world.rand))
            {
                par2ItemStack.stackSize = 0;
            }
        }
        else if (world.getBlockId(i, j, k) == MainMod.DemonicTNT.blockID)
        {
            MainMod.DemonicTNT.onBlockDestroyedByPlayer(world, i, j, k, 1);
            world.setBlockToAir(i, j, k);
        }
        else
        {
            this.field_96466_b = false;
        }

        return par2ItemStack;
    }

    protected void playDispenseSound(IBlockSource par1IBlockSource)
    {
        if (this.field_96466_b)
        {
            par1IBlockSource.getWorld().playAuxSFX(1000, par1IBlockSource.getXInt(), par1IBlockSource.getYInt(), par1IBlockSource.getZInt(), 0);
        }
        else
        {
            par1IBlockSource.getWorld().playAuxSFX(1001, par1IBlockSource.getXInt(), par1IBlockSource.getYInt(), par1IBlockSource.getZInt(), 0);
        }
    }

    /**
     * Dispenses the specified ItemStack from a dispenser.
     */
    public ItemStack dispense(IBlockSource iblocksource, ItemStack itemstack)
    {
        return this.dispenseStack(iblocksource, itemstack);
    }
}

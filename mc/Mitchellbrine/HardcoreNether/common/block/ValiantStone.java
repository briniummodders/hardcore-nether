package mc.Mitchellbrine.HardcoreNether.common.block;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ValiantStone extends Item
{
    public ValiantStone(int par1)
    {
        super(par1);
    }

    /**
     * Called whenever this item is equipped and the right mouse button is pressed. Args: itemStack, world, entityPlayer
     */
    public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
        par3EntityPlayer.setItemInUse(par1ItemStack, this.getMaxItemUseDuration(par1ItemStack));
        MainMod.ValiantActivate = 1;
        return par1ItemStack;
    }
}

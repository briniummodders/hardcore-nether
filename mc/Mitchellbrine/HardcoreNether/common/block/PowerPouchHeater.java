package mc.Mitchellbrine.HardcoreNether.common.block;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import mc.Mitchellbrine.HardcoreNether.common.tileentity.TileEntityHeater;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class PowerPouchHeater extends BlockContainer
{
    public PowerPouchHeater(int id)
    {
        super(id, Material.iron);
        this.setCreativeTab(MainMod.HardcoreTab);
        this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.375F, 1.0F);
    }

    /**
     * Returns a new instance of a block's tile entity class. Called on placing the block.
     */
    public TileEntity createNewTileEntity(World world)
    {
        return new TileEntityHeater();
    }

    /**
     * The type of render function that is called for this block
     */
    public int getRenderType()
    {
        return -1;
    }

    /**
     * Is this block (a) opaque and (b) a full 1m cube?  This determines whether or not to render the shared face of two
     * adjacent blocks and also whether the player can attach torches, redstone wire, etc to this block.
     */
    public boolean isOpaqueCube()
    {
        return false;
    }

    /**
     * If this block doesn't render as an ordinary block it will return False (examples: signs, buttons, stairs, etc)
     */
    public boolean renderAsNormalBlock()
    {
        return false;
    }

    /**
     * When this method is called, your block should register all the icons it needs with the given IconRegister. This
     * is the only chance you get to register icons.
     */
    public void registerIcons(IconRegister icon)
    {
        this.blockIcon = icon.registerIcon("hardcoreNether:PPHIcon");
    }

    /**
     * Called upon block activation (right click on the block.)
     */
    public boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9)
    {
        if (par1World.getBlockId(par2, par3 - 1, par4) == Block.netherrack.blockID)
        {
            par1World.setBlock(par2, par3 - 1, par4, MainMod.flamingrack.blockID);
        }

        if (par1World.getBlockId(par2, par3 - 1, par4 - 1) == Block.netherrack.blockID)
        {
            par1World.setBlock(par2, par3 - 1, par4 - 1, MainMod.flamingrack.blockID);
        }

        if (par1World.getBlockId(par2, par3 - 1, par4 + 1) == Block.netherrack.blockID)
        {
            par1World.setBlock(par2, par3 - 1, par4 + 1, MainMod.flamingrack.blockID);
        }

        if (par1World.getBlockId(par2 - 1, par3 - 1, par4 - 1) == Block.netherrack.blockID)
        {
            par1World.setBlock(par2 - 1, par3 - 1, par4 - 1, MainMod.flamingrack.blockID);
        }

        if (par1World.getBlockId(par2 - 1, par3 - 1, par4) == Block.netherrack.blockID)
        {
            par1World.setBlock(par2 - 1, par3 - 1, par4, MainMod.flamingrack.blockID);
        }

        if (par1World.getBlockId(par2 + 1, par3 - 1, par4) == Block.netherrack.blockID)
        {
            par1World.setBlock(par2 + 1, par3 - 1, par4, MainMod.flamingrack.blockID);
        }

        if (par1World.getBlockId(par2 + 1, par3 - 1, par4 + 1) == Block.netherrack.blockID)
        {
            par1World.setBlock(par2 + 1, par3 - 1, par4 + 1, MainMod.flamingrack.blockID);
        }

        if (par1World.getBlockId(par2 + 1, par3 - 1, par4 - 1) == Block.netherrack.blockID)
        {
            par1World.setBlock(par2 + 1, par3 - 1, par4 - 1, MainMod.flamingrack.blockID);
        }

        if (par1World.getBlockId(par2 - 1, par3 - 1, par4 + 1) == MainMod.flamingrack.blockID)
        {
            par1World.setBlock(par2 - 1, par3 - 1, par4 + 1, MainMod.flamingrack.blockID);
        }

        if (!par1World.isRemote)
        {
            par5EntityPlayer.addStat(MainMod.heaterAchievement, 1);

            if (par1World.getBlockId(par2, par3 - 1, par4) == Block.waterStill.blockID)
            {
                par1World.setBlock(par2, par3 - 1, par4, Block.lavaStill.blockID);
            }

            if (par1World.getBlockId(par2, par3 - 1, par4 - 1) == Block.waterStill.blockID)
            {
                par1World.setBlock(par2, par3 - 1, par4 - 1, Block.lavaStill.blockID);
            }

            if (par1World.getBlockId(par2, par3 - 1, par4 + 1) == Block.waterStill.blockID)
            {
                par1World.setBlock(par2, par3 - 1, par4 + 1, Block.lavaStill.blockID);
            }

            if (par1World.getBlockId(par2 - 1, par3 - 1, par4 - 1) == Block.waterStill.blockID)
            {
                par1World.setBlock(par2 - 1, par3 - 1, par4 - 1, Block.lavaStill.blockID);
            }

            if (par1World.getBlockId(par2 - 1, par3 - 1, par4) == Block.waterStill.blockID)
            {
                par1World.setBlock(par2 - 1, par3 - 1, par4, Block.lavaStill.blockID);
            }

            if (par1World.getBlockId(par2 + 1, par3 - 1, par4) == Block.waterStill.blockID)
            {
                par1World.setBlock(par2 + 1, par3 - 1, par4, Block.lavaStill.blockID);
            }

            if (par1World.getBlockId(par2 + 1, par3 - 1, par4 + 1) == Block.waterStill.blockID)
            {
                par1World.setBlock(par2 + 1, par3 - 1, par4 + 1, Block.lavaStill.blockID);
            }

            if (par1World.getBlockId(par2 + 1, par3 - 1, par4 - 1) == Block.waterStill.blockID)
            {
                par1World.setBlock(par2 + 1, par3 - 1, par4 - 1, Block.lavaStill.blockID);
            }

            if (par1World.getBlockId(par2 - 1, par3 - 1, par4 + 1) == Block.waterStill.blockID)
            {
                par1World.setBlock(par2 - 1, par3 - 1, par4 + 1, Block.lavaStill.blockID);
            }
        }

        return false;
    }
}

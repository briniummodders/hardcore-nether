package mc.Mitchellbrine.HardcoreNether.common.block;

import java.util.Random;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.block.BlockTorch;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class CorruptTorch extends BlockTorch
{
    private int tickRate;

    public CorruptTorch(int par1)
    {
        super(par1);
    }

    /**
     * Called whenever the block is added into the world. Args: world, x, y, z
     */
    public void onBlockAdded(World par1World, int par2, int par3, int par4)
    {
        this.tickRate = 60;
    }

    /**
     * Called upon block activation (right click on the block.)
     */
    public boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9)
    {
        ItemStack itemstack = par5EntityPlayer.inventory.getCurrentItem();

        if (!par1World.isRemote)
        {
            if (itemstack != null && this.blockID == MainMod.corruptTorchOff.blockID && itemstack.itemID == MainMod.holyLighter.itemID)
            {
                par1World.setBlock(par2, par3, par4, MainMod.corruptTorchOn.blockID);
                return true;
            }

            if (itemstack == null || itemstack.itemID != MainMod.holyLighter.itemID && this.blockID == MainMod.corruptTorchOn.blockID || itemstack.itemID != MainMod.holyLighter.itemID && this.blockID == MainMod.corruptTorchOff.blockID)
            {
                return false;
            }
        }

        return false;
    }

    /**
     * Ticks the block if it's been scheduled
     */
    public void updateTick(World par1World, int par2, int par3, int par4, Random par5Random)
    {
        if (this.tickRate >= 1)
        {
            --this.tickRate;
        }
        else
        {
            par1World.setBlock(par2, par3, par4, MainMod.corruptTorchOff.blockID);
        }
    }

    /**
     * Returns the ID of the items to drop on destruction.
     */
    public int idDropped(int par1, Random par2Random, int par3)
    {
        return par2Random.nextInt() > 80 ? MainMod.corruptTorchOn.blockID : MainMod.corruptTorchOff.blockID;
    }

    public int idPicked(int par1, Random par2Random, int par3, int par4)
    {
        return MainMod.corruptTorchOff.blockID;
    }
}

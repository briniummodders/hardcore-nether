package mc.Mitchellbrine.HardcoreNether.common.block;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class TutFurnaceRecipes
{
    private static final TutFurnaceRecipes smeltingBase = new TutFurnaceRecipes();
    private Map smeltingList = new HashMap();
    private Map experienceList = new HashMap();
    private HashMap<List<Integer>, ItemStack> metaSmeltingList = new HashMap();
    private HashMap<List<Integer>, Float> metaExperience = new HashMap();

    public static final TutFurnaceRecipes smelting()
    {
        return smeltingBase;
    }

    private TutFurnaceRecipes()
    {
        this.addSmelting(new ItemStack(Item.swordWood), new ItemStack(MainMod.hardcoreWoodSword), 0.7F);
        this.addSmelting(new ItemStack(Item.swordStone), new ItemStack(MainMod.hardcoreStoneSword), 0.7F);
        this.addSmelting(new ItemStack(Item.swordGold), new ItemStack(MainMod.hardcoreGoldSword), 0.7F);
        this.addSmelting(new ItemStack(Item.swordIron), new ItemStack(MainMod.hardcoreIronSword), 0.7F);
        this.addSmelting(new ItemStack(Item.swordDiamond), new ItemStack(MainMod.hardcoreDiamondSword), 0.7F);
        this.addSmelting(new ItemStack(MainMod.encryption1), 0, new ItemStack(MainMod.encryptedSword1), 0.7F);
        this.addSmelting(new ItemStack(MainMod.encryption1), 1, new ItemStack(MainMod.encryptedSword2), 0.7F);
        this.addSmelting(new ItemStack(MainMod.encryption1), 2, new ItemStack(MainMod.encryptedSword3), 0.7F);
    }

    public void addSmelting(ItemStack slot1, ItemStack slot3, float par3)
    {
        this.smeltingList.put(Arrays.asList(new Integer[] {Integer.valueOf(slot1.itemID)}), slot3);
        this.experienceList.put(Integer.valueOf(slot3.itemID), Float.valueOf(par3));
    }

    @Deprecated
    public ItemStack getSmeltingResult(int par1)
    {
        return (ItemStack)this.smeltingList.get(Integer.valueOf(par1));
    }

    public Map getSmeltingList()
    {
        return this.smeltingList;
    }

    @Deprecated
    public float getExperience(int par1)
    {
        return this.experienceList.containsKey(Integer.valueOf(par1)) ? ((Float)this.experienceList.get(Integer.valueOf(par1))).floatValue() : 0.0F;
    }

    public void addSmelting(ItemStack slot1, int metadata, ItemStack slot3, float experience)
    {
        this.metaSmeltingList.put(Arrays.asList(new Integer[] {Integer.valueOf(slot1.itemID), Integer.valueOf(metadata)}), slot3);
        this.metaExperience.put(Arrays.asList(new Integer[] {Integer.valueOf(slot1.itemID), Integer.valueOf(metadata)}), Float.valueOf(experience));
    }

    public ItemStack getSmeltingResult(ItemStack item)
    {
        if (item == null)
        {
            return null;
        }
        else
        {
            ItemStack ret = (ItemStack)this.metaSmeltingList.get(Arrays.asList(new Integer[] {Integer.valueOf(item.itemID), Integer.valueOf(item.getItemDamage())}));
            return ret != null ? ret : (ItemStack)this.smeltingList.get(Integer.valueOf(item.itemID));
        }
    }

    public float getExperience(ItemStack item)
    {
        if (item != null && item.getItem() != null)
        {
            float ret = item.getItem().getSmeltingExperience(item);

            if (ret < 0.0F && this.metaExperience.containsKey(Arrays.asList(new Integer[] {Integer.valueOf(item.itemID), Integer.valueOf(item.getItemDamage())})))
            {
                ret = ((Float)this.metaExperience.get(Arrays.asList(new Integer[] {Integer.valueOf(item.itemID), Integer.valueOf(item.getItemDamage())}))).floatValue();
            }

            if (ret < 0.0F && this.experienceList.containsKey(Integer.valueOf(item.itemID)))
            {
                ret = ((Float)this.experienceList.get(Integer.valueOf(item.itemID))).floatValue();
            }

            return ret < 0.0F ? 0.0F : ret;
        }
        else
        {
            return 0.0F;
        }
    }

    public Map<List<Integer>, ItemStack> getMetaSmeltingList()
    {
        return this.metaSmeltingList;
    }
}

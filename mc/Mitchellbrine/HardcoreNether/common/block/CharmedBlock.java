package mc.Mitchellbrine.HardcoreNether.common.block;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.world.World;

public class CharmedBlock extends Block
{
    public NBTTagCompound nbt;

    public CharmedBlock(int id, Material par2Material)
    {
        super(id, par2Material);
    }

    /**
     * Called upon block activation (right click on the block.)
     */
    public boolean onBlockActivated(World world, int bx, int by, int bz, EntityPlayer playerEntity, int unknown, float px, float py, float pz)
    {
        ByteArrayOutputStream bos = new ByteArrayOutputStream(8);
        DataOutputStream outputStream = new DataOutputStream(bos);

        try
        {
            outputStream.writeInt(MainMod.Special);
        }
        catch (Exception var15)
        {
            var15.printStackTrace();
        }

        Packet250CustomPayload packet = new Packet250CustomPayload();
        packet.channel = "HardcoreNether";
        packet.data = bos.toByteArray();
        packet.length = bos.size();
        Side side = FMLCommonHandler.instance().getEffectiveSide();

        if (side == Side.SERVER)
        {
            EntityPlayerMP player = (EntityPlayerMP)playerEntity;
        }
        else if (side == Side.CLIENT)
        {
            EntityClientPlayerMP player1 = (EntityClientPlayerMP)playerEntity;
            player1.sendQueue.addToSendQueue(packet);
        }

        return false;
    }
}

package mc.Mitchellbrine.HardcoreNether.common.block;

import java.util.Random;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.world.World;

public class FlamingRack extends Block
{
    public FlamingRack(int par1)
    {
        super(par1, MainMod.hardcoreMaterial);
        this.setCreativeTab(MainMod.HardcoreTab);
    }

    /**
     * Called whenever an entity is walking on top of this block. Args: world, x, y, z, entity
     */
    public void onEntityWalking(World par1World, int par2, int par3, int par4, Entity par5Entity)
    {
        ((EntityLivingBase)par5Entity).setFire(3600);
    }

    /**
     * Called right before the block is destroyed by a player.  Args: world, x, y, z, metaData
     */
    public void onBlockDestroyedByPlayer(World par1World, int par2, int par3, int par4, int par5)
    {
        MainMod.Special += 5;
    }

    /**
     * Returns the ID of the items to drop on destruction.
     */
    public int idDropped(int par1, Random par2Random, int par3)
    {
        return MainMod.flamingrack.blockID;
    }
}

package mc.Mitchellbrine.HardcoreNether.common.block;

import java.util.Random;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class AngelStone extends Block
{
    public AngelStone(int par1, Material par2Material)
    {
        super(par1, par2Material);
    }

    /**
     * Returns the ID of the items to drop on destruction.
     */
    public int idDropped(int par1, Random par2Random, int par3)
    {
        return MainMod.angelicCobble.blockID;
    }
}

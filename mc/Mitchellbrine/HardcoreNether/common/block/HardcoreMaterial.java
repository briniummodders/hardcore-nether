package mc.Mitchellbrine.HardcoreNether.common.block;

import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;

public class HardcoreMaterial extends Material
{
    public HardcoreMaterial(MapColor par1MapColor)
    {
        super(par1MapColor);
    }
}

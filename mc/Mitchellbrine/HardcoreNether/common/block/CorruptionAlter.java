package mc.Mitchellbrine.HardcoreNether.common.block;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import java.util.Random;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.Icon;
import net.minecraft.world.World;

public class CorruptionAlter extends Block
{
    private static int isFueled;
    @SideOnly(Side.CLIENT)
    private Icon field_94393_a;
    @SideOnly(Side.CLIENT)
    private Icon field_94392_b;
    private static EntityPlayer player;

    public CorruptionAlter(int par1, Material par2Material)
    {
        super(par1, par2Material);
    }

    /**
     * From the specified side and block metadata retrieves the blocks texture. Args: side, metadata
     */
    public Icon getIcon(int par1, int par2)
    {
        return par1 == 0 ? this.field_94392_b : (par1 == 1 ? this.field_94393_a : this.blockIcon);
    }

    /**
     * When this method is called, your block should register all the icons it needs with the given IconRegister. This
     * is the only chance you get to register icons.
     */
    public void registerIcons(IconRegister par1IconRegister)
    {
        this.blockIcon = par1IconRegister.registerIcon("hardcoreNether:corruptedAlter_side");
        this.field_94393_a = par1IconRegister.registerIcon("hardcoreNether:corruptedAlter_top");
        this.field_94392_b = par1IconRegister.registerIcon("hardcoreNether:corruptedAlter_side");
    }

    /**
     * Called upon block activation (right click on the block.)
     */
    public boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9)
    {
        ItemStack itemstack = par5EntityPlayer.inventory.getCurrentItem();

        if (!par1World.isRemote)
        {
            if (par1World.getBlockId(par2 - 1, par3 - 1, par4 - 1) == MainMod.corruptedBlock.blockID && par1World.getBlockId(par2 + 1, par3 - 1, par4 + 1) == MainMod.corruptedBlock.blockID && par1World.getBlockId(par2 - 1, par3 - 1, par4 + 1) == MainMod.corruptedBlock.blockID && par1World.getBlockId(par2 + 1, par3 - 1, par4 - 1) == MainMod.corruptedBlock.blockID && par1World.getBlockId(par2 - 1, par3 - 2, par4 - 1) == MainMod.corruptedBlock.blockID && par1World.getBlockId(par2 + 1, par3 - 2, par4 + 1) == MainMod.corruptedBlock.blockID && par1World.getBlockId(par2 - 1, par3 - 2, par4 + 1) == MainMod.corruptedBlock.blockID && par1World.getBlockId(par2 + 1, par3 - 2, par4 - 1) == MainMod.corruptedBlock.blockID && par1World.getBlockId(par2, par3 - 3, par4) == MainMod.bloodBlock.blockID)
            {
                if (itemstack != null)
                {
                    if (itemstack.itemID == MainMod.templeteSword.itemID)
                    {
                        par5EntityPlayer.addChatMessage(EnumChatFormatting.DARK_RED + "LET YOU BE FOREVER POWERFUL!");
                        par5EntityPlayer.setCurrentItemOrArmor(0, new ItemStack(MainMod.corruptedSword, 1));
                        ++MainMod.corruptionLevel;
                    }
                    else if (itemstack.itemID == MainMod.templetePick.itemID)
                    {
                        par5EntityPlayer.addChatMessage(EnumChatFormatting.DARK_RED + "LET YOU MINE YOUR WAY TO VICTORY!");
                        par5EntityPlayer.setCurrentItemOrArmor(0, new ItemStack(MainMod.corruptedPick, 1));
                        ++MainMod.corruptionLevel;
                    }
                    else if (itemstack.itemID == MainMod.templeteAxe.itemID)
                    {
                        par5EntityPlayer.addChatMessage(EnumChatFormatting.DARK_RED + "LET YOUR ENEMIES BE BEHEADED BY YOUR BRAVERY!");
                        par5EntityPlayer.setCurrentItemOrArmor(0, new ItemStack(MainMod.corruptedAxe, 1));
                        ++MainMod.corruptionLevel;
                    }
                    else if (itemstack.itemID == MainMod.templeteSpade.itemID)
                    {
                        par5EntityPlayer.addChatMessage(EnumChatFormatting.DARK_RED + "LET YOU DIG YOUR WAY TO YOUR ENEMIES DEMISES!");
                        par5EntityPlayer.setCurrentItemOrArmor(0, new ItemStack(MainMod.corruptedSpade, 1));
                        ++MainMod.corruptionLevel;
                    }
                    else if (itemstack.itemID == MainMod.templeteHoe.itemID)
                    {
                        par5EntityPlayer.setCurrentItemOrArmor(0, new ItemStack(MainMod.corruptedHoe, 1));
                        ++MainMod.corruptionLevel;
                    }
                    else if (itemstack.itemID != MainMod.templeteSword.itemID || itemstack.itemID != MainMod.templetePick.itemID || itemstack.itemID != MainMod.templeteAxe.itemID || itemstack.itemID != MainMod.templeteSpade.itemID || itemstack.itemID != MainMod.templeteHoe.itemID || itemstack == null)
                    {
                        par5EntityPlayer.addChatMessage(EnumChatFormatting.DARK_RED + "WHAT HAVE YOU TO OFFER TO ME?");
                    }
                }
                else
                {
                    par5EntityPlayer.addChatMessage(EnumChatFormatting.DARK_RED + "WHAT HAVE YOU TO OFFER TO ME?");
                }

                par1World.setBlock(par2 - 1, par3, par4 - 1, Block.fire.blockID);
                par1World.setBlock(par2 + 1, par3, par4 + 1, Block.fire.blockID);
                par1World.setBlock(par2 + 1, par3, par4 - 1, Block.fire.blockID);
                par1World.setBlock(par2 - 1, par3, par4 + 1, Block.fire.blockID);
            }
            else
            {
                par5EntityPlayer.addChatMessage(EnumChatFormatting.DARK_RED + "THE ALTER IS NOT COMPLETE!");
            }
        }

        return false;
    }

    @SideOnly(Side.CLIENT)

    /**
     * A randomly called display update to be able to add particles or other items for display
     */
    public void randomDisplayTick(World par1World, int par2, int par3, int par4, Random par5Random)
    {
        float f = (float)par2 + 0.5F;
        float f1 = (float)par3 + 0.0F + par5Random.nextFloat() * 6.0F / 16.0F;
        float f2 = (float)par4 + 0.5F;
        float f3 = 0.52F;
        float f4 = par5Random.nextFloat() * 0.6F - 0.3F;

        if (MainMod.corruptionLevel >= 6 && MainMod.corruptionLevel < 8)
        {
            par1World.spawnParticle("smoke", (double)(f - f3), (double)f1, (double)(f2 + f4), 0.0D, 0.0D, 0.0D);
            par1World.spawnParticle("smoke", (double)(f + f3), (double)f1, (double)(f2 + f4), 0.0D, 0.0D, 0.0D);
            par1World.spawnParticle("smoke", (double)(f + f4), (double)f1, (double)(f2 - f3), 0.0D, 0.0D, 0.0D);
            par1World.spawnParticle("smoke", (double)(f + f4), (double)f1, (double)(f2 + f3), 0.0D, 0.0D, 0.0D);
        }
        else if (MainMod.corruptionLevel >= 8)
        {
            par1World.spawnParticle("flame", (double)(f - f3), (double)f1, (double)(f2 + f4), 0.0D, 0.0D, 0.0D);
            par1World.spawnParticle("flame", (double)(f + f3), (double)f1, (double)(f2 + f4), 0.0D, 0.0D, 0.0D);
            par1World.spawnParticle("flame", (double)(f + f4), (double)f1, (double)(f2 - f3), 0.0D, 0.0D, 0.0D);
            par1World.spawnParticle("flame", (double)(f + f4), (double)f1, (double)(f2 + f3), 0.0D, 0.0D, 0.0D);
            par1World.spawnEntityInWorld(new EntityLightningBolt(par1World, (double)par2, (double)par3, (double)par4));
        }
    }
}

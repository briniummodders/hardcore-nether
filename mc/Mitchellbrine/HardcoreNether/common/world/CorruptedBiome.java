package mc.Mitchellbrine.HardcoreNether.common.world;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.block.Block;
import net.minecraft.world.biome.BiomeGenBase;

public class CorruptedBiome extends BiomeGenBase
{
    public CorruptedBiome(int par1)
    {
        super(par1);
        this.topBlock = (byte)MainMod.corruptedGrass.blockID;
        this.fillerBlock = (byte)Block.bedrock.blockID;
    }
}

package mc.Mitchellbrine.HardcoreNether.common.world.dimensional;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.world.WorldProvider;
import net.minecraft.world.biome.WorldChunkManagerHell;
import net.minecraft.world.chunk.IChunkProvider;

public class WorldProviderValiant extends WorldProvider
{
    /**
     * Returns the dimension's name, e.g. "The End", "Nether", or "Overworld".
     */
    public String getDimensionName()
    {
        return "The Valiantite";
    }

    /**
     * True if the player can respawn in this dimension (true = overworld, false = nether).
     */
    public boolean canRespawnHere()
    {
        return true;
    }

    /**
     * creates a new world chunk manager for WorldProvider
     */
    public void registerWorldChunkManager()
    {
        this.worldChunkMgr = new WorldChunkManagerHell(MainMod.valiantBiome, 0.8F, 0.1F);
        this.dimensionId = MainMod.dimensionValiant;
    }

    public String getSaveFolder()
    {
        return "Hardcore Nether/Valiantite";
    }

    /**
     * Returns a new chunk provider which generates chunks for this world
     */
    public IChunkProvider createChunkGenerator()
    {
        return new ChunkProviderValiant(this.worldObj, this.worldObj.getSeed(), true);
    }
}

package mc.Mitchellbrine.HardcoreNether.common.world;

import cpw.mods.fml.common.IWorldGenerator;

import java.util.Random;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;

public class WorldGeneratorFlame implements IWorldGenerator
{
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider)
    {
        switch (world.provider.dimensionId)
        {
            case -1:
                this.generateNether(world, random, chunkX * 16, chunkZ * 16);

            case 0:
                this.generateSurface(world, random, chunkX, chunkZ);

            case 1:
                this.generateEnd(world, random, chunkX * 16, chunkZ * 16);

            case 20:
                this.generateDemonic(world, random, chunkX, chunkZ);

            default:
        }
    }

    private void generateDemonic(World world, Random random, int BlockX, int BlockZ)
    {
        for (int i = 0; i < 10; ++i)
        {
            int var10000 = BlockX + random.nextInt(16);
            var10000 = BlockZ + random.nextInt(16);
            int Ycoord = random.nextInt(90);

            if (Ycoord >= 70)
            {
                ;
            }
        }
    }

    private void generateSurface(World world, Random random, int BlockX, int BlockZ)
    {
        int i;
        int Ycoord;

        for (i = 0; i < 40; ++i)
        {
            int Xcoord = BlockX + random.nextInt(16);
            int Zcoord = BlockZ + random.nextInt(16);
            Ycoord = random.nextInt(6);
            (new WorldGenMinable(MainMod.valiantEarth.blockID, 10)).generate(world, random, Xcoord, Ycoord, Zcoord);
        }

        for (i = 0; i < 50; ++i)
        {
            int var10000 = BlockX + random.nextInt(16);
            var10000 = BlockZ + random.nextInt(16);
            Ycoord = random.nextInt(70);
        }
    }

    private void generateEnd(World world, Random random, int BlockX, int BlockZ)
    {
        for (int i = 0; i < 20; ++i)
        {
            int Xcoord = BlockX + random.nextInt(16);
            int Zcoord = BlockZ + random.nextInt(16);
            int Ycoord = random.nextInt(40);
            (new WorldGenEnd(MainMod.valiantEnd.blockID, 10)).generate(world, random, Xcoord, Ycoord, Zcoord);
        }
    }

    private void generateNether(World world, Random random, int BlockX, int BlockZ)
    {
        for (int i = 0; i < 40; ++i)
        {
            int Xcoord = BlockX + random.nextInt(16);
            int Zcoord = BlockZ + random.nextInt(16);
            int Ycoord = random.nextInt(60);
            (new WorldGenNether(MainMod.flamingrack.blockID, 10)).generate(world, random, Xcoord, Ycoord, Zcoord);
        }
    }
}

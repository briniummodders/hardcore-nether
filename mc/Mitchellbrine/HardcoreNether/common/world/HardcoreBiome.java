package mc.Mitchellbrine.HardcoreNether.common.world;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.world.biome.BiomeGenBase;

public class HardcoreBiome extends BiomeGenBase
{
    public HardcoreBiome(int par1)
    {
        super(par1);
        this.topBlock = (byte)MainMod.flamingrack.blockID;
        this.fillerBlock = (byte)MainMod.demonicStone.blockID;
    }
}

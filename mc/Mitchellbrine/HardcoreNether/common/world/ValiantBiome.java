package mc.Mitchellbrine.HardcoreNether.common.world;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.world.biome.BiomeGenBase;

public class ValiantBiome extends BiomeGenBase
{
    public ValiantBiome(int par1)
    {
        super(par1);
        this.topBlock = (byte)MainMod.valiantEarth.blockID;
        this.fillerBlock = (byte)MainMod.angelicStone.blockID;
    }
}

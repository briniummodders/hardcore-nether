package mc.Mitchellbrine.HardcoreNether.common.entity;

import java.util.List;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAvoidEntity;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

public class EntityDemon extends EntityMob
{
    private int angerLevel = 0;

    public EntityDemon(World par1World)
    {
        super(par1World);
        this.isImmuneToFire = true;
        this.canBreatheUnderwater();
        this.experienceValue = 10;
        this.tasks.addTask(3, new EntityAIAvoidEntity(this, EntitySolder.class, 6.0F, 0.25D, 0.30000001192092896D));
    }

    protected void applyEntityAttributes()
    {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setAttribute(1000.0D);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setAttribute(0.5D);
    }

    protected void entityInit()
    {
        super.entityInit();
    }

    /**
     * Returns true if the newer Entity AI code should be run
     */
    public boolean isAIEnabled()
    {
        return true;
    }

    /**
     * Called when a player interacts with a mob. e.g. gets milk from a cow, gets into the saddle on a pig.
     */
    public boolean interact(EntityPlayer par1EntityPlayer)
    {
        ItemStack itemstack = par1EntityPlayer.inventory.getCurrentItem();

        if (itemstack != null && itemstack.itemID == MainMod.valiantCoin.itemID)
        {
            if (MainMod.barterLevel == 0)
            {
                if (!par1EntityPlayer.worldObj.isRemote)
                {
                    par1EntityPlayer.addChatMessage(EnumChatFormatting.RED + "DEMON: A bargain, but I want MORE!");
                    ++MainMod.barterLevel;
                }

                if (!par1EntityPlayer.capabilities.isCreativeMode)
                {
                    --par1EntityPlayer.inventory.getCurrentItem().stackSize;
                }
            }
            else if (MainMod.barterLevel == 1)
            {
                if (!par1EntityPlayer.worldObj.isRemote)
                {
                    par1EntityPlayer.addChatMessage(EnumChatFormatting.RED + "DEMON: Better, but keep trying!");
                    ++MainMod.barterLevel;
                }

                if (!par1EntityPlayer.capabilities.isCreativeMode)
                {
                    --par1EntityPlayer.inventory.getCurrentItem().stackSize;
                }
            }
            else if (MainMod.barterLevel == 2)
            {
                if (!par1EntityPlayer.worldObj.isRemote)
                {
                    par1EntityPlayer.addChatMessage(EnumChatFormatting.RED + "DEMON: There ya go! Here\'s a sword!");
                    this.dropItem(MainMod.hardcoreIronSword.itemID, 1);
                    ++MainMod.barterLevel;
                }

                if (!par1EntityPlayer.capabilities.isCreativeMode)
                {
                    --par1EntityPlayer.inventory.getCurrentItem().stackSize;
                }
            }
            else if (MainMod.barterLevel == 3)
            {
                if (!par1EntityPlayer.worldObj.isRemote)
                {
                    par1EntityPlayer.inventory.consumeInventoryItem(MainMod.hardcoreIronSword.itemID);
                    par1EntityPlayer.addChatMessage(EnumChatFormatting.RED + "DEMON: I\'ll take that you no-good bargainer! You want more, huh? Take it up with my boss!");
                    ++MainMod.barterLevel;
                }
            }
            else if (MainMod.barterLevel == 4)
            {
                if (!par1EntityPlayer.worldObj.isRemote)
                {
                    par1EntityPlayer.addChatMessage(EnumChatFormatting.RED + "DEMON: He\'s watching us!");
                    ++MainMod.barterLevel;
                }
            }
            else if (MainMod.barterLevel == 5)
            {
                if (!par1EntityPlayer.worldObj.isRemote)
                {
                    par1EntityPlayer.addChatMessage(EnumChatFormatting.DARK_RED + "DEMON: Take these! You\'re our last hope!");
                    this.dropItem(MainMod.hardcoreEye.itemID, 2);
                    this.setDead();
                    MainMod.barterLevel = 0;
                }
                else
                {
                    par1EntityPlayer.addStat(MainMod.soulSeller, 1);
                }
            }

            return true;
        }
        else
        {
            return super.interact(par1EntityPlayer);
        }
    }

    public int getAttackStrength(Entity par1Entity)
    {
        return 160;
    }

    /**
     * Makes entity wear random armor based on difficulty
     */
    protected void addRandomArmor()
    {
        this.setCurrentItemOrArmor(0, new ItemStack(MainMod.hardcoreSword));
        this.setCurrentItemOrArmor(1, new ItemStack(MainMod.hardcoreHelmet));
        this.setCurrentItemOrArmor(2, new ItemStack(MainMod.hardcorePlate));
        this.setCurrentItemOrArmor(3, new ItemStack(MainMod.hardcoreLegs));
        this.setCurrentItemOrArmor(4, new ItemStack(MainMod.hardcoreBoots));
    }

    /**
     * (abstract) Protected helper method to write subclass entity data to NBT.
     */
    public void writeEntityToNBT(NBTTagCompound par1NBTTagCompound)
    {
        super.writeEntityToNBT(par1NBTTagCompound);
        par1NBTTagCompound.setShort("Anger", (short)this.angerLevel);
    }

    /**
     * (abstract) Protected helper method to read subclass entity data from NBT.
     */
    public void readEntityFromNBT(NBTTagCompound par1NBTTagCompound)
    {
        super.readEntityFromNBT(par1NBTTagCompound);
        this.angerLevel = par1NBTTagCompound.getShort("Anger");
    }

    /**
     * Finds the closest player within 16 blocks to attack, or null if this Entity isn't interested in attacking
     * (Animals, Spiders at day, peaceful PigZombies).
     */
    protected Entity findPlayerToAttack()
    {
        return this.angerLevel == 0 ? null : super.findPlayerToAttack();
    }

    public boolean attackEntityFrom(DamageSource par1DamageSource, int par2)
    {
        if (this.isEntityInvulnerable())
        {
            return false;
        }
        else
        {
            Entity entity = par1DamageSource.getEntity();

            if (entity instanceof EntityPlayer)
            {
                List list = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, this.boundingBox.expand(32.0D, 32.0D, 32.0D));

                for (int j = 0; j < list.size(); ++j)
                {
                    Entity entity1 = (Entity)list.get(j);

                    if (entity1 instanceof EntityDemon)
                    {
                        EntityDemon entitypigzombie = (EntityDemon)entity1;
                        entitypigzombie.becomeAngryAt(entity);
                    }
                }

                this.becomeAngryAt(entity);
            }

            return super.attackEntityFrom(par1DamageSource, (float)par2);
        }
    }

    private void becomeAngryAt(Entity par1Entity)
    {
        this.entityToAttack = par1Entity;
        this.angerLevel = 400 + this.rand.nextInt(400);
    }
}

package mc.Mitchellbrine.HardcoreNether.common.entity.villager;

import cpw.mods.fml.common.registry.VillagerRegistry.IVillageTradeHandler;

import java.util.Random;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.item.ItemStack;
import net.minecraft.village.MerchantRecipe;
import net.minecraft.village.MerchantRecipeList;

public class SatanTradeHandler implements IVillageTradeHandler
{
    public void manipulateTradesForVillager(EntityVillager villager, MerchantRecipeList recipeList, Random random)
    {
        if (random.nextInt() > 60)
        {
            recipeList.add(new MerchantRecipe(new ItemStack(MainMod.templete, 12), new ItemStack(MainMod.templeteSword, 1, 0)));
            recipeList.add(new MerchantRecipe(new ItemStack(MainMod.templete, 10), new ItemStack(MainMod.templetePick, 1, 0)));
            recipeList.add(new MerchantRecipe(new ItemStack(MainMod.templete, 10), new ItemStack(MainMod.templeteAxe, 1, 0)));
            recipeList.add(new MerchantRecipe(new ItemStack(MainMod.templete, 10), new ItemStack(MainMod.templeteSpade, 1, 0)));
            recipeList.add(new MerchantRecipe(new ItemStack(MainMod.templete, 10), new ItemStack(MainMod.templeteHoe, 1, 0)));
        }
    }
}

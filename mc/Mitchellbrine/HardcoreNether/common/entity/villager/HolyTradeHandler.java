package mc.Mitchellbrine.HardcoreNether.common.entity.villager;

import cpw.mods.fml.common.registry.VillagerRegistry.IVillageTradeHandler;

import java.util.Random;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.village.MerchantRecipe;
import net.minecraft.village.MerchantRecipeList;

public class HolyTradeHandler implements IVillageTradeHandler
{
    public void manipulateTradesForVillager(EntityVillager villager, MerchantRecipeList recipeList, Random random)
    {
        if (random.nextInt() > 80)
        {
            recipeList.add(new MerchantRecipe(new ItemStack(Item.emerald, 10), new ItemStack(MainMod.angelicBow, 1, 0)));
        }
    }
}

package mc.Mitchellbrine.HardcoreNether.common.entity;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.world.World;

public class EntityHand extends EntityMob
{
    public EntityHand(World par1World)
    {
        super(par1World);
        this.isImmuneToFire = true;
        this.canBreatheUnderwater();
        this.experienceValue = 50;
    }

    protected void entityInit()
    {
        super.entityInit();
    }

    protected void applyEntityAttributes()
    {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setAttribute(4500.0D);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setAttribute(0.5D);
    }

    public int getAttackStrength(Entity par1Entity)
    {
        return 80;
    }

    /**
     * Returns the item ID for the item the mob drops on death.
     */
    protected int getDropItemId()
    {
        return MainMod.hardcoreHand.itemID;
    }

    /**
     * Returns the sound this mob makes while it's alive.
     */
    protected String getLivingSound()
    {
        return "hardcoreNether:mob.hand.live";
    }
}

package mc.Mitchellbrine.HardcoreNether.common.entity;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.boss.IBossDisplayData;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;

public class EntityHostStage3 extends EntityMob implements IBossDisplayData
{
    private int field_70846_g;
    private int hostAttackTimer;

    public EntityHostStage3(World par1World)
    {
        super(par1World);
        this.isImmuneToFire = true;
        this.canBreatheUnderwater();
        this.experienceValue = 1400;
    }

    protected void entityInit()
    {
        super.entityInit();
    }

    /**
     * Drop 0-2 items of this living's type. @param par1 - Whether this entity has recently been hit by a player. @param
     * par2 - Level of Looting used to kill this mob.
     */
    protected void dropFewItems(boolean par1, int par2)
    {
        this.dropItem(MainMod.heartOfRagnad.itemID, 1);
    }

    /**
     * Called when the mob's health reaches 0.
     */
    public void onDeath(DamageSource par1DamageSource)
    {
        if (par1DamageSource.getSourceOfDamage() instanceof EntityPlayer)
        {
            EntityPlayer player = (EntityPlayer)par1DamageSource.getSourceOfDamage();
            player.addStat(MainMod.bossKill, 1);
        }
    }

    protected void applyEntityAttributes()
    {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setAttribute(3333.0D);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setAttribute(1.2D);
    }

    public int getAttackStrength(Entity par1Entity)
    {
        return 800;
    }

    /**
     * Returns the sound this mob makes while it's alive.
     */
    protected String getLivingSound()
    {
        return "hardcoreNether:mob.host.live";
    }

    /**
     * Basic mob attack. Default to touch of death in EntityCreature. Overridden by each mob to define their attack.
     */
    protected void attackEntity(Entity par1Entity, float par2)
    {
        if (this.attackTime <= 0 && par2 < 2.0F && par1Entity.boundingBox.maxY > this.boundingBox.minY && par1Entity.boundingBox.minY < this.boundingBox.maxY)
        {
            this.attackTime = 5;
            this.attackEntityAsMob(par1Entity);
            ((EntityLivingBase)par1Entity).addPotionEffect(new PotionEffect(Potion.wither.id, 60, 2, false));
        }
    }
}

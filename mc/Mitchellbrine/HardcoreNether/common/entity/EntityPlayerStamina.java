package mc.Mitchellbrine.HardcoreNether.common.entity;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraftforge.common.IExtendedEntityProperties;

public class EntityPlayerStamina implements IExtendedEntityProperties
{
    public final EntityPlayer player;
    public int Stamina;
    public static final String EXT_PROP_NAME = "EntityPlayerStaminaBalances";

    public EntityPlayerStamina(EntityPlayer player)
    {
        this.Stamina = MainMod.Special;
        this.player = player;
    }

    public void saveNBTData(NBTTagCompound compound)
    {
        compound.setInteger("Stamina", this.Stamina);
    }

    public void loadNBTData(NBTTagCompound compound)
    {
        this.Stamina = compound.getInteger("Stamina");
    }

    public void init(Entity entity, World world) {}
}

package mc.Mitchellbrine.HardcoreNether.common.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.boss.IBossDisplayData;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;

public class EntityHostStage2 extends EntityMob implements IBossDisplayData
{
    private int explosionRadius = 4;
    private int field_70846_g;
    private int hostAttackTimer;
    private Explosion explosion;

    public EntityHostStage2(World par1World)
    {
        super(par1World);
        this.isImmuneToFire = true;
        this.canBreatheUnderwater();
        this.experienceValue = 0;
    }

    protected void applyEntityAttributes()
    {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setAttribute(5000.0D);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setAttribute(0.8D);
    }

    protected void entityInit()
    {
        super.entityInit();
        this.getDataWatcher().addObject(21, Byte.valueOf((byte)0));
        this.setAggressive(true);
    }

    public int getAttackStrength(Entity par1Entity)
    {
        return 800;
    }

    public void setAggressive(boolean par1)
    {
        this.getDataWatcher().updateObject(21, Byte.valueOf((byte)(par1 ? 1 : 0)));
    }

    public boolean getAggressive()
    {
        return this.getDataWatcher().getWatchableObjectByte(21) == 1;
    }

    /**
     * Returns the sound this mob makes while it's alive.
     */
    protected String getLivingSound()
    {
        return "hardcoreNether:mob.host.live";
    }

    /**
     * Basic mob attack. Default to touch of death in EntityCreature. Overridden by each mob to define their attack.
     */
    protected void attackEntity(Entity par1Entity, float par2)
    {
        if (this.attackTime <= 0 && par2 < 2.0F && par1Entity.boundingBox.maxY > this.boundingBox.minY && par1Entity.boundingBox.minY < this.boundingBox.maxY)
        {
            this.attackTime = 5;
            this.attackEntityAsMob(par1Entity);

            if (this.worldObj.getGameRules().getGameRuleBooleanValue("mobGriefing"))
            {
                this.worldObj.createExplosion(par1Entity, this.posX, this.posY, this.posZ, (float)this.explosionRadius, true);
            }
            else
            {
                this.worldObj.createExplosion(par1Entity, this.posX, this.posY, this.posZ, (float)this.explosionRadius, false);
            }
        }
    }
}

package mc.Mitchellbrine.HardcoreNether.common.tileentity;

import cpw.mods.fml.common.Mod.ServerStarting;
import cpw.mods.fml.common.Mod.ServerStopping;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.event.FMLServerStoppingEvent;
import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class TileEntityCharm extends TileEntity
{
    public NBTTagCompound nbt;
    public static int Special = MainMod.Special;

    /**
     * Reads a tile entity from NBT.
     */
    public void readFromNBT(NBTTagCompound nbt)
    {
        super.readFromNBT(nbt);
        Special = nbt.getInteger("Special");
    }

    /**
     * Writes a tile entity to NBT.
     */
    public void writeToNBT(NBTTagCompound nbt)
    {
        super.writeToNBT(nbt);
        nbt.setInteger("Special", Special);
    }

    @ServerStopping
    public void onServerStop(FMLServerStoppingEvent event)
    {
        this.writeToNBT(this.nbt);
    }

    @ServerStarting
    public void onServerStart(FMLServerStartingEvent event)
    {
        this.readFromNBT(this.nbt);
    }
}

package mc.Mitchellbrine.HardcoreNether.common.handler;

import cpw.mods.fml.common.network.IPacketHandler;
import cpw.mods.fml.common.network.Player;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;

public class TutPacketHandler implements IPacketHandler
{
    public void onPacketData(INetworkManager manager, Packet250CustomPayload packet, Player player)
    {
        if (packet.channel.equals("HardcoreNether"))
        {
            this.handlePacket(packet);
        }
    }

    public void handlePacket(Packet250CustomPayload packet)
    {
        DataInputStream inputStream = new DataInputStream(new ByteArrayInputStream(packet.data));

        try
        {
            MainMod.Special = inputStream.readInt();
        }
        catch (IOException var4)
        {
            var4.printStackTrace();
        }
    }
}

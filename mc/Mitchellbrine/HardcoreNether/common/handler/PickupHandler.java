package mc.Mitchellbrine.HardcoreNether.common.handler;

import cpw.mods.fml.common.IPickupNotifier;
import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;

public class PickupHandler implements IPickupNotifier
{
    public void notifyPickup(EntityItem item, EntityPlayer player)
    {
        if (item.getEntityItem().itemID == MainMod.valiantNugget.itemID)
        {
            player.addStat(MainMod.valiantAchievement, 1);
        }

        if (item.getEntityItem().itemID == MainMod.blood.itemID)
        {
            player.addStat(MainMod.bloodAchievement, 1);
        }
    }
}

package mc.Mitchellbrine.HardcoreNether.common.handler;

import cpw.mods.fml.common.ICraftingHandler;
import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

public class HardcoreCraftingHandler implements ICraftingHandler
{
    public void onCrafting(EntityPlayer player, ItemStack item, IInventory inventory)
    {
        if (item.itemID == MainMod.valiantNugget.itemID)
        {
            player.addStat(MainMod.valiantAchievement, 1);
        }

        if (item.itemID == MainMod.corruptedOre.itemID)
        {
            player.addStat(MainMod.corruptAchievement, 1);
        }

        if (item.itemID == MainMod.hardcoreOre.itemID)
        {
            MainMod.Special += 5;
        }
    }

    public void onSmelting(EntityPlayer player, ItemStack item)
    {
        if (item.itemID == MainMod.hardcoreOre.itemID)
        {
            player.addStat(MainMod.HardcoreBeginnings, 1);
        }
    }
}

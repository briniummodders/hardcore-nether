package mc.Mitchellbrine.HardcoreNether.common.core;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.creativetab.CreativeTabs;

final class HardcoreTab extends CreativeTabs
{
    HardcoreTab(int par1, String par2Str)
    {
        super(par1, par2Str);
    }

    @SideOnly(Side.CLIENT)

    /**
     * the itemID for the item to be displayed on the tab
     */
    public int getTabIconItemIndex()
    {
        return MainMod.flamingrack.blockID;
    }

    public String getTabLabel()
    {
        return "Hardcore Nether by Mitchellbrine";
    }
}

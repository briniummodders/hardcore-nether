package mc.Mitchellbrine.HardcoreNether.common.core;

import java.lang.reflect.Field;
import java.util.Map;

import mc.Mitchellbrine.HardcoreNether.client.handler.GuiHandler;
import mc.Mitchellbrine.HardcoreNether.common.block.AngelStone;
import mc.Mitchellbrine.HardcoreNether.common.block.BlockHCLeaves;
import mc.Mitchellbrine.HardcoreNether.common.block.BlockHCLog;
import mc.Mitchellbrine.HardcoreNether.common.block.BlockHCWall;
import mc.Mitchellbrine.HardcoreNether.common.block.BlockHardcoreSapling;
import mc.Mitchellbrine.HardcoreNether.common.block.BlockPhonyChest;
import mc.Mitchellbrine.HardcoreNether.common.block.BlockScroll;
import mc.Mitchellbrine.HardcoreNether.common.block.BloodSoil;
import mc.Mitchellbrine.HardcoreNether.common.block.CorruptTorch;
import mc.Mitchellbrine.HardcoreNether.common.block.CorruptedOre;
import mc.Mitchellbrine.HardcoreNether.common.block.CorruptionAlter;
import mc.Mitchellbrine.HardcoreNether.common.block.CorruptionBlock;
import mc.Mitchellbrine.HardcoreNether.common.block.DemonStone;
import mc.Mitchellbrine.HardcoreNether.common.block.DemonicTNT;
import mc.Mitchellbrine.HardcoreNether.common.block.DispenserBehaviorLighter;
import mc.Mitchellbrine.HardcoreNether.common.block.FlamingRack;
import mc.Mitchellbrine.HardcoreNether.common.block.HardcoreMaterial;
import mc.Mitchellbrine.HardcoreNether.common.block.HardcorePortal;
import mc.Mitchellbrine.HardcoreNether.common.block.HardcoreStairs;
import mc.Mitchellbrine.HardcoreNether.common.block.HolyFire;
import mc.Mitchellbrine.HardcoreNether.common.block.PowerPouchFroster;
import mc.Mitchellbrine.HardcoreNether.common.block.PowerPouchHeater;
import mc.Mitchellbrine.HardcoreNether.common.block.PoweredPouch;
import mc.Mitchellbrine.HardcoreNether.common.block.TutFurnace;
import mc.Mitchellbrine.HardcoreNether.common.block.ValiantPortal;
import mc.Mitchellbrine.HardcoreNether.common.block.ValiantStairs;
import mc.Mitchellbrine.HardcoreNether.common.block.ValiantStone;
import mc.Mitchellbrine.HardcoreNether.common.block.ValiantiteOreEarth;
import mc.Mitchellbrine.HardcoreNether.common.block.ValiantiteOreEnd;
import mc.Mitchellbrine.HardcoreNether.common.command.StaminaCommand;
import mc.Mitchellbrine.HardcoreNether.common.entity.EntityAngelicPig;
import mc.Mitchellbrine.HardcoreNether.common.entity.EntityBrain;
import mc.Mitchellbrine.HardcoreNether.common.entity.EntityDemon;
import mc.Mitchellbrine.HardcoreNether.common.entity.EntityDemonicTNT;
import mc.Mitchellbrine.HardcoreNether.common.entity.EntityDemonicWolf;
import mc.Mitchellbrine.HardcoreNether.common.entity.EntityHand;
import mc.Mitchellbrine.HardcoreNether.common.entity.EntityHardcoreCreeper;
import mc.Mitchellbrine.HardcoreNether.common.entity.EntityHardcorePigZombie;
import mc.Mitchellbrine.HardcoreNether.common.entity.EntityHolyArrow;
import mc.Mitchellbrine.HardcoreNether.common.entity.EntityHost;
import mc.Mitchellbrine.HardcoreNether.common.entity.EntityHostStage2;
import mc.Mitchellbrine.HardcoreNether.common.entity.EntityHostStage3;
import mc.Mitchellbrine.HardcoreNether.common.entity.EntitySolder;
import mc.Mitchellbrine.HardcoreNether.common.entity.EntityValiantite;
import mc.Mitchellbrine.HardcoreNether.common.entity.villager.HolyTradeHandler;
import mc.Mitchellbrine.HardcoreNether.common.entity.villager.SatanTradeHandler;
import mc.Mitchellbrine.HardcoreNether.common.handler.HardcoreCraftingHandler;
import mc.Mitchellbrine.HardcoreNether.common.handler.PickupHandler;
import mc.Mitchellbrine.HardcoreNether.common.handler.TutPacketHandler;
import mc.Mitchellbrine.HardcoreNether.common.item.AngelArmor;
import mc.Mitchellbrine.HardcoreNether.common.item.AngelStaff;
import mc.Mitchellbrine.HardcoreNether.common.item.AngelStaffUse;
import mc.Mitchellbrine.HardcoreNether.common.item.AngelicBow;
import mc.Mitchellbrine.HardcoreNether.common.item.AntiWitherEffect;
import mc.Mitchellbrine.HardcoreNether.common.item.AntiWitherPotion;
import mc.Mitchellbrine.HardcoreNether.common.item.Book1;
import mc.Mitchellbrine.HardcoreNether.common.item.CorruptedArmor;
import mc.Mitchellbrine.HardcoreNether.common.item.DemonStaff;
import mc.Mitchellbrine.HardcoreNether.common.item.DemonStaffUse;
import mc.Mitchellbrine.HardcoreNether.common.item.HCPotions;
import mc.Mitchellbrine.HardcoreNether.common.item.HCRecord;
import mc.Mitchellbrine.HardcoreNether.common.item.HardcoreArmor;
import mc.Mitchellbrine.HardcoreNether.common.item.HardcoreAxe;
import mc.Mitchellbrine.HardcoreNether.common.item.HardcoreDiamondSword;
import mc.Mitchellbrine.HardcoreNether.common.item.HardcoreEncrypt1Sword;
import mc.Mitchellbrine.HardcoreNether.common.item.HardcoreEncrypt2Sword;
import mc.Mitchellbrine.HardcoreNether.common.item.HardcoreEncrypt3Sword;
import mc.Mitchellbrine.HardcoreNether.common.item.HardcoreEncrypt4Sword;
import mc.Mitchellbrine.HardcoreNether.common.item.HardcoreEncrypt5Sword;
import mc.Mitchellbrine.HardcoreNether.common.item.HardcoreEncrypt6Sword;
import mc.Mitchellbrine.HardcoreNether.common.item.HardcoreGoldSword;
import mc.Mitchellbrine.HardcoreNether.common.item.HardcoreHoe;
import mc.Mitchellbrine.HardcoreNether.common.item.HardcoreIronSword;
import mc.Mitchellbrine.HardcoreNether.common.item.HardcorePickaxe;
import mc.Mitchellbrine.HardcoreNether.common.item.HardcoreRing;
import mc.Mitchellbrine.HardcoreNether.common.item.HardcoreSpade;
import mc.Mitchellbrine.HardcoreNether.common.item.HardcoreStoneSword;
import mc.Mitchellbrine.HardcoreNether.common.item.HardcoreSword;
import mc.Mitchellbrine.HardcoreNether.common.item.HardcoreWoodSword;
import mc.Mitchellbrine.HardcoreNether.common.item.HolyLighter;
import mc.Mitchellbrine.HardcoreNether.common.item.ItemEncryption1;
import mc.Mitchellbrine.HardcoreNether.common.item.PortalPlacer;
import mc.Mitchellbrine.HardcoreNether.common.item.ValiantArmor;
import mc.Mitchellbrine.HardcoreNether.common.item.ValiantAxe;
import mc.Mitchellbrine.HardcoreNether.common.item.ValiantHoe;
import mc.Mitchellbrine.HardcoreNether.common.item.ValiantPickaxe;
import mc.Mitchellbrine.HardcoreNether.common.item.ValiantSpade;
import mc.Mitchellbrine.HardcoreNether.common.item.ValiantSword;
import mc.Mitchellbrine.HardcoreNether.common.item.ValiantiteStar;
import mc.Mitchellbrine.HardcoreNether.common.item.XPCharm;
import mc.Mitchellbrine.HardcoreNether.common.item.enchantment.EnchantmentAngel;
import mc.Mitchellbrine.HardcoreNether.common.item.enchantment.EnchantmentHand;
import mc.Mitchellbrine.HardcoreNether.common.stamina.EventHandler_Stamina;
import mc.Mitchellbrine.HardcoreNether.common.stamina.StaminaHandler;
import mc.Mitchellbrine.HardcoreNether.common.tileentity.TileEntityCharm;
import mc.Mitchellbrine.HardcoreNether.common.tileentity.TileEntityFroster;
import mc.Mitchellbrine.HardcoreNether.common.tileentity.TileEntityHeater;
import mc.Mitchellbrine.HardcoreNether.common.tileentity.TileEntityScroll;
import mc.Mitchellbrine.HardcoreNether.common.tileentity.TileEntityTutFurnace;
import mc.Mitchellbrine.HardcoreNether.common.world.CorruptedBiome;
import mc.Mitchellbrine.HardcoreNether.common.world.HardcoreBiome;
import mc.Mitchellbrine.HardcoreNether.common.world.ValiantBiome;
import mc.Mitchellbrine.HardcoreNether.common.world.WorldGeneratorFlame;
import mc.Mitchellbrine.HardcoreNether.common.world.dimensional.WorldProviderHardcore;
import mc.Mitchellbrine.HardcoreNether.common.world.dimensional.WorldProviderValiant;
import mc.Mitchellbrine.HardcoreNether.dungeon.EntityScientist;
import net.minecraft.block.Block;
import net.minecraft.block.BlockDispenser;
import net.minecraft.block.BlockFence;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.entity.RenderSnowball;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.dispenser.IBehaviorDispenseItem;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnumEnchantmentType;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityEggInfo;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.monster.EntityEnderman;
import net.minecraft.entity.monster.EntitySkeleton;
import net.minecraft.entity.monster.EntitySpider;
import net.minecraft.entity.monster.EntityWitch;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.entity.passive.EntityChicken;
import net.minecraft.entity.passive.EntityCow;
import net.minecraft.entity.passive.EntityPig;
import net.minecraft.entity.passive.EntitySheep;
import net.minecraft.item.EnumArmorMaterial;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemSpade;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.potion.Potion;
import net.minecraft.stats.Achievement;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.WeightedRandomChestContent;
import net.minecraft.world.GameRules;
import net.minecraft.world.WorldSavedData;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.common.AchievementPage;
import net.minecraftforge.common.ChestGenHooks;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.common.EnumHelper;
import net.minecraftforge.common.MinecraftForge;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import cpw.mods.fml.common.registry.VillagerRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@Mod(modid = "hardcoreNether", name = "Hardcore Nether", version = "Beta 1.1")
@NetworkMod(clientSideRequired = true, serverSideRequired = false, channels = {"HardcoreNether"}, packetHandler = TutPacketHandler.class)
public class MainMod
{
    @SidedProxy(clientSide = "mc.Mitchellbrine.HardcoreNether.client.core.ClientProxy", serverSide = "mc.Mitchellbrine.HardcoreNether.common.core.CommonProxy")
    
    public static CommonProxy proxy;
    
    public static RenderingRegistry render;
    
    @Instance("MainMod")
    public static MainMod instance = new MainMod();
        
    public static int Special = 0;
        
    public static int startEntityId = 200;
    
    public static int dimension = 20;
    
    public static int dimensionValiant = 21;
    
    public static int ValiantActivate;
    
    public static int barterLevel;
    
    public static int corruptionLevel;
    
    public static boolean rosieTextures;
    
    public static boolean isSnapshot = true;
    
    public static ResourceLocation corruptVillagerSkin = new ResourceLocation("/textures/render/corruptVillager.png");
    
    public static String TRD = EnumChatFormatting.BOLD + "T" + EnumChatFormatting.RESET + "emporal " + EnumChatFormatting.BOLD + "R" + EnumChatFormatting.RESET + "estoration " + EnumChatFormatting.BOLD + "D" + EnumChatFormatting.RESET + "evice";
    
    public static CreativeTabs HardcoreTab = new HardcoreTab(CreativeTabs.getNextID(), "Hardcore Nether");
    
    public static EnumToolMaterial hardcore = EnumHelper.addToolMaterial("hardcore", 6, 781, 16.0F, 30.0F, 20);
    public static EnumToolMaterial hardcoreWood = EnumHelper.addToolMaterial("hardcoreWood", 7, 976, 20.0F, 38.0F, 25);
    public static EnumToolMaterial hardcoreStone = EnumHelper.addToolMaterial("hardcoreStone", 9, 1172, 24.0F, 45.0F, 30);
    public static EnumToolMaterial hardcoreGold = EnumHelper.addToolMaterial("hardcoreGold", 11, 1367, 28.0F, 53.0F, 35);
    public static EnumToolMaterial hardcoreIron = EnumHelper.addToolMaterial("hardcoreIron", 12, 1562, 32.0F, 60.0F, 40);
    public static EnumToolMaterial hardcoreDiamond = EnumHelper.addToolMaterial("hardcoreDiamond", 15, 1923, 40.0F, 75.0F, 50);
    public static EnumToolMaterial valiant = EnumHelper.addToolMaterial("valiant", 6, 1562, 32.0F, 30.0F, 40);
    public static EnumToolMaterial corrupted = EnumHelper.addToolMaterial("corrupted", 8, 2000, 42.0F, 40.0F, 45);
    
    public static EnumArmorMaterial hardcoreArmor = EnumHelper.addArmorMaterial("HARDCORE", 100, new int[] {4, 6, 10, 4}, 30);
    public static EnumArmorMaterial ValiantArmor = EnumHelper.addArmorMaterial("VALIANT", 200, new int[] {4, 6, 10, 4}, 60);
    public static EnumArmorMaterial corruptArmor = EnumHelper.addArmorMaterial("CORRUPTED", 300, new int[] {6, 8, 12, 6}, 90);
    public static EnumArmorMaterial angelArmor = EnumHelper.addArmorMaterial("ANGEL", 100, new int[] {0, 0, 0, 0}, 30);
    
    public static WorldSavedData staminaSave = new StaminaHandler("Stamina");
    
    public static Material hardcoreMaterial = new HardcoreMaterial(MapColor.tntColor);
    
    public static Enchantment angelBlessing = new EnchantmentAngel(100, 1, EnumEnchantmentType.weapon);
    public static Enchantment handOfGod = new EnchantmentHand(101, 1, EnumEnchantmentType.weapon);
    
    public static final Block flamingrack = (new FlamingRack(160)).setHardness(2.0F).setResistance(2000.0F).setTextureName("hardcoreNether:flamerock").setUnlocalizedName("hardcoreNether:flamerock");
    public static final Item hardcoreOre = (new Item(3010)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:hardcoreOre").setUnlocalizedName("hardcoreNether:hardcoreOre");
    public static final Item demonicGem = (new Item(3020)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:demonOre").setUnlocalizedName("hardcoreNether:demonOre");
    public static final Block hardcoreBlock = (new Block(161, Material.rock)).setHardness(2.2F).setCreativeTab(HardcoreTab).setHardness(3.0F).setResistance(2000.0F).setTextureName("hardcoreNether:hardcoreBlock").setUnlocalizedName("hardcoreNether:hardcoreBlock");
    public static final Item hardcoreSword = (new HardcoreSword(3011, hardcore)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:hardcoreSword").setUnlocalizedName("hardcoreNether:hardcoreSword");
    public static final Item stamina = (new Item(5018)).setCreativeTab(CreativeTabs.tabAllSearch).setTextureName("hardcoreNether:stamina").setUnlocalizedName("hardcoreNether:stamina");
    public static final Block valiantEarth = (new ValiantiteOreEarth(162, hardcoreMaterial)).setHardness(3.0F).setResistance(2000.0F).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:valiantOreBed").setUnlocalizedName("hardcoreNether:valiantOreBed");
    public static Block demonicStone = (new DemonStone(191, Material.rock)).setCreativeTab(HardcoreTab).setHardness(1.5F).setUnlocalizedName("hardcoreNether:demonStone").setTextureName("hardcoreNether:demonStone");
    public static Block demonicCobble = (new Block(192, Material.rock)).setCreativeTab(HardcoreTab).setHardness(2.0F).setUnlocalizedName("hardcoreNether:demonCobble").setTextureName("hardcoreNether:demonCobble");
    public static Block demonicStoneBricks = (new Block(193, Material.rock)).setCreativeTab(HardcoreTab).setHardness(1.5F).setResistance(10.0F).setUnlocalizedName("hardcoreNether:demonStoneBrick").setTextureName("hardcoreNether:demonStoneBrick");
    public static Block summonerChest = (new BlockPhonyChest(194)).setCreativeTab(HardcoreTab).setUnlocalizedName("hardcoreNether:phonyChest");
    public static Block angelicStone = (new AngelStone(195, Material.rock)).setCreativeTab(HardcoreTab).setHardness(1.5F).setUnlocalizedName("hardcoreNether:angelStone").setTextureName("hardcoreNether:angelStone");
    public static Block angelicCobble = (new Block(196, Material.rock)).setCreativeTab(HardcoreTab).setHardness(2.0F).setUnlocalizedName("hardcoreNether:angelCobble").setTextureName("hardcoreNether:angelCobble");
    public static Block angelicStoneBricks = (new Block(197, Material.rock)).setCreativeTab(HardcoreTab).setHardness(1.5F).setResistance(10.0F).setUnlocalizedName("hardcoreNether:angelStoneBrick").setTextureName("hardcoreNether:angelStoneBrick");
    public static Block demonicCobbleWall = (new BlockHCWall(198, demonicCobble)).setCreativeTab(HardcoreTab).setUnlocalizedName("hardcoreNether:demonWall");
    public static Block angelicCobbleWall = (new BlockHCWall(199, angelicCobble)).setCreativeTab(HardcoreTab).setUnlocalizedName("hardcoreNether:angelWall");
    public static Block demonicCobbleMossy = (new Block(203, Material.rock)).setCreativeTab(HardcoreTab).setHardness(2.0F).setResistance(10.0F).setUnlocalizedName("hardcoreNether:mossyDemonCobble").setTextureName("hardcoreNether:demonCobble_mossy");
    public static BiomeGenBase hardcoreBiome = (new HardcoreBiome(53)).setColor(10489616).setBiomeName("Hardcore Biome").setDisableRain().setTemperatureRainfall(1.0F, 0.5F).setMinMaxHeight(0.5F, 1.0F);
    public static BiomeGenBase valiantBiome = (new ValiantBiome(52)).setColor(10489616).setBiomeName("Valiant Biome").setDisableRain().setTemperatureRainfall(1.0F, 0.5F).setMinMaxHeight(0.2F, 1.0F);
    public static Item hardcoreHelmet;
    public static Item hardcorePlate;
    public static Item hardcoreLegs;
    public static Item hardcoreBoots;
    public static Item valiantHelmet;
    public static Item valiantPlate;
    public static Item valiantLegs;
    public static Item valiantBoots;
    public static Item angelWingsArmor;
    public static final Block valiantEnd = (new ValiantiteOreEnd(163, hardcoreMaterial)).setHardness(3.0F).setResistance(2000.0F).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:valiantOreEnd").setUnlocalizedName("hardcoreNether:valiantOreEnd");
    public static final Item valiantShard = (new Item(3028)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:valiantShard").setUnlocalizedName("hardcoreNether:valiantShard");
    public static final Item valiantNugget = (new Item(3029)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:valiantNugget").setUnlocalizedName("hardcoreNether:valiantNugget");
    public static final Item valiantStone = (new ValiantStone(3030)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:valiantStone").setUnlocalizedName("hardcoreNether:valiantStone");
    public static final Block valiantBlock = (new Block(164, hardcoreMaterial)).setHardness(2.2F).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:valiantBlock").setUnlocalizedName("hardcoreNether:valiantBlock");
    public static final Item angelStaff = (new AngelStaff(3016)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:angelStaff").setUnlocalizedName("hardcoreNether:angelStaff");
    public static final Item angelStaffUse = (new AngelStaffUse(3017)).setTextureName("hardcoreNether:angelStaffUse").setUnlocalizedName("hardcoreNether:angelStaffUse");
    public static final Item demonStaff = (new DemonStaff(3021)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:demonStaff").setUnlocalizedName("hardcoreNether:demonStaff");
    public static final Item demonStaffUse = (new DemonStaffUse(3022)).setTextureName("hardcoreNether:demonStaff").setUnlocalizedName("hardcoreNether:demonStaff");
    public static Block fuserIdle;
    public static Block fuserActive;
    public static Block hardcoreStair = (new HardcoreStairs(167, hardcoreBlock, 0)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:hardcoreStair").setUnlocalizedName("hardcoreNether:hardcoreStair");
    public static Block valiantStair = (new ValiantStairs(168, valiantBlock, 0)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:valiantStair").setUnlocalizedName("hardcoreNether:valiantStair");
    public static final Item angelicGem = (new Item(3023)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:angelicOre").setUnlocalizedName("hardcoreNether:angelicOre");
    public static final Item angelicBow = (new AngelicBow(3024)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:AngelBow").setUnlocalizedName("hardcoreNether:AngelBow");
    public static final Item holyArrow = (new Item(3025)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:holyArrow").setUnlocalizedName("hardcoreNether:holyArrow");
    public static HardcorePortal hardcorePortal = (HardcorePortal)(new HardcorePortal(255)).setHardness(-1.0F).setTextureName("hardcoreNether:HardcorePortal").setUnlocalizedName("hardcoreNether:HardcorePortal");
    public static final Item portalPlacer = (new PortalPlacer(3027)).setTextureName("hardcoreNether:hardcorePortalMake").setUnlocalizedName("hardcoreNether:hardcorePortalMake");
    public static final Item hardcorePick = (new HardcorePickaxe(3031, hardcore)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:hardcorePick").setUnlocalizedName("hardcoreNether:hardcorePick");
    public static final Item hardcoreSpade = (new HardcoreSpade(3032, hardcore)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:hardcoreSpade").setUnlocalizedName("hardcoreNether:hardcoreSpade");
    public static final Item hardcoreAxe = (new HardcoreAxe(3033, hardcore)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:hardcoreAxe").setUnlocalizedName("hardcoreNether:hardcoreAxe");
    public static final Item hardcoreHoe = (new HardcoreHoe(3034, hardcore)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:hardcoreHoe").setUnlocalizedName("hardcoreNether:hardcoreHoe");
    public static final Item valiantSword = (new ValiantSword(3039, valiant)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:valiantSword").setUnlocalizedName("hardcoreNether:valiantSword");
    public static final Item valiantPick = (new ValiantPickaxe(3040, valiant)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:valiantPick").setUnlocalizedName("hardcoreNether:valiantPick");
    public static final Item valiantSpade = (new ValiantSpade(3041, valiant)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:valiantSpade").setUnlocalizedName("hardcoreNether:valiantSpade");
    public static final Item valiantAxe = (new ValiantAxe(3042, valiant)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:valiantAxe").setUnlocalizedName("hardcoreNether:valiantAxe");
    public static final Item valiantHoe = (new ValiantHoe(3043, valiant)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:valiantHoe").setUnlocalizedName("hardcoreNether:valiantHoe");
    public static final Item hardcoreWoodSword = (new HardcoreWoodSword(3046, hardcoreWood)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:hardcoreMixWood").setUnlocalizedName("hardcoreNether:hardcoreMixWood");
    public static final Item hardcoreStoneSword = (new HardcoreStoneSword(3047, hardcoreStone)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:hardcoreMixStone").setUnlocalizedName("hardcoreNether:hardcoreMixStone");
    public static final Item hardcoreGoldSword = (new HardcoreGoldSword(3048, hardcoreGold)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:hardcoreMixGold").setUnlocalizedName("hardcoreNether:hardcoreMixGold");
    public static final Item hardcoreIronSword = (new HardcoreIronSword(3049, hardcoreIron)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:hardcoreMix").setUnlocalizedName("hardcoreNether:hardcoreMix");
    public static final Item hardcoreDiamondSword = (new HardcoreDiamondSword(3050, hardcoreDiamond)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:hardcoreMixDiamond").setUnlocalizedName("hardcoreNether:hardcoreMixDiamond");
    public static final Item fuser = (new Item(3052)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:fuser").setUnlocalizedName("hardcoreNether:fuser");
    public static final Item valiantCoin = (new Item(3057)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:valiantCoin").setUnlocalizedName("hardcoreNether:valiantCoin");
    public static Block DemonicTNT;
    public static Item book1 = (new Book1(3090)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:journal").setUnlocalizedName("hardcoreNether:journal1");
    public static Item demonSoundtrack = (new HCRecord(3091, "Demons")).setCreativeTab(CreativeTabs.tabMisc).setTextureName("hardcoreNether:demonRecord").setUnlocalizedName("hardcoreNether:demonRecord");
    public static Item electricSoundtrack = (new HCRecord(3092, "Electricity")).setCreativeTab(CreativeTabs.tabMisc).setTextureName("hardcoreNether:electricRecord").setUnlocalizedName("hardcoreNether:electricRecord");
    public static Item ambushSoundtrack = (new HCRecord(3094, "Ambush")).setCreativeTab(CreativeTabs.tabMisc).setTextureName("hardcoreNether:ambushRecord").setUnlocalizedName("hardcoreNether:ambushRecord");
    public static Item valiantBone = (new Item(3093)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:valiantBone").setUnlocalizedName("hardcoreNether:valiantBone");
    public static Item heartOfRagnad = (new Item(3095)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:heartOfRagnad").setUnlocalizedName("hardcoreNether:heartOfRagnad");
    public static final Item encryption1 = (new ItemEncryption1(3053)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:encryptedBook").setUnlocalizedName("hardcoreNether:encryptedBook");
    public static final Item encryptedSword1 = (new HardcoreEncrypt1Sword(3054, hardcore)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:hardcoreEncrypt1Sword").setUnlocalizedName("hardcoreNether:hardcoreEncrypt1Sword");
    public static final Item encryptedSword2 = (new HardcoreEncrypt2Sword(3055, hardcore)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:hardcoreEncrypt2Sword").setUnlocalizedName("hardcoreNether:hardcoreEncrypt2Sword");
    public static final Item encryptedSword3 = (new HardcoreEncrypt3Sword(3056, hardcore)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:hardcoreEncrypt3Sword").setUnlocalizedName("hardcoreNether:hardcoreEncrypt3Sword");
    public static final Item encryptedSword4 = (new HardcoreEncrypt4Sword(3084, valiant)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:valiantSword").setUnlocalizedName("hardcoreNether:hardcoreEncrypt4Sword");
    public static final Item encryptedSword5 = (new HardcoreEncrypt5Sword(3085, valiant)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:valiantSword").setUnlocalizedName("hardcoreNether:hardcoreEncrypt5Sword");
    public static final Item encryptedSword6 = (new HardcoreEncrypt6Sword(3086, valiant)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:valiantSword").setUnlocalizedName("hardcoreNether:hardcoreEncrypt6Sword");
    public static Item antiWitherBottle = (new AntiWitherPotion(3087)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:antiWitherPotion").setUnlocalizedName("hardcoreNether:antiWitherPotion");
    public static Item angelWings = (new Item(3088)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:angelWing").setUnlocalizedName("hardcoreNether:angelWings");
    public static Block fuserBlock = (new Block(177, hardcoreMaterial)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:fuserBlock").setUnlocalizedName("hardcoreNether:fuserBlock");
    public static ValiantPortal valiantPortal = (ValiantPortal)(new ValiantPortal(254)).setHardness(-1.0F).setTextureName("hardcoreNether:ValiantPortal").setUnlocalizedName("hardcoreNether:ValiantPortal");
    public static Block hardcoreFence = (new BlockFence(169, "hardcoreNether:hardcoreFence", hardcoreMaterial)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:hardcoreFence").setUnlocalizedName("hardcoreNether:hardcoreFence");
    public static Block valiantFence = (new BlockFence(174, "hardcoreNether:valiantFence", hardcoreMaterial)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:valiantFence").setUnlocalizedName("hardcoreNether:valiantFence");
    public static final Item holyLighter = (new HolyLighter(3045)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:holyLight");
    public static HolyFire IceFire = (HolyFire)(new HolyFire(165)).setTextureName("hardcoreNether:fire").setUnlocalizedName("hardcoreNether:fire");
    public static final Item corruptedSword = (new ItemSword(3071, corrupted)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:corruptedSword").setUnlocalizedName("hardcoreNether:corruptedSword");
    public static final Item corruptedPick = (new ItemPickaxe(3072, corrupted)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:corruptedPick").setUnlocalizedName("hardcoreNether:corruptedPick");
    public static final Item corruptedSpade = (new ItemSpade(3073, corrupted)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:corruptedSpade").setUnlocalizedName("hardcoreNether:corruptedSpade");
    public static final Item corruptedAxe = (new ItemAxe(3074, corrupted)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:corruptedAxe").setUnlocalizedName("hardcoreNether:corruptedAxe");
    public static final Item corruptedHoe = (new ItemHoe(3075, corrupted)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:corruptedHoe").setUnlocalizedName("hardcoreNether:corruptedHoe");
    public static Block corruptedGrass = (new BloodSoil(179, hardcoreMaterial)).setHardness(0.6F).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:corruptedSoil_side").setUnlocalizedName("hardcoreNether:corruptedSoil_side");
    public static Block PPFroster = (new PowerPouchFroster(187)).setUnlocalizedName("hardcoreNether:PPFroster");
    public static Block PPHeater = (new PowerPouchHeater(188)).setUnlocalizedName("hardcoreNether:PPHeater");
    public static Item corruptedOre = (new CorruptedOre(3063)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:corruptedIngot").setUnlocalizedName("hardcoreNether:corruptedIngot");
    public static Item blood = (new Item(3064)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:blood").setUnlocalizedName("hardcoreNether:blood");
    public static final Item hardcoreBrain = (new Item(3059)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:hostBrain").setUnlocalizedName("hardcoreNether:hostBrain");
    public static final Item hardcoreHand = (new Item(3060)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:hostHand").setUnlocalizedName("hardcoreNether:hostHand");
    public static final Item hardcoreEye = (new Item(3061)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:hostEye").setUnlocalizedName("hardcoreNether:hostEye");
    public static Block hostScroll = (new BlockScroll(189)).setCreativeTab(HardcoreTab).setUnlocalizedName("hardcoreNether:hostScroll");
    public static Block hostScrollDecoded = (new BlockScroll(190)).setCreativeTab(HardcoreTab).setUnlocalizedName("hardcoreNether:hostScrollDecoded");
    public static final Achievement HardcoreBeginnings = (new Achievement(2002, "HardcoreBeginnings", 0, 0, hardcoreOre, (Achievement)null)).setIndependent().registerAchievement();
    public static final Achievement valiantAchievement = (new Achievement(2001, "valiant", 2, 0, valiantNugget, HardcoreBeginnings)).registerAchievement();
    public static final Achievement corruptAchievement = (new Achievement(2003, "corrupt", 4, 0, corruptedOre, valiantAchievement)).registerAchievement();
    public static final Achievement soulSeller = (new Achievement(2004, "soul", 0, -2, hardcoreEye, (Achievement)null)).setIndependent().registerAchievement();
    public static final Achievement spamalot = (new Achievement(2005, "spamalot", 2, -2, valiantSword, soulSeller)).registerAchievement();
    public static final Achievement summoner = (new Achievement(2006, "summon", 0, 2, hostScroll, (Achievement)null)).setIndependent().registerAchievement();
    public static final Achievement bossKill = (new Achievement(2009, "killRagnad", 2, 2, antiWitherBottle, summoner)).registerAchievement();
    public static final Achievement bloodAchievement = (new Achievement(2010, "blood", 6, 0, blood, corruptAchievement)).registerAchievement();
    public static final Achievement who = (new Achievement(2011, "who", 0, -4, book1, (Achievement)null)).registerAchievement();
    public static final Achievement frosterAchievement = (new Achievement(2012, "froster", 0, 4, PPFroster, (Achievement)null)).registerAchievement();
    public static final Achievement heaterAchievement = (new Achievement(2013, "heater", 2, 4, PPHeater, frosterAchievement)).registerAchievement();
    public static AchievementPage hardcoreNether = new AchievementPage("HardcoreNether", new Achievement[] {HardcoreBeginnings, valiantAchievement, corruptAchievement, bloodAchievement, soulSeller, spamalot, summoner, bossKill, who, frosterAchievement, heaterAchievement});
    public static Item throwableValiantite = (new ValiantiteStar(3044)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:valiantStar").setUnlocalizedName("hardcoreNether:valiantStar");
    public static final Item enderRing = (new HardcoreRing(3058)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:enderRing").setUnlocalizedName("hardcoreNether:enderRing");
    public static Block corruptedBlock = (new CorruptionBlock(180, hardcoreMaterial)).setHardness(0.9F).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:corruptedBlock").setUnlocalizedName("hardcoreNether:corruptedBlock");
    public static Block corruptedAlter = (new CorruptionAlter(181, hardcoreMaterial)).setHardness(2.0F).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:corruptedAlter_side").setUnlocalizedName("hardcoreNether:corruptedAlter_side");
    public static Block bloodBlock = (new Block(182, hardcoreMaterial)).setHardness(0.9F).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:blockBlood").setUnlocalizedName("hardcoreNether:blockBlood");
    public static Item templete = (new Item(3065)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:template").setUnlocalizedName("hardcoreNether:template");
    public static Item templeteSword = (new Item(3066)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:templateSword").setUnlocalizedName("hardcoreNether:templateSword");
    public static Item templetePick = (new Item(3067)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:templatePick").setUnlocalizedName("hardcoreNether:templatePick");
    public static Item templeteHoe = (new Item(3068)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:templateHoe").setUnlocalizedName("hardcoreNether:templateHoe");
    public static Item templeteAxe = (new Item(3069)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:templateAxe").setUnlocalizedName("hardcoreNether:templateAxe");
    public static Item templeteSpade = (new Item(3070)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:templateSpade").setUnlocalizedName("hardcoreNether:templateSpade");
    public static BiomeGenBase corruptedBiome = (new CorruptedBiome(54)).setColor(10489616).setBiomeName("Corrupted Plains").setDisableRain().setTemperatureRainfall(1.0F, 0.5F).setMinMaxHeight(0.2F, 0.4F);
    public static Item corruptedHelmet;
    public static Item corruptedPlate;
    public static Item corruptedLegs;
    public static Item corruptedBoots;
    public static Item charmOfExp = (new XPCharm(3062)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:XPCharm").setUnlocalizedName("hardcoreNether:XPCharm");
    public static Item dataPad = (new Item(3081)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:dataPad").setUnlocalizedName("hardcoreNether:dataPad");
    public static Item conductiveIron = (new Item(3080)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:tintedIron").setUnlocalizedName("hardcoreNether:tintedIron");
    public static Item frosterAddon = (new Item(3082)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:froster").setUnlocalizedName("hardcoreNether:frosterAddon");
    public static Item heaterAddon = (new Item(3083)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:heater").setUnlocalizedName("hardcoreNether:heaterAddon");
    public static Block powerPouch = (new PoweredPouch(183, false)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:hardcoreBlock").setUnlocalizedName("hardcoreNether:PowerPouch");
    public static Block poweredPouch = (new PoweredPouch(184, true)).setTextureName("hardcoreNether:hardcoreBlock").setUnlocalizedName("hardcoreNether:PowerPouchPowered");
    public static Block corruptTorchOn = (new CorruptTorch(185)).setTextureName("hardcoreNether:torch").setCreativeTab(CreativeTabs.tabAllSearch).setLightValue(1.0F).setUnlocalizedName("hardcoreNether:Torch");
    public static Block corruptTorchOff = (new CorruptTorch(186)).setCreativeTab(HardcoreTab).setUnlocalizedName("hardcoreNether:TorchOff").setLightValue(0.0F).setTextureName("hardcoreNether:torch");
    public static Potion antiWitherPotion;
    public static Block HCLog = (new BlockHCLog(200)).setUnlocalizedName("hardcoreNether:HCWood");
    public static Block HCLeaves = (new BlockHCLeaves(201)).setUnlocalizedName("hardcoreNether:HCLeaves");
    public static Block hardcoreSapling = (new BlockHardcoreSapling(202)).setUnlocalizedName("hardcoreNether:hardcoreSapling").setTextureName("hardcoreNether:hardcoreSapling");

    public static Block woodeniteOre = new Block(203, Material.rock).setUnlocalizedName("hardcoreNether:woodeniteOre").setTextureName("hardcoreNether:woodeniteOre");
    public static Block steniteOre = new Block(204, Material.rock).setUnlocalizedName("hardcoreNether:steniteOre").setTextureName("hardcoreNether:steniteOre");
    public static Block goldeniteOre = new Block(205, Material.rock).setUnlocalizedName("hardcoreNether:goldeniteOre").setTextureName("hardcoreNether:goldeniteOre");
    public static Block ironiteOre = new Block(206, Material.rock).setUnlocalizedName("hardcoreNether:ironiteOre").setTextureName("hardcoreNether:ironiteOre");
    public static Block crystalliteOre = new Block(207, Material.rock).setUnlocalizedName("hardcoreNether:crystalliteOre").setTextureName("hardcoreNether:crystalliteOre");
    
    
    public void addAchievementName(String par1String, String par2String)
    {
        LanguageRegistry.instance().addStringLocalization("achievement." + par1String + "en_US", par2String);
    }

    public void addAchievementDesc(String par1String, String par2String)
    {
        LanguageRegistry.instance().addStringLocalization("achievement." + par1String + ".desc", "en_US", par2String);
    }

    @EventHandler
    public void preInit(FMLPreInitializationEvent e)
    {
        proxy.registerRenderThings();
        VillagerRegistry.instance().registerVillageTradeHandler(2, new HolyTradeHandler());
        VillagerRegistry.instance().registerVillagerId(20);
        proxy.registerVillagerStuff();
        proxy.registerSounds();
        VillagerRegistry.instance().registerVillageTradeHandler(20, new SatanTradeHandler());
        Potion[] potionTypes = null;
        Field[] arr$ = Potion.class.getDeclaredFields();
        int len$ = arr$.length;

        for (int i$ = 0; i$ < len$; ++i$)
        {
            Field f = arr$[i$];
            f.setAccessible(true);

            try
            {
                if (f.getName().equals("potionTypes") || f.getName().equals("potionTypes"))
                {
                    Field error = Field.class.getDeclaredField("modifiers");
                    error.setAccessible(true);
                    error.setInt(f, f.getModifiers() & -17);
                    potionTypes = (Potion[])((Potion[])f.get((Object)null));
                    Potion[] newPotionTypes = new Potion[256];
                    System.arraycopy(potionTypes, 0, newPotionTypes, 0, potionTypes.length);
                    f.set((Object)null, newPotionTypes);
                }
            }
            catch (Exception var9)
            {
                System.err.println("Severe error, please report this to the mod author:");
                System.err.println(var9);
            }
        }

        MinecraftForge.EVENT_BUS.register(new AntiWitherEffect());
    }

    @EventHandler
    public void Init(FMLInitializationEvent e)
    {
        ChestGenHooks.getInfo("dungeonChest").addItem(new WeightedRandomChestContent(new ItemStack(charmOfExp), 1, 2, 1));
        ChestGenHooks.getInfo("strongholdCorridor").addItem(new WeightedRandomChestContent(new ItemStack(enderRing), 1, 2, 1));
        
        hardcoreHelmet = (new HardcoreArmor(3012, hardcoreArmor, proxy.addArmor("hardcoreArmor"), 0)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:hardcoreHelmet").setUnlocalizedName("hardcoreNether:hardcoreHelmet");
        hardcorePlate = (new HardcoreArmor(3013, hardcoreArmor, proxy.addArmor("hardcoreArmor"), 1)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:hardcorePlate").setUnlocalizedName("hardcoreNether:hardcorePlate");
        hardcoreLegs = (new HardcoreArmor(3014, hardcoreArmor, proxy.addArmor("hardcoreArmor"), 2)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:hardcoreLegs").setUnlocalizedName("hardcoreNether:hardcoreLegs");
        hardcoreBoots = (new HardcoreArmor(3015, hardcoreArmor, proxy.addArmor("hardcoreArmor"), 3)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:hardcoreBoots").setUnlocalizedName("hardcoreNether:hardcoreBoots");
        valiantHelmet = (new ValiantArmor(3035, ValiantArmor, proxy.addArmor("Valiant"), 0)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:valiantHelmet").setUnlocalizedName("hardcoreNether:valiantHelmet");
        valiantPlate = (new ValiantArmor(3036, ValiantArmor, proxy.addArmor("Valiant"), 1)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:valiantPlate").setUnlocalizedName("hardcoreNether:valiantPlate");
        valiantLegs = (new ValiantArmor(3037, ValiantArmor, proxy.addArmor("Valiant"), 2)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:valiantLegs").setUnlocalizedName("hardcoreNether:valiantLegs");
        valiantBoots = (new ValiantArmor(3038, ValiantArmor, proxy.addArmor("Valiant"), 3)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:valiantBoots").setUnlocalizedName("hardcoreNether:valiantBoots");
        corruptedHelmet = (new CorruptedArmor(3076, corruptArmor, proxy.addArmor("Corrupted"), 0)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:corruptedHelmet").setUnlocalizedName("hardcoreNether:corruptedHelmet");
        corruptedPlate = (new CorruptedArmor(3077, corruptArmor, proxy.addArmor("Corrupted"), 1)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:corruptedPlate").setUnlocalizedName("hardcoreNether:corruptedPlate");
        corruptedLegs = (new CorruptedArmor(3078, corruptArmor, proxy.addArmor("Corrupted"), 2)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:corruptedLegs").setUnlocalizedName("hardcoreNether:corruptedLegs");
        corruptedBoots = (new CorruptedArmor(3079, corruptArmor, proxy.addArmor("Corrupted"), 3)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:corruptedBoots").setUnlocalizedName("hardcoreNether:corruptedBoots");
        angelWingsArmor = (new AngelArmor(3089, angelArmor, proxy.addArmor("Angel"), 1)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:angelicArmor").setUnlocalizedName("hardcoreNether:angelicArmor");
        fuserIdle = (new TutFurnace(175, false)).setCreativeTab(HardcoreTab);
        fuserActive = new TutFurnace(176, true);
        DemonicTNT = (new DemonicTNT(178)).setCreativeTab(HardcoreTab).setTextureName("hardcoreNether:DemonicTNT_Side").setUnlocalizedName("hardcoreNether:DemonicTNT_Side");
        
        MinecraftForge.setToolClass(hardcoreSword, "sword", 4);
        MinecraftForge.setToolClass(hardcorePick, "pickaxe", 4);
        MinecraftForge.setToolClass(hardcoreSpade, "shovel", 4);
        MinecraftForge.setToolClass(hardcoreAxe, "axe", 4);
        MinecraftForge.setToolClass(hardcoreHoe, "hoe", 4);
        MinecraftForge.setToolClass(valiantSword, "sword", 8);
        MinecraftForge.setToolClass(valiantPick, "pickaxe", 8);
        MinecraftForge.setToolClass(valiantSpade, "shovel", 8);
        MinecraftForge.setToolClass(valiantAxe, "axe", 8);
        MinecraftForge.setToolClass(valiantHoe, "hoe", 8);
        MinecraftForge.setToolClass(hardcoreWoodSword, "sword", 5);
        MinecraftForge.setToolClass(hardcoreStoneSword, "sword", 6);
        MinecraftForge.setToolClass(hardcoreGoldSword, "sword", 7);
        MinecraftForge.setToolClass(hardcoreIronSword, "sword", 8);
        MinecraftForge.setToolClass(hardcoreDiamondSword, "sword", 9);
        MinecraftForge.setToolClass(encryptedSword1, "sword", 4);
        MinecraftForge.setToolClass(encryptedSword2, "sword", 4);
        MinecraftForge.setToolClass(encryptedSword3, "sword", 4);
        MinecraftForge.setToolClass(hardcoreDiamondSword, "sword", 4);
        
        GameRegistry.registerBlock(DemonicTNT, "DemonicTNT");
        GameRegistry.registerBlock(flamingrack, "flamingNetherrack");
        GameRegistry.registerBlock(hardcoreSapling, "hardcoreSapling");
        GameRegistry.registerItem(encryption1, "encryption");
        GameRegistry.registerItem(valiantCoin, "valiantCoin");
        GameRegistry.registerBlock(hardcoreFence, "hardcoreFence");
        GameRegistry.registerBlock(valiantFence, "valiantFence");
        GameRegistry.registerBlock(fuserIdle, "fuserIdle");
        GameRegistry.registerBlock(fuserActive, "fuserActive");
        GameRegistry.registerItem(hardcoreOre, "hardcoreOre");
        GameRegistry.registerItem(angelStaff, "angelStaff");
        GameRegistry.registerItem(demonicGem, "demonOre");
        GameRegistry.registerItem(encryptedSword1, "encrypted1");
        GameRegistry.registerItem(hardcoreWoodSword, "hardcoreMixWood");
        GameRegistry.registerItem(heartOfRagnad, "heartOfRagnad");
        GameRegistry.registerItem(hardcoreStoneSword, "hardcoreMixStone");
        GameRegistry.registerItem(demonSoundtrack, "demonRecord");
        GameRegistry.registerItem(electricSoundtrack, "electricRecord");
        GameRegistry.registerItem(ambushSoundtrack, "ambushRecord");
        GameRegistry.registerItem(hardcoreGoldSword, "hardcoreMixGold");
        GameRegistry.registerItem(hardcoreIronSword, "hardcoreMix");
        GameRegistry.registerItem(hardcoreDiamondSword, "hardcoreMixDiamond");
        GameRegistry.registerItem(hardcoreSword, "hardcoreSword");
        GameRegistry.registerItem(hardcorePick, "hardcorePick");
        GameRegistry.registerItem(valiantBone, "valiantBone");
        GameRegistry.registerBlock(demonicCobbleWall, "demonWall");
        GameRegistry.registerBlock(angelicCobbleWall, "angelWall");
        GameRegistry.registerItem(hardcoreSpade, "hardcoreSpade");
        GameRegistry.registerItem(hardcoreAxe, "hardcoreAxe");
        GameRegistry.registerItem(hardcoreHoe, "hardcoreHoe");
        GameRegistry.registerItem(hardcoreHelmet, "hardcoreHelmet");
        GameRegistry.registerItem(hardcorePlate, "hardcorePlate");
        GameRegistry.registerItem(hardcoreLegs, "hardcoreLegs");
        GameRegistry.registerItem(hardcoreBoots, "hardcoreBoots");
        GameRegistry.registerItem(throwableValiantite, "throwVal");
        GameRegistry.registerItem(angelStaffUse, "angelStaffUse");
        GameRegistry.registerItem(demonStaff, "demonStaff");
        GameRegistry.registerItem(demonStaffUse, "demonStaffUse");
        GameRegistry.registerItem(angelicBow, "angelBow");
        GameRegistry.registerItem(angelWings, "angelWings");
        GameRegistry.registerItem(book1, "book1");
        GameRegistry.registerItem(holyArrow, "holyArrow");
        GameRegistry.registerItem(charmOfExp, "xpCharm");
        GameRegistry.registerBlock(summonerChest, "phonyChest");
        GameRegistry.registerBlock(demonicCobbleMossy, "mossyDemonCobble");
        GameRegistry.registerItem(antiWitherBottle, "antiWither");
        GameRegistry.registerItem(stamina, "stamina");
        GameRegistry.registerItem(encryptedSword1, "encryptedSword1");
        GameRegistry.registerItem(encryptedSword2, "encryptedSword2");
        GameRegistry.registerItem(encryptedSword3, "encryptedSword3");
        GameRegistry.registerItem(angelWingsArmor, "angelWingsArmor");
        GameRegistry.registerItem(angelicGem, "angelicOre");
        GameRegistry.registerBlock(hardcorePortal, "hardcorePortal");
        GameRegistry.registerBlock(hardcoreStair, "hardcoreStairs");
        GameRegistry.registerBlock(HCLog, "hardcoreLog");
        GameRegistry.registerBlock(HCLeaves, "hardcoreLeaves");
        GameRegistry.registerBlock(valiantStair, "valiantStairs");
        GameRegistry.registerBlock(fuserBlock, "fuserBlock");
        GameRegistry.registerBlock(PPFroster, "froster");
        GameRegistry.registerBlock(PPHeater, "heater");
        GameRegistry.registerItem(portalPlacer, "portalPlacer");
        GameRegistry.registerItem(holyLighter, "holyLighter");
        GameRegistry.registerItem(valiantShard, "valiantShard");
        GameRegistry.registerItem(valiantNugget, "valiantNugget");
        GameRegistry.registerItem(valiantStone, "valiantStone");
        GameRegistry.registerItem(valiantHelmet, "valiantHelmet");
        GameRegistry.registerItem(valiantPlate, "valiantPlate");
        GameRegistry.registerItem(fuser, "fuser");
        GameRegistry.registerItem(valiantLegs, "valiantLegs");
        GameRegistry.registerItem(valiantBoots, "valiantBoots");
        GameRegistry.registerItem(valiantSword, "valiantSword");
        GameRegistry.registerItem(valiantPick, "valiantPick");
        GameRegistry.registerItem(valiantSpade, "valiantSpade");
        GameRegistry.registerItem(valiantAxe, "valiantAxe");
        GameRegistry.registerItem(valiantHoe, "valiantHoe");
        GameRegistry.registerBlock(valiantBlock, "valiantBlock");
        GameRegistry.registerBlock(valiantEnd, "valiantEnd");
        GameRegistry.registerBlock(valiantEarth, "valiantEarth");
        GameRegistry.registerBlock(angelicStone, "angelStone");
        GameRegistry.registerBlock(angelicCobble, "angelCobble");
        GameRegistry.registerBlock(angelicStoneBricks, "angelStoneBricks");
        GameRegistry.registerBlock(IceFire, "holyFire");
        GameRegistry.registerBlock(valiantPortal, "valiantPortal");
        GameRegistry.registerItem(enderRing, "enderRing");
        GameRegistry.registerItem(hardcoreEye, "hostEye");
        GameRegistry.registerItem(hardcoreHand, "hostHand");
        GameRegistry.registerItem(hardcoreBrain, "hostBrain");
        GameRegistry.registerBlock(corruptedGrass, "bloodSoil");
        GameRegistry.registerItem(corruptedOre, "corruptedOre");
        GameRegistry.registerBlock(corruptedBlock, "corruptedBlock");
        GameRegistry.registerItem(blood, "blood");
        GameRegistry.registerItem(templete, "template");
        GameRegistry.registerItem(templeteSword, "templateSword");
        GameRegistry.registerItem(templetePick, "templatePick");
        GameRegistry.registerItem(templeteHoe, "templateHoe");
        GameRegistry.registerItem(templeteAxe, "templateAxe");
        GameRegistry.registerItem(templeteSpade, "templateSpade");
        GameRegistry.registerBlock(bloodBlock, "blockBlood");
        GameRegistry.registerItem(corruptedSword, "corruptedSword");
        GameRegistry.registerItem(corruptedPick, "corruptedPick");
        GameRegistry.registerItem(corruptedSpade, "corruptedSpade");
        GameRegistry.registerItem(corruptedAxe, "corruptedAxe");
        GameRegistry.registerItem(corruptedHoe, "corruptedHoe");
        GameRegistry.registerBlock(powerPouch, "powerPouch");
        GameRegistry.registerBlock(poweredPouch, "poweredPouch");
        GameRegistry.registerBlock(corruptedAlter, "corruptAlter");
        GameRegistry.registerBlock(corruptTorchOn, "hardcoreTorch");
        GameRegistry.registerBlock(corruptTorchOff, "hardcoreOffTorch");
        GameRegistry.registerItem(dataPad, "datapad");
        GameRegistry.registerItem(conductiveIron, "tintedIron");
        GameRegistry.registerItem(frosterAddon, "frosterAddon");
        GameRegistry.registerItem(heaterAddon, "heaterAddon");
        GameRegistry.registerItem(encryptedSword4, "encryptedSword4");
        GameRegistry.registerItem(encryptedSword5, "encryptedSword5");
        GameRegistry.registerItem(encryptedSword6, "encryptedSword6");
        GameRegistry.registerBlock(hostScroll, "hostScroll");
        GameRegistry.registerBlock(hostScrollDecoded, "hostScrollDecoded");
        GameRegistry.registerBlock(demonicStone, "demonStone");
        GameRegistry.registerBlock(demonicCobble, "demonCobble");
        GameRegistry.registerBlock(demonicStoneBricks, "demonSBricks");
        
        LanguageRegistry.addName(angelWings, "Angelic Wings");
        LanguageRegistry.addName(HCLog, "Hardcore Wood");
        LanguageRegistry.addName(HCLeaves, "Hardcore Leaves");
        LanguageRegistry.addName(summonerChest, "Phony Chest");
        LanguageRegistry.addName(angelWingsArmor, "Angelic Wings");
        LanguageRegistry.addName(heartOfRagnad, "Heart Of Ragnad");
        LanguageRegistry.addName(antiWitherBottle, "Potion of Anti-Decay");
        LanguageRegistry.addName(enderRing, EnumChatFormatting.DARK_PURPLE + "Ring of the Tunneler");
        LanguageRegistry.addName(hardcoreBlock, "Hardcore Block");
        LanguageRegistry.addName(hardcoreBlock, "Hardcore Block");
        LanguageRegistry.addName(flamingrack, "Flaming Netherrack");
        LanguageRegistry.addName(hardcoreOre, "Hardcore Ingot");
        LanguageRegistry.addName(demonStaffUse, "Demonic Staff");
        LanguageRegistry.addName(hardcoreSword, EnumChatFormatting.RED + "Hardcore Sword");
        LanguageRegistry.addName(hardcorePick, "Hardcore Pickaxe");
        LanguageRegistry.addName(hardcoreSpade, "Hardcore Shovel");
        LanguageRegistry.addName(hardcoreSapling, "Demonic Sapling");
        LanguageRegistry.addName(demonicCobbleWall, "Demonic Cobblestone Wall");
        LanguageRegistry.addName(angelicCobbleWall, "Angelic Cobblestone Wall");
        LanguageRegistry.addName(hardcoreAxe, "Hardcore Axe");
        LanguageRegistry.addName(valiantBone, "Valiantite Bone");
        LanguageRegistry.addName(hardcoreHoe, "Hardcore Hoe");
        LanguageRegistry.addName(powerPouch, "Power Pouch");
        LanguageRegistry.addName(demonSoundtrack, "Music Disc");
        LanguageRegistry.addName(electricSoundtrack, "Music Disc");
        LanguageRegistry.addName(ambushSoundtrack, "Music Disc");
        LanguageRegistry.addName(poweredPouch, "Power Pouch");
        LanguageRegistry.addName(corruptTorchOn, "Hardcore Torch");
        LanguageRegistry.addName(corruptTorchOff, "Hardcore Torch");
        LanguageRegistry.addName(dataPad, "Power Pouch Data Pad");
        LanguageRegistry.addName(conductiveIron, "Conductive Iron");
        LanguageRegistry.addName(frosterAddon, "Froster Addon for Power Pouch");
        LanguageRegistry.addName(heaterAddon, "Heater Addon for Power Pouch");
        LanguageRegistry.addName(encryptedSword4, EnumChatFormatting.LIGHT_PURPLE + "Valiantite Sword");
        LanguageRegistry.addName(encryptedSword5, EnumChatFormatting.LIGHT_PURPLE + "Valiantite Sword");
        LanguageRegistry.addName(encryptedSword6, EnumChatFormatting.LIGHT_PURPLE + "Valiantite Sword");
        LanguageRegistry.addName(hostScroll, EnumChatFormatting.DARK_RED + "Hfnnlmrmt Ivtmzw");
        LanguageRegistry.addName(hostScrollDecoded, EnumChatFormatting.DARK_RED + "Summoning Ragnad");
        LanguageRegistry.addName(demonicStone, "Demonic Stone");
        LanguageRegistry.addName(demonicCobble, "Demonic Cobblestone");
        LanguageRegistry.addName(demonicCobbleMossy, "Mossy Demonic Cobblestone");
        LanguageRegistry.addName(demonicStoneBricks, "Demonic Stone Bricks");
        LanguageRegistry.addName(angelicStone, "Angelic Stone");
        LanguageRegistry.addName(angelicCobble, "Angelic Cobblestone");
        LanguageRegistry.addName(angelicStoneBricks, "Angelic Stone Bricks");
        LanguageRegistry.addName(stamina, "Stamina");
        LanguageRegistry.addName(bloodBlock, "Block of Blood");
        LanguageRegistry.addName(book1, "Journal #1");
        LanguageRegistry.instance().addStringLocalization("achievement.HardcoreBeginnings", "Hardcore Beginnings!");
        LanguageRegistry.instance().addStringLocalization("achievement.HardcoreBeginnings.desc", "Smelt a Hardcore Block Into an Ingot!");
        LanguageRegistry.instance().addStringLocalization("achievement.valiant", "I am Valiant");
        LanguageRegistry.instance().addStringLocalization("achievement.valiant.desc", "Obtain a Valiantite Nugget");
        LanguageRegistry.instance().addStringLocalization("achievement.corrupt", "Not Corrupted!");
        LanguageRegistry.instance().addStringLocalization("achievement.corrupt.desc", "Obtain a Corrupted Ingot");
        LanguageRegistry.instance().addStringLocalization("achievement.blood", "Blood and Gore!");
        LanguageRegistry.instance().addStringLocalization("achievement.blood.desc", "Obtain some blood");
        LanguageRegistry.instance().addStringLocalization("achievement.soul", "Soul Seller");
        LanguageRegistry.instance().addStringLocalization("achievement.soul.desc", "Trade with a demon");
        LanguageRegistry.instance().addStringLocalization("achievement.spamalot", "Spamalot");
        LanguageRegistry.instance().addStringLocalization("achievement.spamalot.desc", "Trade with a solider");
        LanguageRegistry.instance().addStringLocalization("achievement.summon", "Summoner");
        LanguageRegistry.instance().addStringLocalization("achievement.summon.desc", "Summon Ragnad");
        LanguageRegistry.instance().addStringLocalization("achievement.killRagnad", "Finish Him");
        LanguageRegistry.instance().addStringLocalization("achievement.killRagnad.desc", "Kill Ragnad once and for all!");
        LanguageRegistry.instance().addStringLocalization("achievement.who", "Who?");
        LanguageRegistry.instance().addStringLocalization("achievement.who.desc", "Obtain the first journal");
        LanguageRegistry.instance().addStringLocalization("achievement.froster", "Arctic Breeze");
        LanguageRegistry.instance().addStringLocalization("achievement.froster.desc", "Freeze the ground below you with a froster");
        LanguageRegistry.instance().addStringLocalization("achievement.heater", "Electric Blanket");
        LanguageRegistry.instance().addStringLocalization("achievement.heater.desc", "Heat the ground below you with a heater");
        LanguageRegistry.instance().addStringLocalization("itemGroup.Hardcore Nether by Mitchellbrine", "Hardcore Nether by Mitchellbrine");
        LanguageRegistry.instance().addStringLocalization("potion.antiwither", "Anti-Wither");
        LanguageRegistry.instance().addStringLocalization("hardcoreNether:ost.demons", "Demons");
        LanguageRegistry.addName(hardcoreHelmet, "Helmet of Herobrine");
        LanguageRegistry.addName(hardcorePlate, "Shirt of the Styx");
        LanguageRegistry.addName(hardcoreLegs, "Pants of Persephone");
        LanguageRegistry.addName(hardcoreBoots, "Feet of Satan");
        LanguageRegistry.addName(valiantHelmet, "The Angel\'s Halo");
        LanguageRegistry.addName(valiantPlate, "The Knight\'s Chain");
        LanguageRegistry.addName(valiantLegs, "The Hunter\'s Pants");
        LanguageRegistry.addName(valiantBoots, "The Feet of the Angel");
        LanguageRegistry.addName(angelStaff, EnumChatFormatting.GOLD + "Angelic Staff");
        LanguageRegistry.addName(angelStaffUse, EnumChatFormatting.GOLD + "Angelic Staff");
        LanguageRegistry.addName(demonicGem, "Demonic Gem");
        LanguageRegistry.addName(demonStaff, EnumChatFormatting.DARK_GRAY + "Demonic Staff");
        LanguageRegistry.addName(angelicGem, "Angelic Gem");
        LanguageRegistry.addName(angelicBow, "Angelic Bow");
        LanguageRegistry.addName(holyArrow, "Holy Arrow");
        LanguageRegistry.addName(hardcorePortal, "Portal");
        LanguageRegistry.addName(new ItemStack(encryption1, 1, 0), EnumChatFormatting.OBFUSCATED + "Fire Aspect");
        LanguageRegistry.addName(new ItemStack(encryption1, 1, 1), EnumChatFormatting.OBFUSCATED + "Knockback");
        LanguageRegistry.addName(new ItemStack(encryption1, 1, 2), EnumChatFormatting.OBFUSCATED + "Looting");
        LanguageRegistry.addName(portalPlacer, TRD);
        LanguageRegistry.addName(encryptedSword1, EnumChatFormatting.RED + "Hardcore Sword");
        LanguageRegistry.addName(encryptedSword2, EnumChatFormatting.RED + "Hardcore Sword");
        LanguageRegistry.addName(encryptedSword3, EnumChatFormatting.RED + "Hardcore Sword");
        LanguageRegistry.addName(valiantShard, "Valiantite Shard");
        LanguageRegistry.addName(valiantNugget, "Valiantite Nugget");
        LanguageRegistry.addName(valiantStone, "Valiantite Stone");
        LanguageRegistry.addName(valiantBlock, "Valiantite Block");
        LanguageRegistry.addName(valiantEnd, "Valiantite Ore");
        LanguageRegistry.addName(valiantEarth, "Valiantite Ore");
        LanguageRegistry.addName(valiantSword, EnumChatFormatting.GOLD + "Valiantite Sword");
        LanguageRegistry.addName(valiantPick, "Valiantite Pickaxe");
        LanguageRegistry.addName(valiantSpade, "Valiantite Shovel");
        LanguageRegistry.addName(valiantAxe, "Valiantite Axe");
        LanguageRegistry.addName(valiantHoe, "Valiantite Hoe");
        LanguageRegistry.addName(IceFire, "Holy Fire");
        LanguageRegistry.addName(hardcoreFence, "Hardcore Fence");
        LanguageRegistry.addName(valiantFence, "Valiantite Fence");
        LanguageRegistry.addName(throwableValiantite, "Valiantite Ninja Star");
        LanguageRegistry.addName(fuserIdle, "Fuser");
        LanguageRegistry.addName(fuserActive, "Fuser");
        LanguageRegistry.addName(valiantCoin, EnumChatFormatting.LIGHT_PURPLE + "Valiant Coin");
        LanguageRegistry.addName(hardcoreWoodSword, "Hardcore-Wood Sword");
        LanguageRegistry.addName(hardcoreStoneSword, "Hardcore-Stone Sword");
        LanguageRegistry.addName(hardcoreGoldSword, "Hardcore-Gold Sword");
        LanguageRegistry.addName(hardcoreIronSword, "Hardcore-Iron Sword");
        LanguageRegistry.addName(hardcoreDiamondSword, "Hardcore-Diamond Sword");
        LanguageRegistry.addName(hardcoreEye, EnumChatFormatting.DARK_RED + "Eye of the Host");
        LanguageRegistry.addName(hardcoreHand, EnumChatFormatting.DARK_RED + "Hand of the Host");
        LanguageRegistry.addName(hardcoreBrain, EnumChatFormatting.DARK_RED + "Brain of the Host");
        LanguageRegistry.addName(charmOfExp, EnumChatFormatting.GOLD + "Charm of Experience");
        LanguageRegistry.addName(corruptedGrass, "Corrupted Soil");
        LanguageRegistry.addName(corruptedOre, "Corrupted Ingot");
        LanguageRegistry.addName(corruptedBlock, "Block of Corruption");
        LanguageRegistry.addName(blood, EnumChatFormatting.DARK_RED + "Blood");
        LanguageRegistry.addName(corruptedAlter, EnumChatFormatting.DARK_RED + "Corruption Alter");
        LanguageRegistry.addName(templete, "Blank Template");
        LanguageRegistry.addName(templeteSword, "Sword Template");
        LanguageRegistry.addName(templetePick, "Pickaxe Template");
        LanguageRegistry.addName(templeteHoe, "Hoe Template");
        LanguageRegistry.addName(templeteAxe, "Axe Template");
        LanguageRegistry.addName(templeteSpade, "Shovel Template");
        LanguageRegistry.addName(corruptedHelmet, "Helmet of Corruption");
        LanguageRegistry.addName(corruptedPlate, "Plate of Destruction");
        LanguageRegistry.addName(corruptedLegs, "Legs of Crooks");
        LanguageRegistry.addName(corruptedBoots, "Feet of Evil");
        LanguageRegistry.addName(corruptedSword, EnumChatFormatting.DARK_RED + "Corrupted Sword");
        LanguageRegistry.addName(corruptedPick, "Corrupted Pickaxe");
        LanguageRegistry.addName(corruptedSpade, "Corrupted Shovel");
        LanguageRegistry.addName(corruptedAxe, "Corrupted Axe");
        LanguageRegistry.addName(corruptedHoe, "Corrupted Hoe");
        LanguageRegistry.addName(holyLighter, "Holy Lighter");
        LanguageRegistry.addName(hardcoreStair, "Hardcore Stairs");
        LanguageRegistry.addName(valiantStair, "Valiantite Stairs");
        LanguageRegistry.addName(fuser, "Fuser");
        LanguageRegistry.addName(fuserBlock, "Fuser Block");
        LanguageRegistry.addName(valiantPortal, "Portal");
        LanguageRegistry.addName(DemonicTNT, "Demonic Explosives");
        LanguageRegistry.addName(PPFroster, "Power Pouch w/ Froster");
        LanguageRegistry.addName(PPHeater, "Power Pouch w/ Heater");
        LanguageRegistry.instance().addStringLocalization("entity.hardcoreNether.HolyArrow.name", "Holy Arrow");
        LanguageRegistry.instance().addStringLocalization("entity.hardcoreNether.DemonicTNT.name", "DemonicTNT");
        
        GameRegistry.registerWorldGenerator(new WorldGeneratorFlame());
        
        EntityRegistry.registerModEntity(EntityHardcorePigZombie.class, "HardcorePigZombie", 1, this, 80, 3, true);
        EntityRegistry.registerModEntity(EntityHardcoreCreeper.class, "HardcoreCreeper", 1, this, 80, 3, true);
        EntityRegistry.registerModEntity(EntityBrain.class, "Brain", 2, this, 80, 3, true);
        EntityRegistry.registerModEntity(EntityHand.class, "Hand", 3, this, 80, 3, true);
        EntityRegistry.registerModEntity(EntityDemonicTNT.class, "DemonicTNT", 4, this, 128, 1, true);
        EntityRegistry.registerModEntity(EntitySolder.class, "Solder", 5, this, 80, 3, true);
        EntityRegistry.registerModEntity(EntityDemon.class, "Demon", 6, this, 80, 3, true);
        EntityRegistry.registerModEntity(EntityHost.class, "Host", 7, this, 80, 3, true);
        EntityRegistry.registerModEntity(EntityHostStage2.class, "Host2", 8, this, 80, 3, true);
        EntityRegistry.registerModEntity(EntityHostStage3.class, "Host3", 9, this, 80, 3, true);
        EntityRegistry.registerModEntity(EntityAngelicPig.class, "AngelPig", 10, this, 80, 3, true);
        EntityRegistry.registerModEntity(EntityScientist.class, "Scientist", 11, this, 80, 3, true);
        EntityRegistry.registerModEntity(EntityDemonicWolf.class, "demonicWolf", 12, this, 80, 3, true);
        
        EntityRegistry.addSpawn(EntityHardcorePigZombie.class, 8, 2, 8, EnumCreatureType.monster, new BiomeGenBase[] {BiomeGenBase.hell, hardcoreBiome});
        EntityRegistry.addSpawn(EntityZombie.class, 10, 2, 8, EnumCreatureType.monster, new BiomeGenBase[] {BiomeGenBase.hell});
        EntityRegistry.addSpawn(EntityCreeper.class, 10, 2, 8, EnumCreatureType.monster, new BiomeGenBase[] {BiomeGenBase.hell});
        EntityRegistry.addSpawn(EntitySpider.class, 10, 2, 8, EnumCreatureType.monster, new BiomeGenBase[] {BiomeGenBase.hell});
        EntityRegistry.addSpawn(EntitySkeleton.class, 10, 2, 8, EnumCreatureType.monster, new BiomeGenBase[] {BiomeGenBase.hell});
        EntityRegistry.addSpawn(EntityEnderman.class, 10, 2, 8, EnumCreatureType.monster, new BiomeGenBase[] {BiomeGenBase.hell});
        EntityRegistry.addSpawn(EntityWitch.class, 10, 2, 8, EnumCreatureType.monster, new BiomeGenBase[] {BiomeGenBase.hell});
        EntityRegistry.addSpawn(EntityHardcoreCreeper.class, 10, 2, 8, EnumCreatureType.monster, new BiomeGenBase[] {BiomeGenBase.hell, hardcoreBiome});
        EntityRegistry.addSpawn(EntitySolder.class, 3, 1, 1, EnumCreatureType.ambient, new BiomeGenBase[] {valiantBiome});
        EntityRegistry.addSpawn(EntityDemon.class, 3, 1, 1, EnumCreatureType.ambient, new BiomeGenBase[] {hardcoreBiome});
        EntityRegistry.addSpawn(EntityAngelicPig.class, 10, 2, 8, EnumCreatureType.ambient, new BiomeGenBase[] {valiantBiome});
        
        EntityRegistry.removeSpawn(EntitySheep.class, EnumCreatureType.creature, new BiomeGenBase[] {hardcoreBiome});
        EntityRegistry.removeSpawn(EntityCow.class, EnumCreatureType.creature, new BiomeGenBase[] {hardcoreBiome});
        EntityRegistry.removeSpawn(EntityPig.class, EnumCreatureType.creature, new BiomeGenBase[] {hardcoreBiome});
        EntityRegistry.removeSpawn(EntityChicken.class, EnumCreatureType.creature, new BiomeGenBase[] {hardcoreBiome});
        
        GameRegistry.registerPickupHandler(new PickupHandler());
        
        AchievementPage.registerAchievementPage(hardcoreNether);
       
        GameRegistry.registerBlock(hardcoreBlock, "hardcoreBlock");
        
        EntityRegistry.registerGlobalEntityID(EntityHolyArrow.class, "HolyArrow", EntityRegistry.findGlobalUniqueEntityId());
        
        DimensionManager.registerProviderType(dimension, WorldProviderHardcore.class, false);
        DimensionManager.registerProviderType(dimensionValiant, WorldProviderValiant.class, false);
        DimensionManager.registerDimension(dimension, dimension);
        DimensionManager.registerDimension(dimensionValiant, dimensionValiant);
        
        GameRegistry.addBiome(corruptedBiome);
        
        GameRegistry.registerCraftingHandler(new HardcoreCraftingHandler());
        
        GameRegistry.addShapelessRecipe(new ItemStack(hardcoreOre, 1), new Object[] {stamina});
        
        GameRegistry.addRecipe(new ItemStack(hardcoreSword, 1), new Object[] {" X ", " X ", " Y ", 'X', hardcoreOre, 'Y', Item.stick});
        GameRegistry.addRecipe(new ItemStack(hardcorePick, 1), new Object[] {"XXX", " Y ", " Y ", 'X', hardcoreOre, 'Y', Item.stick});
        GameRegistry.addRecipe(new ItemStack(hardcoreSpade, 1), new Object[] {" X ", " Y ", " Y ", 'X', hardcoreOre, 'Y', Item.stick});
        GameRegistry.addRecipe(new ItemStack(hardcoreAxe, 1), new Object[] {"XX ", "XY ", " Y ", 'X', hardcoreOre, 'Y', Item.stick});
        GameRegistry.addRecipe(new ItemStack(hardcoreAxe, 1), new Object[] {" XX", " YX", " Y ", 'X', hardcoreOre, 'Y', Item.stick});
        GameRegistry.addRecipe(new ItemStack(hardcoreHoe, 1), new Object[] {" XX", " Y ", " Y ", 'X', hardcoreOre, 'Y', Item.stick});
        GameRegistry.addRecipe(new ItemStack(hardcoreHoe, 1), new Object[] {"XX ", " Y ", " Y ", 'X', hardcoreOre, 'Y', Item.stick});
        GameRegistry.addShapedRecipe(new ItemStack(conductiveIron, 1), new Object[] {"XY", 'X', Item.ingotIron, 'Y', hardcoreOre});
        GameRegistry.addRecipe(new ItemStack(valiantNugget, 2), new Object[] {"XX", 'X', valiantStone});
        GameRegistry.addRecipe(new ItemStack(valiantShard, 2), new Object[] {"XX", 'X', valiantNugget});
        GameRegistry.addSmelting(flamingrack.blockID, new ItemStack(hardcoreOre, 1), 0.5F);
        GameRegistry.addSmelting(demonicCobble.blockID, new ItemStack(demonicStone, 1), 0.5F);
        GameRegistry.addSmelting(angelicCobble.blockID, new ItemStack(angelicStone, 1), 0.5F);
        GameRegistry.addRecipe(new ItemStack(angelStaff, 1), new Object[] {"Y Y", "YXY", " X ", 'X', Item.stick, 'Y', angelWings});
        GameRegistry.addRecipe(new ItemStack(hardcoreBlock, 1), new Object[] {"XXX", "XXX", "XXX", 'X', hardcoreOre});
        GameRegistry.addRecipe(new ItemStack(demonicGem, 1), new Object[] {"YYY", "YXY", "YYY", 'X', Item.emerald, 'Y', hardcoreOre});
        GameRegistry.addRecipe(new ItemStack(angelicGem, 1), new Object[] {"YYY", "YXY", "YYY", 'X', Item.emerald, 'Y', Item.ingotGold});
        GameRegistry.addRecipe(new ItemStack(demonStaff, 1), new Object[] {"Y Y", "YZY", " X ", 'X', Item.stick, 'Y', Item.feather, 'Z', demonicGem});
        GameRegistry.addRecipe(new ItemStack(angelicBow, 1), new Object[] {" #X", "# X", " #X", '#', angelicGem, 'X', Item.silk});
        GameRegistry.addRecipe(new ItemStack(holyArrow, 4), new Object[] {" X ", " Y ", " Z ", 'X', Item.flint, 'Y', Item.ingotGold, 'Z', Item.feather});
        GameRegistry.addShapelessRecipe(new ItemStack(valiantNugget, 1), new Object[] {valiantShard});
        GameRegistry.addShapelessRecipe(new ItemStack(valiantStone, 1), new Object[] {valiantNugget});
        
        LanguageRegistry.instance().addStringLocalization("entity.hardcoreNether.HardcorePigZombie.name", "Hardcore Pig Zombie");
        LanguageRegistry.instance().addStringLocalization("entity.hardcoreNether.HardcoreCreeper.name", "Hardcore Pig Zombie Scout");
        LanguageRegistry.instance().addStringLocalization("entity.hardcoreNether.Brain.name", "The Brain");
        LanguageRegistry.instance().addStringLocalization("entity.hardcoreNether.Hand.name", "The Hand");
        LanguageRegistry.instance().addStringLocalization("entity.hardcoreNether.Solder.name", "Valiantite Solider");
        LanguageRegistry.instance().addStringLocalization("entity.hardcoreNether.Demon.name", "Demon");
        LanguageRegistry.instance().addStringLocalization("entity.hardcoreNether.AngelPig.name", "Angelic Pig");
        LanguageRegistry.instance().addStringLocalization("entity.hardcoreNether.Scientist.name", "Corrupted Scientist");
        LanguageRegistry.instance().addStringLocalization("entity.hardcoreNether.Host.name", EnumChatFormatting.DARK_RED + "Ragnad");
        LanguageRegistry.instance().addStringLocalization("entity.hardcoreNether.Host2.name", EnumChatFormatting.DARK_RED + "Ragnad");
        LanguageRegistry.instance().addStringLocalization("entity.hardcoreNether.Host3.name", EnumChatFormatting.DARK_RED + "Ragnad");
        LanguageRegistry.instance().addStringLocalization("entity.hardcoreNether.demonicWolf.name", "Demonic Dog");
        LanguageRegistry.instance().addStringLocalization("entity.HardcorePigZombie.name", "Hardcore Pig Zombie");
        LanguageRegistry.instance().addStringLocalization("entity.HardcoreCreeper.name", "Hardcore Pig Zombie Scout");
        LanguageRegistry.instance().addStringLocalization("entity.Brain.name", "The Brain");
        LanguageRegistry.instance().addStringLocalization("entity.Hand.name", "The Hand");
        LanguageRegistry.instance().addStringLocalization("entity.Solder.name", "Valiantite Solider");
        LanguageRegistry.instance().addStringLocalization("entity.Demon.name", "Demon");
        LanguageRegistry.instance().addStringLocalization("entity.Host.name", "Hardcore Host");
        
        registerEntityEgg(EntityHardcorePigZombie.class, 15373203, 5009705);
        registerEntityEgg(EntityHardcoreCreeper.class, 15373203, 5009705);
        registerEntityEgg(EntityBrain.class, 15771042, 14377823);
        registerEntityEgg(EntityHand.class, 16382457, 12369084);
        registerEntityEgg(EntitySolder.class, 15771042, 0);
        registerEntityEgg(EntityDemon.class, 10489616, 0);
        registerEntityEgg(EntityHost.class, 8073150, 10489616);
        registerEntityEgg(EntityAngelicPig.class, 15771042, 16382457);
        registerEntityEgg(EntityScientist.class, 10592673, 10489616);
        registerEntityEgg(EntityDemonicWolf.class, 14144467, 13545366);
        
        GameRegistry.addRecipe(new ItemStack(valiantHelmet, 1), new Object[] {"XXX", "X X", 'X', new ItemStack(valiantStone, 1)});
        GameRegistry.addRecipe(new ItemStack(valiantPlate, 1), new Object[] {"X X", "XXX", "XXX", 'X', new ItemStack(valiantStone, 1)});
        GameRegistry.addRecipe(new ItemStack(valiantLegs, 1), new Object[] {"XXX", "X X", "X X", 'X', new ItemStack(valiantStone, 1)});
        GameRegistry.addRecipe(new ItemStack(powerPouch, 1), new Object[] {"XXX", "XYX", "XXX", 'X', conductiveIron, 'Y', Item.redstoneRepeater});
        GameRegistry.addShapedRecipe(new ItemStack(corruptTorchOff, 4), new Object[] {"X  ", "Y  ", 'X', angelicGem, 'Y', hardcoreOre});
        GameRegistry.addRecipe(new ItemStack(valiantSword, 1), new Object[] {" X ", " X ", " Y ", 'X', valiantStone, 'Y', Item.stick});
        GameRegistry.addRecipe(new ItemStack(valiantPick, 1), new Object[] {"XXX", " Y ", " Y ", 'X', valiantStone, 'Y', Item.stick});
        GameRegistry.addRecipe(new ItemStack(valiantSpade, 1), new Object[] {" X ", " Y ", " Y ", 'X', valiantStone, 'Y', Item.stick});
        GameRegistry.addRecipe(new ItemStack(valiantAxe, 1), new Object[] {"XX ", "XY ", " Y ", 'X', valiantStone, 'Y', Item.stick});
        GameRegistry.addRecipe(new ItemStack(valiantAxe, 1), new Object[] {" XX", " YX", " Y ", 'X', valiantStone, 'Y', Item.stick});
        GameRegistry.addRecipe(new ItemStack(valiantHoe, 1), new Object[] {" XX", " Y ", " Y ", 'X', valiantStone, 'Y', Item.stick});
        GameRegistry.addRecipe(new ItemStack(valiantHoe, 1), new Object[] {"XX ", " Y ", " Y ", 'X', valiantStone, 'Y', Item.stick});
        GameRegistry.addRecipe(new ItemStack(valiantBlock, 1), new Object[] {"XXX", "XXX", "XXX", 'X', valiantStone});
        GameRegistry.addRecipe(new ItemStack(corruptedHelmet, 1), new Object[] {"XXX", "X X", 'X', new ItemStack(corruptedOre)});
        GameRegistry.addRecipe(new ItemStack(corruptedPlate, 1), new Object[] {"X X", "XXX", "XXX", 'X', new ItemStack(corruptedOre)});
        GameRegistry.addRecipe(new ItemStack(corruptedBoots, 1), new Object[] {"A A", "A A", 'A', new ItemStack(corruptedOre)});
        GameRegistry.addRecipe(new ItemStack(corruptedLegs, 1), new Object[] {"XXX", "X X", "X X", 'X', new ItemStack(corruptedOre)});
        GameRegistry.addRecipe(new ItemStack(corruptedBlock, 1), new Object[] {"XXX", "XXX", "XXX", 'X', corruptedOre});
        GameRegistry.addRecipe(new ItemStack(demonicStoneBricks, 1), new Object[] {"XX", "XX", 'X', demonicStone});
        GameRegistry.addRecipe(new ItemStack(angelicStoneBricks, 1), new Object[] {"XX", "XX", 'X', angelicStone});
        GameRegistry.addShapelessRecipe(new ItemStack(corruptedOre, 1), new Object[] {blood});
        GameRegistry.addShapelessRecipe(new ItemStack(corruptedOre, 9), new Object[] {corruptedBlock});
        GameRegistry.addShapelessRecipe(new ItemStack(blood, 9), new Object[] {bloodBlock});
        GameRegistry.addRecipe(new ItemStack(portalPlacer, 1), new Object[] {"ZXA", " Y ", " Y ", 'Y', new ItemStack(Item.dyePowder, 1, 4), 'X', Block.blockIron, 'Z', hardcoreBlock, 'A', valiantBlock});
        GameRegistry.addRecipe(new ItemStack(Item.monsterPlacer, 1, 203), new Object[] {"YYY", "YXY", "YYY", 'X', new ItemStack(Item.egg, 1), 'Y', valiantStone});
        GameRegistry.addRecipe(new ItemStack(Item.monsterPlacer, 1, 204), new Object[] {"YYY", "YXY", "YYY", 'X', new ItemStack(Item.egg, 1), 'Y', hardcoreOre});
        GameRegistry.addRecipe(new ItemStack(throwableValiantite, 16), new Object[] {" Y ", "YYY", " Y ", 'Y', valiantShard});
        GameRegistry.addRecipe(new ItemStack(holyLighter, 1), new Object[] {"A ", " B", 'A', Item.ingotGold, 'B', angelicGem});
        GameRegistry.addRecipe(new ItemStack(hardcoreFence, 2), new Object[] {"###", "###", '#', hardcoreOre});
        GameRegistry.addRecipe(new ItemStack(valiantFence, 2), new Object[] {"###", "###", '#', valiantStone});
        GameRegistry.addRecipe(new ItemStack(fuser, 1), new Object[] {"  A", " B ", "A  ", 'A', Item.diamond, 'B', valiantStone});
        GameRegistry.addRecipe(new ItemStack(valiantCoin, 4), new Object[] {"AAA", "AAA", 'A', valiantNugget});
        GameRegistry.addRecipe(new ItemStack(fuser, 1), new Object[] {"A  ", " B ", "  A", 'A', Item.diamond, 'B', valiantStone});
        GameRegistry.addRecipe(new ItemStack(fuserBlock, 1), new Object[] {"BBB", "BAB", "BBB", 'A', fuser, 'B', hardcoreBlock});
        GameRegistry.addRecipe(new ItemStack(fuserIdle, 1), new Object[] {"BBB", "BAB", "BBB", 'A', fuser, 'B', fuserBlock});
        GameRegistry.addRecipe(new ItemStack(angelWingsArmor, 1), new Object[] {"XYX", 'X', angelWings, 'Y', Item.silk});
        GameRegistry.addShapedRecipe(new ItemStack(antiWitherBottle, 1), new Object[] {"XYZ", 'X', new ItemStack(Item.skull, 1, 1), 'Y', new ItemStack(Item.potion, 1, 16), 'Z', Item.fermentedSpiderEye});
        GameRegistry.addRecipe(new ItemStack(hostScroll, 1), new Object[] {"EBE", "HPH", 'E', hardcoreEye, 'B', hardcoreBrain, 'H', hardcoreHand, 'P', Item.paper});
        GameRegistry.addShapedRecipe(new ItemStack(hostScrollDecoded, 1), new Object[] {"XY", 'X', hostScroll, 'Y', stamina});
        GameRegistry.addRecipe(new ItemStack(encryption1, 1, 0), new Object[] {"XXX", "XZY", "XXX", 'X', hardcoreOre, 'Y', valiantStone, 'Z', new ItemStack(Item.dyePowder, 1, 1)});
        GameRegistry.addRecipe(new ItemStack(encryption1, 1, 1), new Object[] {"XXX", "XZY", "XXX", 'X', hardcoreOre, 'Y', valiantStone, 'Z', Item.bone});
        GameRegistry.addRecipe(new ItemStack(encryption1, 1, 2), new Object[] {"XXX", "XZY", "XXX", 'X', hardcoreOre, 'Y', valiantStone, 'Z', Item.fermentedSpiderEye});
        GameRegistry.addRecipe(new ItemStack(bloodBlock, 1), new Object[] {"XXX", "XXX", "XXX", 'X', blood});
        GameRegistry.addRecipe(new ItemStack(DemonicTNT, 1), new Object[] {"XXX", "XYX", "XXX", 'X', Block.tnt, 'Y', demonicGem});
        GameRegistry.addRecipe(new ItemStack(dataPad, 1), new Object[] {"XXX", "XYX", "XXX", 'X', conductiveIron, 'Y', Item.redstone});
        GameRegistry.addRecipe(new ItemStack(frosterAddon, 1), new Object[] {"XZX", "ZYZ", "XZX", 'X', Block.blockSnow, 'Y', dataPad, 'Z', Block.ice});
        GameRegistry.addRecipe(new ItemStack(heaterAddon, 1), new Object[] {"XXX", "XYX", "XXX", 'X', Block.hardenedClay, 'Y', dataPad});
        GameRegistry.addShapedRecipe(new ItemStack(PPFroster, 1), new Object[] {"XY", 'X', frosterAddon, 'Y', powerPouch});
        GameRegistry.addShapedRecipe(new ItemStack(PPHeater, 1), new Object[] {"XY", 'X', heaterAddon, 'Y', powerPouch});
        GameRegistry.addRecipe(new ItemStack(corruptedAlter, 1), new Object[] {"XYX", "XZX", "XXX", 'X', corruptedBlock, 'Y', Block.dispenser, 'Z', Item.netherStar});
        GameRegistry.addRecipe(new ItemStack(demonicCobbleWall, 6), new Object[] {"XXX", "XXX", 'X', demonicCobble});
        GameRegistry.addRecipe(new ItemStack(angelicCobbleWall, 6), new Object[] {"XXX", "XXX", 'X', angelicCobble});
        
        this.addFuserRecipe(Item.swordWood, hardcoreWoodSword, true);
        this.addFuserRecipe(Item.swordStone, hardcoreStoneSword, true);
        this.addFuserRecipe(Item.swordGold, hardcoreGoldSword, true);
        this.addFuserRecipe(Item.swordIron, hardcoreIronSword, true);
        this.addFuserRecipe(Item.swordDiamond, hardcoreDiamondSword, true);
        this.addFuserRecipeWithMetadata(new ItemStack(encryption1, 1, 0), new ItemStack(encryptedSword1), true);
        this.addFuserRecipeWithMetadata(new ItemStack(encryption1, 1, 1), new ItemStack(encryptedSword2), true);
        this.addFuserRecipeWithMetadata(new ItemStack(encryption1, 1, 2), new ItemStack(encryptedSword3), true);
        this.addFuserRecipeWithMetadata(new ItemStack(encryption1, 1, 0), new ItemStack(encryptedSword4), false);
        this.addFuserRecipeWithMetadata(new ItemStack(encryption1, 1, 1), new ItemStack(encryptedSword5), false);
        this.addFuserRecipeWithMetadata(new ItemStack(encryption1, 1, 2), new ItemStack(encryptedSword6), false);
        
        GameRegistry.registerTileEntity(TileEntityTutFurnace.class, "tileentitytutfurnace");
        GameRegistry.registerTileEntity(TileEntityCharm.class, "charmBlock");
        GameRegistry.registerTileEntity(TileEntityFroster.class, "tileEntityFroster");
        GameRegistry.registerTileEntity(TileEntityHeater.class, "tileEntityHeater");
        GameRegistry.registerTileEntity(TileEntityScroll.class, "scrolls");
        
        
        GameRules rules = new GameRules();
        
        rules.addGameRule("fallDamage", "true");
        
        proxy.registerTickHandlers();
        
        addDispenserBehavior(holyLighter, new DispenserBehaviorLighter());
        
        antiWitherPotion = (new HCPotions(32, false, 0)).setIconIndex(1, 2).setPotionName("potion.antiwither");
    }

    @EventHandler
    public void PostInit(FMLPostInitializationEvent e) {}

    public static int getUniqueEntityId()
    {
        do
        {
            ++startEntityId;
        }
        while (EntityList.getStringFromID(startEntityId) != null);

        return startEntityId;
    }

    public static void registerEntityEgg(Class <? extends Entity > entity, int primaryColor, int secondaryColor)
    {
        int id = getUniqueEntityId();
        EntityList.IDtoClassMapping.put(Integer.valueOf(id), entity);
        EntityList.entityEggs.put(Integer.valueOf(id), new EntityEggInfo(id, primaryColor, secondaryColor));
    }

    public void AddRenderer(Map map)
    {
        map.put(EntityValiantite.class, new RenderSnowball(throwableValiantite));
    }

    @EventHandler
    public void serverLoad(FMLServerStartingEvent event)
    {
        event.registerServerCommand(new StaminaCommand());
        System.out.print("Entity Constructed");
        MinecraftForge.EVENT_BUS.register(new EventHandler_Stamina());
    }

    public static void addDispenserBehavior(Item item, IBehaviorDispenseItem behavior)
    {
        BlockDispenser.dispenseBehaviorRegistry.putObject(item, behavior);
    }

    public void addFuserRecipe(Item into, Item out, boolean hardcore)
    {
        if (hardcore)
        {
            GameRegistry.addShapedRecipe(new ItemStack(out, 1), new Object[] {"XYZ", 'X', into, 'Y', fuser, 'Z', hardcoreOre});
        }
        else
        {
            GameRegistry.addShapedRecipe(new ItemStack(out, 1), new Object[] {"XYZ", 'X', into, 'Y', fuser, 'Z', valiantStone});
        }
    }

    public void addFuserRecipeWithMetadata(ItemStack into, ItemStack out, boolean hardcore)
    {
        if (hardcore)
        {
            GameRegistry.addShapedRecipe(out, new Object[] {"XYZ", 'X', into, 'Y', fuser, 'Z', hardcoreOre});
        }
        else
        {
            GameRegistry.addShapedRecipe(out, new Object[] {"XYZ", 'X', into, 'Y', fuser, 'Z', valiantStone});
        }
    }
}

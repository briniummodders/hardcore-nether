package mc.Mitchellbrine.HardcoreNether.common.command;

import java.util.ArrayList;
import java.util.List;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumChatFormatting;

public class StaminaCommand implements ICommand
{
    private List aliases = new ArrayList();

    public StaminaCommand()
    {
        this.aliases.add("stam");
        this.aliases.add("s");
    }
    
    public int getRequiredPermissionLevel()
    {
        return 2;
    }

    public int compareTo(Object arg0)
    {
        return 0;
    }

    public String getCommandName()
    {
        return "stamina";
    }

    public String getCommandUsage(ICommandSender icommandsender)
    {
        return "/stamina <add/remove> <amount>";
    }

    public List getCommandAliases()
    {
        return this.aliases;
    }

    public void processCommand(ICommandSender icommandsender, String[] astring)
    {
        if (astring.length == 0)
        {
            ((EntityPlayer)icommandsender).addChatMessage(EnumChatFormatting.DARK_RED + "Not Enough Arguments!");
        }
        else
        {
            if (astring[0].equalsIgnoreCase("add"))
            {
                if (astring[1].equalsIgnoreCase("20"))
                {
                    MainMod.Special += 10;
                }
                else if (astring[1].equalsIgnoreCase("40"))
                {
                    MainMod.Special += 20;
                }
                else if (astring[1].equalsIgnoreCase("60"))
                {
                    MainMod.Special += 30;
                }
                else if (astring[1].equalsIgnoreCase("80"))
                {
                    MainMod.Special += 40;
                }
                else if (astring[1].equalsIgnoreCase("100"))
                {
                    MainMod.Special += 50;
                }
                else
                {
                    ((EntityPlayer)icommandsender).addChatMessage(EnumChatFormatting.DARK_RED + "You can only add stamina by 20/40/60/80/100");
                }
            }
            else if (astring[0].equalsIgnoreCase("remove"))
            {
                if (astring[1].equalsIgnoreCase("20"))
                {
                    MainMod.Special -= 10;
                }
                else if (astring[1].equalsIgnoreCase("40"))
                {
                    MainMod.Special -= 20;
                }
                else if (astring[1].equalsIgnoreCase("60"))
                {
                    MainMod.Special -= 30;
                }
                else if (astring[1].equalsIgnoreCase("80"))
                {
                    MainMod.Special -= 40;
                }
                else if (astring[1].equalsIgnoreCase("100"))
                {
                    MainMod.Special -= 50;
                }
                else
                {
                    ((EntityPlayer)icommandsender).addChatMessage(EnumChatFormatting.DARK_RED + "You can only remove stamina by 20/40/60/80/100");
                }
            }
            else
            {
                ((EntityPlayer)icommandsender).addChatMessage(EnumChatFormatting.DARK_RED + "Invalid Arguments!");
            }
        }
    }

    /**
     * Returns true if the given command sender is allowed to use this command.
     */
    public boolean canCommandSenderUseCommand(ICommandSender icommandsender)
    {
        return icommandsender.canCommandSenderUseCommand(this.getRequiredPermissionLevel(), "stamina");
    }

    /**
     * Adds the strings available in this command to the given list of tab completion options.
     */
    public List addTabCompletionOptions(ICommandSender icommandsender, String[] astring)
    {
        return null;
    }

    /**
     * Return whether the specified command parameter index is a username parameter.
     */
    public boolean isUsernameIndex(String[] astring, int i)
    {
        return false;
    }
}

package mc.Mitchellbrine.HardcoreNether.common.item;

import net.minecraft.potion.Potion;

public class HCPotions extends Potion
{
    public HCPotions(int par1, boolean par2, int par3)
    {
        super(par1, par2, par3);
    }

    /**
     * Sets the index for the icon displayed in the player's inventory when the status is active.
     */
    public Potion setIconIndex(int par1, int par2)
    {
        super.setIconIndex(par1, par2);
        return this;
    }
}

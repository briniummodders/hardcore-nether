package mc.Mitchellbrine.HardcoreNether.common.item;

import java.util.List;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class DemonStaff extends Item
{
    public DemonStaff(int par1)
    {
        super(par1);
        this.setMaxDamage(200);
    }

    public boolean hitEntity(ItemStack par1ItemStack, EntityLiving par2EntityLiving, EntityLiving par3EntityLiving)
    {
        par1ItemStack.damageItem(5, par3EntityLiving);
        return true;
    }

    /**
     * allows items to add custom lines of information to the mouseover description
     */
    public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
    {
        par3List.add("Stamina Power: Demonic Possession");
    }

    /**
     * Called whenever this item is equipped and the right mouse button is pressed. Args: itemStack, world, entityPlayer
     */
    public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
        par3EntityPlayer.setItemInUse(par1ItemStack, this.getMaxItemUseDuration(par1ItemStack));

        if (MainMod.Special == 40)
        {
            par3EntityPlayer.addPotionEffect(new PotionEffect(Potion.damageBoost.id, 1200, 2));
            par3EntityPlayer.addPotionEffect(new PotionEffect(Potion.fireResistance.id, 1200, 2));
            par3EntityPlayer.addPotionEffect(new PotionEffect(Potion.digSlowdown.id, 1200, 2));
            par3EntityPlayer.setCurrentItemOrArmor(0, new ItemStack(MainMod.demonStaffUse, 1));
            MainMod.Special = 0;
        }

        if (MainMod.Special == 0 && !par3EntityPlayer.worldObj.isRemote)
        {
            par3EntityPlayer.addChatMessage("You need more stamina to use this item\'s special!");
        }

        return par1ItemStack;
    }

    /**
     * Returns True is the item is renderer in full 3D when hold.
     */
    public boolean isFull3D()
    {
        return true;
    }
}

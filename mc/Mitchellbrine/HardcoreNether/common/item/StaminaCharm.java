package mc.Mitchellbrine.HardcoreNether.common.item;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class StaminaCharm extends Item
{
    public StaminaCharm(int par1)
    {
        super(par1);
    }

    /**
     * Called whenever this item is equipped and the right mouse button is pressed. Args: itemStack, world, entityPlayer
     */
    public ItemStack onItemRightClick(ItemStack itemstack, World world, EntityPlayer player)
    {
        NBTTagCompound nbt = new NBTTagCompound();

        if (!world.isRemote)
        {
            nbt.setInteger("Special", MainMod.Special);
            System.out.println("Stamina Saved!");
        }
        else
        {
            nbt.getInteger("Special");
        }

        return itemstack;
    }
}

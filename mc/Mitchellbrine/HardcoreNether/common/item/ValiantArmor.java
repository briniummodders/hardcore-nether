package mc.Mitchellbrine.HardcoreNether.common.item;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumArmorMaterial;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class ValiantArmor extends ItemArmor
{
    public ValiantArmor(int par1, EnumArmorMaterial par2EnumArmorMaterial, int par3, int par4)
    {
        super(par1, par2EnumArmorMaterial, par3, par4);
    }

    public String getArmorTexture(ItemStack itemstack, Entity entity, int slot, int layer)
    {
        return itemstack.toString().contains("valiantHelmet") ? "hardcoreNether:valiant_1.png" : (itemstack.toString().contains("valiantPlate") ? "hardcoreNether:valiant_1.png" : (itemstack.toString().contains("valiantBoots") ? "hardcoreNether:valiant_1.png" : (itemstack.toString().contains("valiantLegs") ? "hardcoreNether:valiant_2.png" : null)));
    }

    public void onArmorTickUpdate(World world, EntityPlayer player, ItemStack itemstack)
    {
        ItemStack boots = player.inventory.armorInventory[0];
        ItemStack legs = player.inventory.armorInventory[1];
        ItemStack chest = player.inventory.armorInventory[2];
        ItemStack helm = player.inventory.armorInventory[3];

        if (helm != null && helm.itemID == MainMod.valiantHelmet.itemID && boots != null && boots.itemID == MainMod.valiantBoots.itemID && chest != null && chest.itemID == MainMod.valiantPlate.itemID && legs != null && legs.itemID == MainMod.valiantLegs.itemID)
        {
            player.fallDistance = 0.0F;
            player.addPotionEffect(new PotionEffect(Potion.damageBoost.id, 1200, 5));
        }
    }
}

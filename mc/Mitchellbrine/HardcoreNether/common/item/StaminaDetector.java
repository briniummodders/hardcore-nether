package mc.Mitchellbrine.HardcoreNether.common.item;

import java.util.List;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

public class StaminaDetector extends Item
{
    public StaminaDetector(int par1)
    {
        super(par1);
    }

    /**
     * allows items to add custom lines of information to the mouseover description
     */
    public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
    {
        par3List.add(EnumChatFormatting.YELLOW + "WARNING: DEPRECATED!");
    }

    /**
     * Called whenever this item is equipped and the right mouse button is pressed. Args: itemStack, world, entityPlayer
     */
    public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
        par3EntityPlayer.setItemInUse(par1ItemStack, this.getMaxItemUseDuration(par1ItemStack));

        if (!par3EntityPlayer.worldObj.isRemote)
        {
            par3EntityPlayer.addChatMessage(EnumChatFormatting.DARK_PURPLE + "STAMINA IS CURRENTLY " + EnumChatFormatting.BOLD + MainMod.Special);
        }

        return par1ItemStack;
    }
}

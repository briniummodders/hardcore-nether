package mc.Mitchellbrine.HardcoreNether.common.item;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumArmorMaterial;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class AngelArmor extends ItemArmor
{
    public AngelArmor(int par1, EnumArmorMaterial par2EnumArmorMaterial, int par3, int par4)
    {
        super(par1, par2EnumArmorMaterial, par3, par4);
    }

    public String getArmorTexture(ItemStack itemstack, Entity entity, int slot, int layer)
    {
        return itemstack.toString().contains("angelWingsArmor") ? "hardcoreNether:corrupt_1.png" : null;
    }

    public void onArmorTickUpdate(World world, EntityPlayer player, ItemStack itemstack)
    {
        ItemStack boots = player.inventory.armorInventory[0];
        ItemStack legs = player.inventory.armorInventory[1];
        ItemStack chest = player.inventory.armorInventory[2];
        ItemStack helm = player.inventory.armorInventory[3];

        if (chest != null && chest.itemID == MainMod.angelWingsArmor.itemID && player.fallDistance >= 5.0F)
        {
            player.fallDistance = 0.0F;
        }
    }
}

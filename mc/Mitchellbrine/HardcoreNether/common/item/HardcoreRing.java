package mc.Mitchellbrine.HardcoreNether.common.item;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class HardcoreRing extends Item
{
    public HardcoreRing(int par1)
    {
        super(par1);
    }

    /**
     * Called whenever this item is equipped and the right mouse button is pressed. Args: itemStack, world, entityPlayer
     */
    public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
        if (par3EntityPlayer.isSneaking())
        {
            if (!par3EntityPlayer.worldObj.isRemote)
            {
                par2World.destroyBlock(par3EntityPlayer.getPlayerCoordinates().posX, par3EntityPlayer.getPlayerCoordinates().posY - 1, par3EntityPlayer.getPlayerCoordinates().posZ, true);
                par2World.destroyBlock(par3EntityPlayer.getPlayerCoordinates().posX, par3EntityPlayer.getPlayerCoordinates().posY - 2, par3EntityPlayer.getPlayerCoordinates().posZ, true);
                par2World.destroyBlock(par3EntityPlayer.getPlayerCoordinates().posX, par3EntityPlayer.getPlayerCoordinates().posY - 3, par3EntityPlayer.getPlayerCoordinates().posZ, true);
                par2World.destroyBlock(par3EntityPlayer.getPlayerCoordinates().posX, par3EntityPlayer.getPlayerCoordinates().posY - 4, par3EntityPlayer.getPlayerCoordinates().posZ, true);
                par2World.destroyBlock(par3EntityPlayer.getPlayerCoordinates().posX, par3EntityPlayer.getPlayerCoordinates().posY - 5, par3EntityPlayer.getPlayerCoordinates().posZ, true);
            }
        }
        else if (par3EntityPlayer.capabilities.isFlying)
        {
            par2World.destroyBlock(par3EntityPlayer.getPlayerCoordinates().posX, par3EntityPlayer.getPlayerCoordinates().posY + 1, par3EntityPlayer.getPlayerCoordinates().posZ, true);
            par2World.destroyBlock(par3EntityPlayer.getPlayerCoordinates().posX, par3EntityPlayer.getPlayerCoordinates().posY + 2, par3EntityPlayer.getPlayerCoordinates().posZ, true);
            par2World.destroyBlock(par3EntityPlayer.getPlayerCoordinates().posX, par3EntityPlayer.getPlayerCoordinates().posY + 3, par3EntityPlayer.getPlayerCoordinates().posZ, true);
            par2World.destroyBlock(par3EntityPlayer.getPlayerCoordinates().posX, par3EntityPlayer.getPlayerCoordinates().posY + 4, par3EntityPlayer.getPlayerCoordinates().posZ, true);
            par2World.destroyBlock(par3EntityPlayer.getPlayerCoordinates().posX, par3EntityPlayer.getPlayerCoordinates().posY + 5, par3EntityPlayer.getPlayerCoordinates().posZ, true);
        }
        else if (!par3EntityPlayer.worldObj.isRemote)
        {
            par2World.destroyBlock(par3EntityPlayer.getPlayerCoordinates().posX, par3EntityPlayer.getPlayerCoordinates().posY - 1, par3EntityPlayer.getPlayerCoordinates().posZ, true);
            par2World.destroyBlock(par3EntityPlayer.getPlayerCoordinates().posX, par3EntityPlayer.getPlayerCoordinates().posY - 2, par3EntityPlayer.getPlayerCoordinates().posZ, true);
            par2World.destroyBlock(par3EntityPlayer.getPlayerCoordinates().posX, par3EntityPlayer.getPlayerCoordinates().posY - 3, par3EntityPlayer.getPlayerCoordinates().posZ, true);
        }

        return par1ItemStack;
    }
}

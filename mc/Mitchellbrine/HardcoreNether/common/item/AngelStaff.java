package mc.Mitchellbrine.HardcoreNether.common.item;

import java.util.List;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class AngelStaff extends Item
{
    public AngelStaff(int par1)
    {
        super(par1);
    }

    /**
     * allows items to add custom lines of information to the mouseover description
     */
    public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
    {
        par3List.add("Stamina Power: Speed");
    }

    /**
     * Called whenever this item is equipped and the right mouse button is pressed. Args: itemStack, world, entityPlayer
     */
    public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
        par3EntityPlayer.setItemInUse(par1ItemStack, this.getMaxItemUseDuration(par1ItemStack));

        if (MainMod.Special >= 20)
        {
            par3EntityPlayer.capabilities.setFlySpeed(1.0F);
            par3EntityPlayer.capabilities.setPlayerWalkSpeed(1.0F);
            par3EntityPlayer.setCurrentItemOrArmor(0, new ItemStack(MainMod.angelStaffUse, 1));
            MainMod.Special = 0;
        }

        if (MainMod.Special < 20 && !par3EntityPlayer.worldObj.isRemote)
        {
            par3EntityPlayer.addChatMessage("You need more stamina to use this item\'s special!");
        }

        return par1ItemStack;
    }

    /**
     * Returns True is the item is renderer in full 3D when hold.
     */
    public boolean isFull3D()
    {
        return true;
    }
}

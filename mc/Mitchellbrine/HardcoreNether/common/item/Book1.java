package mc.Mitchellbrine.HardcoreNether.common.item;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import java.util.List;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

public class Book1 extends Item
{
    public int page = 0;

    public Book1(int par1)
    {
        super(par1);
    }

    /**
     * Called whenever this item is equipped and the right mouse button is pressed. Args: itemStack, world, entityPlayer
     */
    public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
        if (!par2World.isRemote)
        {
            if (this.page == 0)
            {
                par3EntityPlayer.addChatMessage("JOURNAL ENTRY: 1");
                par3EntityPlayer.addChatMessage("Dear Journal,");
                par3EntityPlayer.addChatMessage("My name is John Bores. I have been roaming these biomes for years and I\'ve never seen anything like it. Almost Hell on Earth. Like a chunk of Hell rose to the surface. It is most peculiar. It seems to unleash hellfire upon someone walking on it.");
                par3EntityPlayer.addChatMessage("I\'ve spent my time exploring the depths of Hell, and this looks like the netherrack I\'ve seen there. It must be placed by the devil to murder us and steal our souls. " + EnumChatFormatting.DARK_RED + "DO NOT WALK ON IT NO MATTER WHAT!");
                ++this.page;
                par2World.playSoundAtEntity(par3EntityPlayer, "hardcoreNether:ost.demons", 10.0F, 1.0F);
            }
            else if (this.page == 1)
            {
                par3EntityPlayer.addChatMessage("JOURNAL ENTRY: 2");
                par3EntityPlayer.addChatMessage("Dear Journal,");
                par3EntityPlayer.addChatMessage("I have taken the time to mine the block with my diamond pickaxe. It is INCREDIBLY strong! It takes a while to break, but it gives off a black ore. My tooltip tells me it is called \'Hardcore Ore.\' This has demonic written all over it!");
                par3EntityPlayer.addChatMessage("Strange... I feel as if I am more powerful after mining it. I feel stronger. I feel like I could " + EnumChatFormatting.DARK_RED + "DESTROY ALL LIFE ON EARTH!");
                par3EntityPlayer.addChatMessage("Whoa! What was that? I just had a weird sensation come over me... This can\'t be good...");
                ++this.page;
            }
            else if (this.page == 2)
            {
                par3EntityPlayer.addChatMessage("JOURNAL ENTRY: 3");
                par3EntityPlayer.addChatMessage("This ore seems to be seeping through my inventory... I need to hide this book before it seeps through the pages and they bec" + EnumChatFormatting.OBFUSCATED + "ome obfusicated!");
                par3EntityPlayer.addChatMessage("No! It has already begun! I need to find the nearest dungeon and hide this book! If you are reading this, your world is  d" + EnumChatFormatting.OBFUSCATED + "oomed!");
                this.page = 0;
                par3EntityPlayer.addStat(MainMod.who, 1);
            }
        }

        return par1ItemStack;
    }

    /**
     * allows items to add custom lines of information to the mouseover description
     */
    public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
    {
        par3List.add("by John Bores");
    }

    @SideOnly(Side.CLIENT)
    public boolean hasEffect(ItemStack par1ItemStack, int pass)
    {
        return true;
    }
}

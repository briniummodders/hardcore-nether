package mc.Mitchellbrine.HardcoreNether.common.item;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.util.List;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;

public class ItemEncryption1 extends Item
{
    public ItemEncryption1(int par1)
    {
        super(par1);
        this.setHasSubtypes(true);
    }

    public String getItemNameIS(ItemStack is)
    {
        switch (is.getItemDamage())
        {
            case 0:
                return EnumChatFormatting.OBFUSCATED + "Fire Aspect";

            case 1:
                return EnumChatFormatting.OBFUSCATED + "Knockback";

            case 2:
                return EnumChatFormatting.OBFUSCATED + "Looting";

            default:
                return "itemUnknown";
        }
    }

    @SideOnly(Side.CLIENT)

    /**
     * returns a list of items with the same ID, but different meta (eg: dye returns 16 items)
     */
    public void getSubItems(int itemID, CreativeTabs tab, List itemList)
    {
        for (int i = 0; i < 3; ++i)
        {
            itemList.add(new ItemStack(itemID, 1, i));
        }
    }

    @SideOnly(Side.CLIENT)
    public boolean hasEffect(ItemStack par1ItemStack)
    {
        return true;
    }
}

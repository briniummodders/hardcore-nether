package mc.Mitchellbrine.HardcoreNether.common.item;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import mc.Mitchellbrine.HardcoreNether.common.world.dimensional.TeleporterHardcore;
import mc.Mitchellbrine.HardcoreNether.common.world.dimensional.TeleporterValiant;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.Teleporter;
import net.minecraft.world.World;

public class PortalPlacer extends Item
{
    public PortalPlacer(int id)
    {
        super(id);
        this.setCreativeTab(MainMod.HardcoreTab);
    }

    /**
     * Callback for item usage. If the item does something special on right clicking, he will have one of those. Return
     * True if something happen and false if it don't. This is for ITEMS, not BLOCKS
     */
    public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int par4, int par5, int par6, int par7, float par8, float par9, float par10)
    {
        if (!par3World.isRemote)
        {
            EntityPlayerMP thePlayer = (EntityPlayerMP)par2EntityPlayer;

            if (thePlayer.dimension == MainMod.dimension)
            {
                thePlayer.timeUntilPortal = 10;
                thePlayer.mcServer.getConfigurationManager().transferPlayerToDimension(thePlayer, 0, new TeleporterHardcore(thePlayer.mcServer.worldServerForDimension(0)));
            }
            else if (thePlayer.dimension == -1)
            {
                thePlayer.timeUntilPortal = 10;
                thePlayer.mcServer.getConfigurationManager().transferPlayerToDimension(thePlayer, 0, new Teleporter(thePlayer.mcServer.worldServerForDimension(-1)));
            }
            else if (thePlayer.dimension == MainMod.dimensionValiant)
            {
                thePlayer.timeUntilPortal = 10;
                thePlayer.mcServer.getConfigurationManager().transferPlayerToDimension(thePlayer, 0, new TeleporterValiant(thePlayer.mcServer.worldServerForDimension(0)));
            }

            return true;
        }
        else
        {
            return false;
        }
    }
}

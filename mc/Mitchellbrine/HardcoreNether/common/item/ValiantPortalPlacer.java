package mc.Mitchellbrine.HardcoreNether.common.item;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class ValiantPortalPlacer extends Item
{
    private String texture;

    public ValiantPortalPlacer(int id, String texture)
    {
        super(id);
        this.texture = texture;
        this.setCreativeTab(MainMod.HardcoreTab);
    }

    /**
     * Callback for item usage. If the item does something special on right clicking, he will have one of those. Return
     * True if something happen and false if it don't. This is for ITEMS, not BLOCKS
     */
    public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int par4, int par5, int par6, int par7, float par8, float par9, float par10)
    {
        if (par3World.isRemote)
        {
            return false;
        }
        else
        {
            int direction = MathHelper.floor_double((double)(par2EntityPlayer.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;
            int y;
            int x;

            if (direction != 1 && direction != 3)
            {
                for (y = 1; y < 5; ++y)
                {
                    for (x = -1; x < 2; ++x)
                    {
                        if (par3World.getBlockId(par4 + x, par5 + y, par6) != 0)
                        {
                            par2EntityPlayer.addChatMessage("No room for a portal.");
                            return false;
                        }
                    }
                }

                par3World.setBlock(par4, par5 + 1, par6, MainMod.valiantBlock.blockID);
                par3World.setBlock(par4, par5 + 1, par6 + 1, MainMod.valiantBlock.blockID);
                par3World.setBlock(par4, par5 + 1, par6 + 2, MainMod.valiantBlock.blockID);
                par3World.setBlock(par4, par5 + 1, par6 - 1, MainMod.valiantBlock.blockID);
                par3World.setBlock(par4, par5 + 2, par6 - 1, MainMod.valiantBlock.blockID);
                par3World.setBlock(par4, par5 + 3, par6 - 1, MainMod.valiantBlock.blockID);
                par3World.setBlock(par4, par5 + 4, par6 - 1, MainMod.valiantBlock.blockID);
                par3World.setBlock(par4, par5 + 5, par6 - 1, MainMod.valiantBlock.blockID);
                par3World.setBlock(par4, par5 + 2, par6 + 2, MainMod.valiantBlock.blockID);
                par3World.setBlock(par4, par5 + 3, par6 + 2, MainMod.valiantBlock.blockID);
                par3World.setBlock(par4, par5 + 4, par6 + 2, MainMod.valiantBlock.blockID);
                par3World.setBlock(par4, par5 + 5, par6 + 2, MainMod.valiantBlock.blockID);
                par3World.setBlock(par4, par5 + 5, par6, MainMod.valiantBlock.blockID);
                par3World.setBlock(par4, par5 + 5, par6 + 1, MainMod.valiantBlock.blockID);
                par3World.setBlock(par4, par5 + 2, par6, MainMod.IceFire.blockID);
            }
            else
            {
                for (y = 1; y < 5; ++y)
                {
                    for (x = -1; x < 2; ++x)
                    {
                        if (par3World.getBlockId(par4, par5 + y, par6 + x) != 0)
                        {
                            par2EntityPlayer.addChatMessage("No room for a portal.");
                            return false;
                        }
                    }
                }

                par3World.setBlock(par4, par5 + 1, par6, MainMod.valiantBlock.blockID);
                par3World.setBlock(par4, par5 + 1, par6 + 1, MainMod.valiantBlock.blockID);
                par3World.setBlock(par4, par5 + 1, par6 + 2, MainMod.valiantBlock.blockID);
                par3World.setBlock(par4, par5 + 1, par6 - 1, MainMod.valiantBlock.blockID);
                par3World.setBlock(par4, par5 + 2, par6 - 1, MainMod.valiantBlock.blockID);
                par3World.setBlock(par4, par5 + 3, par6 - 1, MainMod.valiantBlock.blockID);
                par3World.setBlock(par4, par5 + 4, par6 - 1, MainMod.valiantBlock.blockID);
                par3World.setBlock(par4, par5 + 5, par6 - 1, MainMod.valiantBlock.blockID);
                par3World.setBlock(par4, par5 + 2, par6 + 2, MainMod.valiantBlock.blockID);
                par3World.setBlock(par4, par5 + 3, par6 + 2, MainMod.valiantBlock.blockID);
                par3World.setBlock(par4, par5 + 4, par6 + 2, MainMod.valiantBlock.blockID);
                par3World.setBlock(par4, par5 + 5, par6 + 2, MainMod.valiantBlock.blockID);
                par3World.setBlock(par4, par5 + 5, par6, MainMod.valiantBlock.blockID);
                par3World.setBlock(par4, par5 + 5, par6 + 1, MainMod.valiantBlock.blockID);
                par3World.setBlock(par4, par5 + 2, par6, MainMod.IceFire.blockID);
            }

            return true;
        }
    }
}

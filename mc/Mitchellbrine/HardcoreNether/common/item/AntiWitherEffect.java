package mc.Mitchellbrine.HardcoreNether.common.item;

import java.util.Random;

import mc.Mitchellbrine.HardcoreNether.common.block.BlockHardcoreSapling;
import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import mc.Mitchellbrine.HardcoreNether.common.entity.EntityHost;
import mc.Mitchellbrine.HardcoreNether.common.entity.EntityHostStage2;
import mc.Mitchellbrine.HardcoreNether.common.entity.EntityHostStage3;
import net.minecraft.entity.monster.EntitySkeleton;
import net.minecraft.potion.Potion;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.player.BonemealEvent;

public class AntiWitherEffect
{
    @ForgeSubscribe
    public void onEntityUpdate(LivingUpdateEvent event)
    {
        EntityHostStage2 hostStage2 = new EntityHostStage2(event.entityLiving.worldObj);
        EntityHostStage3 hostStage3 = new EntityHostStage3(event.entityLiving.worldObj);

        if (event.entityLiving.isPotionActive(MainMod.antiWitherPotion))
        {
            if (event.entityLiving.isPotionActive(Potion.wither) && event.entityLiving.worldObj.rand.nextInt(20) == 0)
            {
                event.entityLiving.attackEntityFrom(DamageSource.wither, -10.0F);
            }

            if (event.entityLiving.getActivePotionEffect(MainMod.antiWitherPotion).getDuration() == 0)
            {
                event.entityLiving.removePotionEffect(MainMod.antiWitherPotion.id);
                return;
            }
        }

        if (event.entityLiving instanceof EntityHost && event.entityLiving.getHealth() <= 5000.0F)
        {
            hostStage2.setLocationAndAngles(event.entityLiving.posX, event.entityLiving.posY, event.entityLiving.posZ, 0.0F, 0.0F);
            event.entityLiving.worldObj.spawnEntityInWorld(hostStage2);
            event.entityLiving.setDead();
        }

        if (event.entityLiving instanceof EntityHostStage2 && event.entityLiving.getHealth() <= 2500.0F)
        {
            hostStage3.setLocationAndAngles(event.entityLiving.posX, event.entityLiving.posY, event.entityLiving.posZ, 0.0F, 0.0F);
            event.entityLiving.worldObj.spawnEntityInWorld(hostStage3);
            event.entityLiving.setDead();
        }
    }

    @ForgeSubscribe
    public void deathEvent(LivingDropsEvent event)
    {
        if (event.entityLiving instanceof EntitySkeleton)
        {
            Random rand = new Random();

            if (((EntitySkeleton)event.entityLiving).getSkeletonType() == 1 && rand.nextInt(100) >= 90)
            {
                event.entityLiving.dropItem(MainMod.valiantBone.itemID, 1);
            }
        }
    }

    @ForgeSubscribe
    public void bonemeal(BonemealEvent event)
    {
        if (event.ID == MainMod.hardcoreSapling.blockID && !event.world.isRemote)
        {
            ((BlockHardcoreSapling)MainMod.hardcoreSapling).growTree(event.world, event.X, event.Y, event.Z, event.world.rand);
        }
    }
}

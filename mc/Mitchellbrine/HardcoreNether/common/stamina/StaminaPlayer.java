package mc.Mitchellbrine.HardcoreNether.common.stamina;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraftforge.common.IExtendedEntityProperties;

public class StaminaPlayer implements IExtendedEntityProperties
{
    public static final String EXT_PROP_NAME = "StaminaPlayerBase";
    private final EntityPlayer entity;
    private int Stamina;

    public StaminaPlayer(EntityPlayer entity)
    {
        this.entity = entity;
    }

    public static final void register(EntityPlayer entity)
    {
        entity.registerExtendedProperties("StaminaPlayerBase", new StaminaPlayer(entity));
    }

    public static final StaminaPlayer get(EntityLivingBase entity)
    {
        return (StaminaPlayer)entity.getExtendedProperties("StaminaPlayerBase");
    }

    public void saveNBTData(NBTTagCompound compound)
    {
        compound.setInteger("Stamina", this.Stamina);
    }

    public void loadNBTData(NBTTagCompound compound)
    {
        this.Stamina = compound.getInteger("Stamina");
        System.out.println("[LIVING BASE] Stamina from NBT: " + this.Stamina);
    }

    public void init(Entity entity, World world)
    {
        System.out.println("[LIVING BASE] Stamina: " + this.Stamina);
    }
}

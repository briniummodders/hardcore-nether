package mc.Mitchellbrine.HardcoreNether.common.stamina;

public interface IStamina
{
    int neededStamina();

    int usedStamina();
}

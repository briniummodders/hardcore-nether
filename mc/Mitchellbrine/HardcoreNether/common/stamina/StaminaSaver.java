package mc.Mitchellbrine.HardcoreNether.common.stamina;

import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.event.FMLServerStoppingEvent;

import java.io.DataInputStream;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.event.ForgeSubscribe;

public class StaminaSaver
{
    NBTTagCompound compound;
    DataInputStream stream = new DataInputStream(this.getClass().getResourceAsStream("/assets/hardcoreNether/saving/stamina.dat"));

    @ForgeSubscribe
    public void saveYourStamina(FMLServerStartingEvent event)
    {
        try
        {
            this.compound = CompressedStreamTools.read(this.stream);
        }
        catch (Exception var3)
        {
            var3.getCause();
        }

        if (this.compound != null)
        {
            this.compound.setInteger("stamina", MainMod.Special);
        }
    }

    @ForgeSubscribe
    public void readYourStamina(FMLServerStoppingEvent event) {}
}

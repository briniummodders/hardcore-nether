package mc.Mitchellbrine.HardcoreNether.common.stamina;

import mc.Mitchellbrine.HardcoreNether.common.entity.EntityPlayerStamina;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.EntityEvent.EntityConstructing;

public class EventHandler_Stamina
{
    public Minecraft mc;

    @ForgeSubscribe
    public void onEntityConstructing(EntityConstructing event)
    {
        if (event.entity instanceof EntityPlayer && event.entity.getExtendedProperties("EntityPlayerStaminaBalances") == null)
        {
            event.entity.registerExtendedProperties("EntityPlayerStaminaBalances", new EntityPlayerStamina((EntityPlayer)event.entity));
        }
    }
}

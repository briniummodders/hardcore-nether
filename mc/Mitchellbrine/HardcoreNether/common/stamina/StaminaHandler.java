package mc.Mitchellbrine.HardcoreNether.common.stamina;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraft.world.WorldSavedData;
import net.minecraft.world.storage.MapStorage;

public class StaminaHandler extends WorldSavedData
{
    private static final String key = "stamina";

    public StaminaHandler()
    {
        super("stamina");
    }

    public StaminaHandler(String identifier)
    {
        super(identifier);
    }

    /**
     * reads in data from the NBTTagCompound into this MapDataBase
     */
    public void readFromNBT(NBTTagCompound nbt)
    {
        MainMod.Special = nbt.getInteger("stamina");
    }

    /**
     * write data to NBTTagCompound from this MapDataBase, similar to Entities and TileEntities
     */
    public void writeToNBT(NBTTagCompound nbt)
    {
        nbt.setInteger("stamina", MainMod.Special);
    }

    public static StaminaHandler get(World world)
    {
        MapStorage storage = world.perWorldStorage;
        StaminaHandler result = (StaminaHandler)storage.loadData(StaminaHandler.class, "stamina");

        if (result == null)
        {
            result = new StaminaHandler();
            storage.setData("stamina", result);
            System.out.print("Successfully Saved Stamina for" + world.getWorldInfo().getWorldName());
        }

        return result;
    }
}

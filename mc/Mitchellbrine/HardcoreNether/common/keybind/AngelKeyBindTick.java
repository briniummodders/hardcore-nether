package mc.Mitchellbrine.HardcoreNether.common.keybind;

import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.TickType;

import java.util.EnumSet;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.entity.player.EntityPlayer;

public class AngelKeyBindTick implements ITickHandler
{
    private final EnumSet<TickType> ticksToGet;
    private static boolean hoverMode = false;

    public AngelKeyBindTick(EnumSet<TickType> ticksToGet)
    {
        this.ticksToGet = ticksToGet;
    }

    public void tickStart(EnumSet<TickType> type, Object ... tickData)
    {
        playerTick((EntityPlayer)tickData[0]);
    }

    public void tickEnd(EnumSet<TickType> type, Object ... tickData) {}

    public EnumSet<TickType> ticks()
    {
        return this.ticksToGet;
    }

    public String getLabel()
    {
        return "AngelPlayerTick";
    }

    public static void playerTick(EntityPlayer player)
    {
        if (player.getCurrentItemOrArmor(3) != null && player.getCurrentItemOrArmor(3).itemID == MainMod.angelWingsArmor.itemID)
        {
            if (AngelKeyBind.keyPressed && player.onGround)
            {
                player.capabilities.allowFlying = true;
                player.motionY = 0.15D;
                player.playSound("mob.enderdragon.wings", 4.0F, 1.0F);
            }
            else if (AngelKeyBind.keyPressed && !player.onGround)
            {
                player.capabilities.allowFlying = true;
                player.motionY = 0.4D;
                player.playSound("hardcoreNether:machine.wings", 4.0F, 1.0F);
            }
            else if (AngelKeyBind2.keyPressed && !player.onGround)
            {
                player.capabilities.allowFlying = true;
                player.motionY = 0.08D;

                if (MainMod.ValiantActivate == 1)
                {
                    player.capabilities.setFlySpeed(player.capabilities.getWalkSpeed() * 80.0F);
                }

                player.playSound("hardcoreNether:machine.wings", 4.0F, 1.0F);
            }

            if (!AngelKeyBind.keyPressed && !AngelKeyBind2.keyPressed && !player.capabilities.isCreativeMode)
            {
                player.capabilities.allowFlying = false;
            }
        }
    }
}

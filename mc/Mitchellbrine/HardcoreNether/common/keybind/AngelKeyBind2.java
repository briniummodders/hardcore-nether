package mc.Mitchellbrine.HardcoreNether.common.keybind;

import cpw.mods.fml.client.registry.KeyBindingRegistry.KeyHandler;
import cpw.mods.fml.common.TickType;
import java.util.EnumSet;
import net.minecraft.client.settings.KeyBinding;

public class AngelKeyBind2 extends KeyHandler
{
    private EnumSet tickTypes;
    public static boolean keyPressed = false;

    public AngelKeyBind2(KeyBinding[] keyBindings, boolean[] repeatings)
    {
        super(keyBindings, repeatings);
        this.tickTypes = EnumSet.of(TickType.CLIENT);
    }

    public String getLabel()
    {
        return "Angelic Flying";
    }

    public void keyDown(EnumSet<TickType> types, KeyBinding kb, boolean tickEnd, boolean isRepeat)
    {
        keyPressed = true;
    }

    public void keyUp(EnumSet<TickType> types, KeyBinding kb, boolean tickEnd)
    {
        keyPressed = false;
    }

    public EnumSet<TickType> ticks()
    {
        return this.tickTypes;
    }
}

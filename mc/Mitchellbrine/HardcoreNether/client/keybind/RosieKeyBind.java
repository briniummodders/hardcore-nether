package mc.Mitchellbrine.HardcoreNether.client.keybind;

import cpw.mods.fml.client.registry.KeyBindingRegistry.KeyHandler;
import cpw.mods.fml.common.TickType;

import java.util.EnumSet;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.client.settings.KeyBinding;

public class RosieKeyBind extends KeyHandler
{
    private EnumSet tickTypes;

    public RosieKeyBind(KeyBinding[] keyBindings, boolean[] repeatings)
    {
        super(keyBindings, repeatings);
        this.tickTypes = EnumSet.of(TickType.CLIENT);
    }

    public String getLabel()
    {
        return "RosieKey";
    }

    public void keyDown(EnumSet<TickType> types, KeyBinding kb, boolean tickEnd, boolean isRepeat)
    {
        MainMod.rosieTextures = true;
    }

    public void keyUp(EnumSet<TickType> types, KeyBinding kb, boolean tickEnd) {}

    public EnumSet<TickType> ticks()
    {
        return this.tickTypes;
    }
}

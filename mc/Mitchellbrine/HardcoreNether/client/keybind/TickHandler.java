package mc.Mitchellbrine.HardcoreNether.client.keybind;

import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.TickType;

import java.util.EnumSet;

import mc.Mitchellbrine.HardcoreNether.common.stamina.StaminaHandler;
import net.minecraft.world.World;

public class TickHandler implements ITickHandler
{
    public static StaminaHandler worldData;

    public void tickStart(EnumSet<TickType> type, Object ... tickData)
    {
        if (type.equals(EnumSet.of(TickType.WORLDLOAD)))
        {
            World world = null;

            for (int i = 0; i < tickData.length; ++i)
            {
                if (tickData[0] instanceof World)
                {
                    world = (World)tickData[0];
                }
            }

            if (world == null)
            {
                return;
            }

            worldData = StaminaHandler.get(world);
            worldData.markDirty();
        }
    }

    public void tickEnd(EnumSet<TickType> type, Object ... tickData) {}

    public EnumSet<TickType> ticks()
    {
        return EnumSet.of(TickType.WORLDLOAD);
    }

    public String getLabel()
    {
        return "Stamina_Ticker";
    }
}

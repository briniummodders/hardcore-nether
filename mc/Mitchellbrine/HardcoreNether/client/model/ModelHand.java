package mc.Mitchellbrine.HardcoreNether.client.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelHand extends ModelBase
{
    ModelRenderer Arm;
    ModelRenderer Hand;
    ModelRenderer Shape2;
    ModelRenderer Shape1;
    ModelRenderer Shape3;
    ModelRenderer Shape4;

    public ModelHand()
    {
        this.textureWidth = 64;
        this.textureHeight = 32;
        this.Arm = new ModelRenderer(this, 0, 0);
        this.Arm.addBox(0.0F, 0.0F, 0.0F, 11, 4, 12);
        this.Arm.setRotationPoint(-6.0F, 0.0F, -3.0F);
        this.Arm.setTextureSize(64, 32);
        this.Arm.mirror = true;
        this.setRotation(this.Arm, 0.0F, 0.0F, 0.0F);
        this.Hand = new ModelRenderer(this, 0, 16);
        this.Hand.addBox(0.0F, 0.0F, 0.0F, 15, 12, 3);
        this.Hand.setRotationPoint(-8.0F, -3.0F, -6.0F);
        this.Hand.setTextureSize(64, 32);
        this.Hand.mirror = true;
        this.setRotation(this.Hand, 0.0F, 0.0F, 0.0F);
        this.Shape2 = new ModelRenderer(this, 46, 0);
        this.Shape2.addBox(0.0F, 0.0F, 0.0F, 3, 2, 6);
        this.Shape2.setRotationPoint(-7.0F, -2.0F, -12.0F);
        this.Shape2.setTextureSize(64, 32);
        this.Shape2.mirror = true;
        this.setRotation(this.Shape2, 0.0F, 0.0F, 0.0F);
        this.Shape1 = new ModelRenderer(this, 46, 8);
        this.Shape1.addBox(0.0F, 0.0F, 0.0F, 3, 2, 6);
        this.Shape1.setRotationPoint(-2.0F, -2.0F, -12.0F);
        this.Shape1.setTextureSize(64, 32);
        this.Shape1.mirror = true;
        this.setRotation(this.Shape1, 0.0F, 0.0F, 0.0F);
        this.Shape3 = new ModelRenderer(this, 36, 16);
        this.Shape3.addBox(0.0F, 0.0F, 0.0F, 3, 2, 6);
        this.Shape3.setRotationPoint(3.0F, -2.0F, -12.0F);
        this.Shape3.setTextureSize(64, 32);
        this.Shape3.mirror = true;
        this.setRotation(this.Shape3, 0.0F, 0.0F, 0.0F);
        this.Shape4 = new ModelRenderer(this, 36, 24);
        this.Shape4.addBox(0.0F, 0.0F, 0.0F, 3, 2, 6);
        this.Shape4.setRotationPoint(-2.0F, 6.0F, -12.0F);
        this.Shape4.setTextureSize(64, 32);
        this.Shape4.mirror = true;
        this.setRotation(this.Shape4, 0.0F, 0.0F, 0.0F);
    }

    /**
     * Sets the models various rotation angles then renders the model.
     */
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
    {
        super.render(entity, f, f1, f2, f3, f4, f5);
        this.setRotationAngles(f, f1, f2, f3, f4, f5);
        this.Arm.render(f5);
        this.Hand.render(f5);
        this.Shape2.render(f5);
        this.Shape1.render(f5);
        this.Shape3.render(f5);
        this.Shape4.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z)
    {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5) {}
}

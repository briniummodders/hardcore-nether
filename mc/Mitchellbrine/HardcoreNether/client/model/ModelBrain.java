package mc.Mitchellbrine.HardcoreNether.client.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelBrain extends ModelBase
{
    ModelRenderer Head;

    public ModelBrain()
    {
        this.textureWidth = 128;
        this.textureHeight = 256;
        this.Head = new ModelRenderer(this, 0, 0);
        this.Head.addBox(0.0F, 0.0F, 0.0F, 32, 32, 32);
        this.Head.setRotationPoint(-15.0F, -16.0F, -15.0F);
        this.Head.setTextureSize(128, 256);
        this.Head.mirror = true;
        this.setRotation(this.Head, 0.0F, 0.0F, 0.0F);
    }

    /**
     * Sets the models various rotation angles then renders the model.
     */
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
    {
        super.render(entity, f, f1, f2, f3, f4, f5);
        this.setRotationAngles(f, f1, f2, f3, f4, f5);
        this.Head.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z)
    {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5) {}
}

package mc.Mitchellbrine.HardcoreNether.client.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelScroll extends ModelBase
{
    ModelRenderer base;
    ModelRenderer edgeFront;
    ModelRenderer Shape1;

    public ModelScroll()
    {
        this.textureWidth = 64;
        this.textureHeight = 32;
        this.base = new ModelRenderer(this, 0, 0);
        this.base.addBox(0.0F, 0.0F, 0.0F, 12, 1, 14);
        this.base.setRotationPoint(-6.0F, 23.0F, -7.0F);
        this.base.setTextureSize(64, 32);
        this.base.mirror = true;
        this.setRotation(this.base, 0.0F, 0.0F, 0.0F);
        this.edgeFront = new ModelRenderer(this, 0, 15);
        this.edgeFront.addBox(0.0F, 0.0F, 0.0F, 12, 1, 1);
        this.edgeFront.setRotationPoint(-6.0F, 22.0F, -8.0F);
        this.edgeFront.setTextureSize(64, 32);
        this.edgeFront.mirror = true;
        this.setRotation(this.edgeFront, 0.0F, 0.0F, 0.0F);
        this.Shape1 = new ModelRenderer(this, 0, 18);
        this.Shape1.addBox(0.0F, 0.0F, 0.0F, 12, 1, 1);
        this.Shape1.setRotationPoint(-6.0F, 22.0F, 7.0F);
        this.Shape1.setTextureSize(64, 32);
        this.Shape1.mirror = true;
        this.setRotation(this.Shape1, 0.0F, 0.0F, 0.0F);
    }

    /**
     * Sets the models various rotation angles then renders the model.
     */
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
    {
        super.render(entity, f, f1, f2, f3, f4, f5);
        this.setRotationAngles(f, f1, f2, f3, f4, f5);
        this.base.render(f5);
        this.edgeFront.render(f5);
        this.Shape1.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z)
    {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5) {}
}

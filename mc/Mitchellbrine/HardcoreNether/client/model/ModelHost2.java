package mc.Mitchellbrine.HardcoreNether.client.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelHost2 extends ModelBase
{
    ModelRenderer Head;
    ModelRenderer Body;
    ModelRenderer leftarm;
    ModelRenderer Shape1;

    public ModelHost2()
    {
        this.textureWidth = 256;
        this.textureHeight = 512;
        this.Head = new ModelRenderer(this, 0, 0);
        this.Head.addBox(0.0F, 0.0F, 0.0F, 32, 32, 32);
        this.Head.setRotationPoint(-16.0F, -32.0F, -16.0F);
        this.Head.setTextureSize(256, 512);
        this.Head.mirror = true;
        this.setRotation(this.Head, 0.0F, 0.0F, 0.0F);
        this.Body = new ModelRenderer(this, 0, 64);
        this.Body.addBox(0.0F, 0.0F, 0.0F, 16, 20, 8);
        this.Body.setRotationPoint(-8.0F, 0.0F, -3.0F);
        this.Body.setTextureSize(256, 512);
        this.Body.mirror = true;
        this.setRotation(this.Body, 0.0F, 0.0F, 0.0F);
        this.leftarm = new ModelRenderer(this, 0, 92);
        this.leftarm.addBox(0.0F, 0.0F, 0.0F, 8, 5, 16);
        this.leftarm.setRotationPoint(8.0F, 1.0F, -11.0F);
        this.leftarm.setTextureSize(256, 512);
        this.leftarm.mirror = true;
        this.setRotation(this.leftarm, 0.0F, 0.0F, 0.0F);
        this.Shape1 = new ModelRenderer(this, 0, 114);
        this.Shape1.addBox(0.0F, 0.0F, 0.0F, 8, 5, 16);
        this.Shape1.setRotationPoint(-16.0F, 1.0F, -11.0F);
        this.Shape1.setTextureSize(256, 512);
        this.Shape1.mirror = true;
        this.setRotation(this.Shape1, 0.0F, 0.0F, 0.0F);
    }

    /**
     * Sets the models various rotation angles then renders the model.
     */
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
    {
        super.render(entity, f, f1, f2, f3, f4, f5);
        this.setRotationAngles(f, f1, f2, f3, f4, f5);
        this.Head.render(f5);
        this.Body.render(f5);
        this.leftarm.render(f5);
        this.Shape1.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z)
    {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5) {}
}

package mc.Mitchellbrine.HardcoreNether.client.render;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import mc.Mitchellbrine.HardcoreNether.common.entity.EntityDemonicWolf;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.passive.EntitySheep;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

@SideOnly(Side.CLIENT)
public class RenderDemonicWolf extends RenderLiving
{
    private static final ResourceLocation wolfTextures = new ResourceLocation("hardcoreNether:textures/entity/demonWolf.png");
    private static final ResourceLocation tamedWolfTextures = new ResourceLocation("hardcoreNether:textures/entity/demonWolf_tame.png");
    private static final ResourceLocation anrgyWolfTextures = new ResourceLocation("hardcoreNether:textures/entity/demonWolf_angry.png");
    private static final ResourceLocation wolfCollarTextures = new ResourceLocation("hardcoreNether:textures/entity/wolf_collar.png");
    private static final ResourceLocation rosieTextures = new ResourceLocation("hardcoreNether:textures/render/Rosie.png");
    private static final ResourceLocation tamedRosieTextures = new ResourceLocation("hardcoreNether:textures/render/Rosie_tame.png");
    private static final ResourceLocation angryRosieTextures = new ResourceLocation("hardcoreNether:textures/render/Rosie_angry.png");

    public RenderDemonicWolf(ModelBase par1ModelBase, ModelBase par2ModelBase, float par3)
    {
        super(par1ModelBase, par3);
        this.setRenderPassModel(par2ModelBase);
    }

    protected float getTailRotation(EntityDemonicWolf par1EntityWolf, float par2)
    {
        return par1EntityWolf.getTailRotation();
    }

    protected int func_82447_a(EntityDemonicWolf par1EntityWolf, int par2, float par3)
    {
        float f1;

        if (par2 == 0 && par1EntityWolf.getWolfShaking())
        {
            f1 = par1EntityWolf.getBrightness(par3) * par1EntityWolf.getShadingWhileShaking(par3);

            if (MainMod.rosieTextures)
            {
                this.bindTexture(rosieTextures);
            }
            else
            {
                this.bindTexture(wolfTextures);
            }

            GL11.glColor3f(f1, f1, f1);
            return 1;
        }
        else if (par2 == 1 && par1EntityWolf.isTamed())
        {
            this.bindTexture(wolfCollarTextures);
            f1 = 1.0F;
            int j = par1EntityWolf.getCollarColor();
            GL11.glColor3f(f1 * EntitySheep.fleeceColorTable[j][0], f1 * EntitySheep.fleeceColorTable[j][1], f1 * EntitySheep.fleeceColorTable[j][2]);
            return 1;
        }
        else
        {
            return -1;
        }
    }

    protected ResourceLocation func_110914_a(EntityDemonicWolf par1EntityWolf)
    {
        return MainMod.rosieTextures ? (par1EntityWolf.isTamed() ? tamedRosieTextures : (par1EntityWolf.isAngry() ? angryRosieTextures : rosieTextures)) : (par1EntityWolf.isTamed() ? tamedWolfTextures : (par1EntityWolf.isAngry() ? anrgyWolfTextures : wolfTextures));
    }

    /**
     * Queries whether should render the specified pass or not.
     */
    protected int shouldRenderPass(EntityLivingBase par1EntityLivingBase, int par2, float par3)
    {
        return this.func_82447_a((EntityDemonicWolf)par1EntityLivingBase, par2, par3);
    }

    /**
     * Defines what float the third param in setRotationAngles of ModelBase is
     */
    protected float handleRotationFloat(EntityLivingBase par1EntityLivingBase, float par2)
    {
        return this.getTailRotation((EntityDemonicWolf)par1EntityLivingBase, par2);
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    protected ResourceLocation getEntityTexture(Entity par1Entity)
    {
        return this.func_110914_a((EntityDemonicWolf)par1Entity);
    }
}

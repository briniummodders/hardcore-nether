package mc.Mitchellbrine.HardcoreNether.client.render.hud;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.TickType;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import java.util.EnumSet;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiIngame;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

@SideOnly(Side.CLIENT)
public class HudTickHandler implements ITickHandler
{
    public void tickStart(EnumSet<TickType> type, Object ... tickData) {}

    public void tickEnd(EnumSet<TickType> type, Object ... tickData)
    {
        Minecraft minecraft = FMLClientHandler.instance().getClient();
        EntityClientPlayerMP player = minecraft.thePlayer;
        Object currentItemStack = null;

        if (Minecraft.getMinecraft().currentScreen == null && Minecraft.getMinecraft().inGameHasFocus && !minecraft.gameSettings.hideGUI && !minecraft.gameSettings.debugCamEnable)
        {
            ScaledResolution scaledresolution = new ScaledResolution(Minecraft.getMinecraft().gameSettings, Minecraft.getMinecraft().displayWidth, Minecraft.getMinecraft().displayHeight);
            FontRenderer fontrenderer = Minecraft.getMinecraft().fontRenderer;
            int width = scaledresolution.getScaledWidth();
            int height = scaledresolution.getScaledHeight();
            Minecraft.getMinecraft().entityRenderer.setupOverlayRendering();
            GuiIngame var10000 = FMLClientHandler.instance().getClient().ingameGUI;
            GuiIngame.drawRect(0, 0, 128, 32, 256);
            FMLClientHandler.instance().getClient().fontRenderer.drawStringWithShadow("Stamina: " + MainMod.Special, 1, 1, 10489616);
            
            if (MainMod.ValiantActivate == 0)
            {
                FMLClientHandler.instance().getClient().fontRenderer.drawStringWithShadow("Valiantite Bonus: LOCKED", 1, 9, 15771042);
            }

            if (MainMod.ValiantActivate == 1)
            {
                FMLClientHandler.instance().getClient().fontRenderer.drawStringWithShadow("Valiantite Bonus: UNLOCKED", 1, 9, 15771042);
            }
        }
    }

    public EnumSet<TickType> ticks()
    {
        return EnumSet.of(TickType.CLIENT, TickType.RENDER);
    }

    public String getLabel()
    {
        return "StaminaHUD";
    }

    private static void renderHUD(Minecraft minecraft, EntityPlayer player, ItemStack itemstack, float particleticks)
    {
        GuiIngame var10000 = FMLClientHandler.instance().getClient().ingameGUI;
        GuiIngame.drawRect(0, 0, 128, 32, 256);
        FMLClientHandler.instance().getClient().fontRenderer.drawStringWithShadow("Stamina:" + MainMod.Special, 0, 0, 25500);
    }
}

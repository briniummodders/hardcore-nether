package mc.Mitchellbrine.HardcoreNether.client.render;

import mc.Mitchellbrine.HardcoreNether.client.model.ModelDemon;
import mc.Mitchellbrine.HardcoreNether.common.entity.EntityDemon;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.util.ResourceLocation;

public class RenderDemon extends RenderLiving
{
    protected ModelDemon model;
    private static final ResourceLocation MobTexture = new ResourceLocation("/textures/render/Demon.png");

    public RenderDemon(ModelDemon ModelDemon, float f)
    {
        super(ModelDemon, f);
        this.model = (ModelDemon)this.mainModel;
    }

    public void renderDemon(EntityDemon entity, double par2, double par4, double par6, float par8, float par9)
    {
        super.doRenderLiving(entity, par2, par4, par6, par8, par9);
    }

    public void doRenderLiving(EntityLiving par1EntityLiving, double par2, double par4, double par6, float par8, float par9)
    {
        this.renderDemon((EntityDemon)par1EntityLiving, par2, par4, par6, par8, par9);
    }

    /**
     * Actually renders the given argument. This is a synthetic bridge method, always casting down its argument and then
     * handing it off to a worker function which does the actual work. In all probabilty, the class Render is generic
     * (Render<T extends Entity) and this method has signature public void doRender(T entity, double d, double d1,
     * double d2, float f, float f1). But JAD is pre 1.5 so doesn't do that.
     */
    public void doRender(Entity par1Entity, double par2, double par4, double par6, float par8, float par9)
    {
        this.renderDemon((EntityDemon)par1Entity, par2, par4, par6, par8, par9);
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    protected ResourceLocation getEntityTexture(Entity entity)
    {
        return MobTexture;
    }
}

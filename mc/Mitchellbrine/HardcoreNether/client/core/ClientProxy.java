package mc.Mitchellbrine.HardcoreNether.client.core;

import java.util.EnumSet;

import mc.Mitchellbrine.HardcoreNether.client.handler.GuiHandler;
import mc.Mitchellbrine.HardcoreNether.client.keybind.RosieKeyBind;
import mc.Mitchellbrine.HardcoreNether.client.keybind.RosieKeyBind2;
import mc.Mitchellbrine.HardcoreNether.client.keybind.TickHandler;
import mc.Mitchellbrine.HardcoreNether.client.model.ModelAngelicPig;
import mc.Mitchellbrine.HardcoreNether.client.model.ModelBrain;
import mc.Mitchellbrine.HardcoreNether.client.model.ModelDemon;
import mc.Mitchellbrine.HardcoreNether.client.model.ModelDemonicWolf;
import mc.Mitchellbrine.HardcoreNether.client.model.ModelHand;
import mc.Mitchellbrine.HardcoreNether.client.model.ModelHost;
import mc.Mitchellbrine.HardcoreNether.client.model.ModelHost2;
import mc.Mitchellbrine.HardcoreNether.client.model.ModelHost3;
import mc.Mitchellbrine.HardcoreNether.client.model.ModelValiantiteSolder;
import mc.Mitchellbrine.HardcoreNether.client.render.RenderAngelicPig;
import mc.Mitchellbrine.HardcoreNether.client.render.RenderBrain;
import mc.Mitchellbrine.HardcoreNether.client.render.RenderDemon;
import mc.Mitchellbrine.HardcoreNether.client.render.RenderDemonicTNT;
import mc.Mitchellbrine.HardcoreNether.client.render.RenderDemonicWolf;
import mc.Mitchellbrine.HardcoreNether.client.render.RenderHand;
import mc.Mitchellbrine.HardcoreNether.client.render.RenderHolyArrow;
import mc.Mitchellbrine.HardcoreNether.client.render.RenderHost;
import mc.Mitchellbrine.HardcoreNether.client.render.RenderHost2;
import mc.Mitchellbrine.HardcoreNether.client.render.RenderHost3;
import mc.Mitchellbrine.HardcoreNether.client.render.RenderSolder;
import mc.Mitchellbrine.HardcoreNether.client.render.hud.HudTickHandler;
import mc.Mitchellbrine.HardcoreNether.client.sound.Sounds;
import mc.Mitchellbrine.HardcoreNether.client.tileentity.TileEntityFrosterRenderer;
import mc.Mitchellbrine.HardcoreNether.client.tileentity.TileEntityHeaterRenderer;
import mc.Mitchellbrine.HardcoreNether.client.tileentity.TileEntityScrollRenderer;
import mc.Mitchellbrine.HardcoreNether.common.core.CommonProxy;
import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import mc.Mitchellbrine.HardcoreNether.common.entity.EntityAngelicPig;
import mc.Mitchellbrine.HardcoreNether.common.entity.EntityBrain;
import mc.Mitchellbrine.HardcoreNether.common.entity.EntityDemon;
import mc.Mitchellbrine.HardcoreNether.common.entity.EntityDemonicTNT;
import mc.Mitchellbrine.HardcoreNether.common.entity.EntityDemonicWolf;
import mc.Mitchellbrine.HardcoreNether.common.entity.EntityHand;
import mc.Mitchellbrine.HardcoreNether.common.entity.EntityHolyArrow;
import mc.Mitchellbrine.HardcoreNether.common.entity.EntityHost;
import mc.Mitchellbrine.HardcoreNether.common.entity.EntityHostStage2;
import mc.Mitchellbrine.HardcoreNether.common.entity.EntityHostStage3;
import mc.Mitchellbrine.HardcoreNether.common.entity.EntitySolder;
import mc.Mitchellbrine.HardcoreNether.common.keybind.AngelKeyBind;
import mc.Mitchellbrine.HardcoreNether.common.keybind.AngelKeyBind2;
import mc.Mitchellbrine.HardcoreNether.common.keybind.AngelKeyBindTick;
import mc.Mitchellbrine.HardcoreNether.common.tileentity.TileEntityFroster;
import mc.Mitchellbrine.HardcoreNether.common.tileentity.TileEntityHeater;
import mc.Mitchellbrine.HardcoreNether.common.tileentity.TileEntityScroll;
import mc.Mitchellbrine.HardcoreNether.dungeon.EntityScientist;
import mc.Mitchellbrine.HardcoreNether.dungeon.ModelScientist;
import mc.Mitchellbrine.HardcoreNether.dungeon.RenderScientist;
import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.common.MinecraftForge;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.KeyBindingRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.TickType;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.common.registry.VillagerRegistry;
import cpw.mods.fml.relauncher.Side;

public class ClientProxy extends CommonProxy
{
	
	private GuiHandler guihandler = new GuiHandler();
	
    public void registerRenderThings()
    {
        RenderingRegistry.registerEntityRenderingHandler(EntityHolyArrow.class, new RenderHolyArrow());
        RenderingRegistry.registerEntityRenderingHandler(EntityBrain.class, new RenderBrain(new ModelBrain(), 0.3F));
        RenderingRegistry.registerEntityRenderingHandler(EntityHand.class, new RenderHand(new ModelHand(), 0.3F));
        RenderingRegistry.registerEntityRenderingHandler(EntityDemonicTNT.class, new RenderDemonicTNT());
        RenderingRegistry.registerEntityRenderingHandler(EntitySolder.class, new RenderSolder(new ModelValiantiteSolder(), 0.3F));
        RenderingRegistry.registerEntityRenderingHandler(EntityDemon.class, new RenderDemon(new ModelDemon(), 0.3F));
        RenderingRegistry.registerEntityRenderingHandler(EntityHost.class, new RenderHost(new ModelHost(), 0.3F));
        RenderingRegistry.registerEntityRenderingHandler(EntityHostStage2.class, new RenderHost2(new ModelHost2(), 0.3F));
        RenderingRegistry.registerEntityRenderingHandler(EntityHostStage3.class, new RenderHost3(new ModelHost3(), 0.3F));
        RenderingRegistry.registerEntityRenderingHandler(EntityScientist.class, new RenderScientist(new ModelScientist(), 0.3F));
        RenderingRegistry.registerEntityRenderingHandler(EntityAngelicPig.class, new RenderAngelicPig(new ModelAngelicPig(), new ModelAngelicPig(), 0.3F));
        RenderingRegistry.registerEntityRenderingHandler(EntityDemonicWolf.class, new RenderDemonicWolf(new ModelDemonicWolf(), new ModelDemonicWolf(), 0.3F));
        ClientRegistry.bindTileEntitySpecialRenderer(TileEntityFroster.class, new TileEntityFrosterRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(TileEntityHeater.class, new TileEntityHeaterRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(TileEntityScroll.class, new TileEntityScrollRenderer());
        KeyBinding[] angelKey = new KeyBinding[] {new KeyBinding("Angelic Flying", 33)};
        KeyBinding[] angelKey2 = new KeyBinding[] {new KeyBinding("Angelic Hovering", 19)};
        KeyBinding[] rosieKey = new KeyBinding[] {new KeyBinding("HC Nether Secret [ON]", 37)};
        KeyBinding[] rosieKey2 = new KeyBinding[] {new KeyBinding("HC Nether Secret [OFF]", 38)};
        boolean[] repeat = new boolean[] {false};
        KeyBindingRegistry.registerKeyBinding(new AngelKeyBind(angelKey, repeat));
        KeyBindingRegistry.registerKeyBinding(new AngelKeyBind2(angelKey2, repeat));
        KeyBindingRegistry.registerKeyBinding(new RosieKeyBind(rosieKey, repeat));
        KeyBindingRegistry.registerKeyBinding(new RosieKeyBind2(rosieKey2, repeat));
        
        NetworkRegistry.instance().registerGuiHandler(MainMod.instance, this.guihandler);
    }

    public int addArmor(String armor)
    {
        return RenderingRegistry.addNewArmourRendererPrefix(armor);
    }

    public void registerVillagerStuff()
    {
        VillagerRegistry.instance().registerVillagerSkin(20, MainMod.corruptVillagerSkin);
    }

    public void registerTickHandlers()
    {
        TickRegistry.registerTickHandler(new HudTickHandler(), Side.CLIENT);
        TickRegistry.registerTickHandler(new AngelKeyBindTick(EnumSet.of(TickType.PLAYER)), Side.CLIENT);
        TickRegistry.registerTickHandler(new TickHandler(), Side.SERVER);
    }

    public void registerSounds()
    {
        MinecraftForge.EVENT_BUS.register(new Sounds());
        System.out.println("ClientProxy Sounds Loaded");
    }
}

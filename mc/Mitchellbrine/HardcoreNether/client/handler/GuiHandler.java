package mc.Mitchellbrine.HardcoreNether.client.handler;

import cpw.mods.fml.common.network.IGuiHandler;
import mc.Mitchellbrine.HardcoreNether.client.container.ContainerTutFurnace;
import mc.Mitchellbrine.HardcoreNether.client.gui.GuiSecret;
import mc.Mitchellbrine.HardcoreNether.client.gui.TutGuiFurnace;
import mc.Mitchellbrine.HardcoreNether.common.tileentity.TileEntityTutFurnace;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class GuiHandler implements IGuiHandler
{
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
    {
        TileEntity tile_entity = world.getBlockTileEntity(x, y, z);
        return tile_entity instanceof TileEntityTutFurnace && ID == 0 ? new ContainerTutFurnace(player.inventory, (TileEntityTutFurnace)tile_entity) : null;
    }

    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
    {
        TileEntity tile_entity = world.getBlockTileEntity(x, y, z);
        return tile_entity instanceof TileEntityTutFurnace && ID == 0 ? new TutGuiFurnace(player.inventory, (TileEntityTutFurnace)tile_entity) : (ID == 1 ? new GuiSecret() : null);
    }
}

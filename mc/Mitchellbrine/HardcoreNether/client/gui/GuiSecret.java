package mc.Mitchellbrine.HardcoreNether.client.gui;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;

public class GuiSecret extends GuiScreen
{
    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        this.buttonList.clear();
        this.buttonList.add(new GuiButton(1, this.width / 2 + 2, this.height / 2 + 20, 98, 20, "Turn on Secret"));
        this.buttonList.add(new GuiButton(2, this.width / 2 - 100, this.height / 2 + 20, 98, 20, "Turn off Secret"));
    }

    /**
     * Fired when a control is clicked. This is the equivalent of ActionListener.actionPerformed(ActionEvent e).
     */
    protected void actionPerformed(GuiButton guibutton)
    {
        if (guibutton.id == 1)
        {
            MainMod.rosieTextures = true;
        }

        if (guibutton.id == 2)
        {
            MainMod.rosieTextures = false;
        }
    }

    /**
     * Returns true if this GUI should pause the game when it is displayed in single-player
     */
    public boolean doesGuiPauseGame()
    {
        return false;
    }

    /**
     * Called when the screen is unloaded. Used to disable keyboard repeat events
     */
    public void onGuiClosed() {}

    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int i, int j, float f)
    {
        this.drawDefaultBackground();
        this.drawGradientRect(20, 20, this.width - 20, this.height - 20, 1615855616, -1602211792);
        drawRect(60, 60, this.width - 60, this.height - 60, -16776961);
        this.drawString(this.fontRenderer, "Rain Controller", this.width / 2 - 100, this.height / 2 - 40, 13421772);
        this.drawCenteredString(this.fontRenderer, "Weather Controller", this.width / 2, 45, 16777215);
        super.drawScreen(i, j, f);
    }
}

package mc.Mitchellbrine.HardcoreNether.client.sound;

import net.minecraftforge.client.event.sound.SoundLoadEvent;
import net.minecraftforge.event.ForgeSubscribe;

public class Sounds
{
    @ForgeSubscribe
    public void onSound(SoundLoadEvent event)
    {
        try
        {
            event.manager.addSound("hardcoreNether:mob/brain/live1.ogg");
            event.manager.addSound("hardcoreNether:mob/brain/live2.ogg");
            event.manager.addSound("hardcoreNether:mob/hand/live1.ogg");
            event.manager.addSound("hardcoreNether:mob/hand/live2.ogg");
            event.manager.addSound("hardcoreNether:mob/host/live1.ogg");
            event.manager.addSound("hardcoreNether:mob/host/live2.ogg");
            event.manager.addSound("hardcoreNether:mob/scientist/living.ogg");
            event.manager.addSound("hardcoreNether:machine/wings.ogg");
            event.manager.addSound("hardcoreNether:ost/demons.ogg");
            event.manager.addSound("hardcoreNether:ost/electricity.ogg");
            event.manager.addStreaming("hardcoreNether:ost/demons.ogg");
            event.manager.addStreaming("hardcoreNether:ost/electricity.ogg");
            event.manager.addStreaming("Demons.ogg");
            event.manager.addStreaming("Electricity.ogg");
            event.manager.addStreaming("Ambush.ogg");
            System.out.println("HC Nether Sounds Loaded");
        }
        catch (Exception var3)
        {
            System.err.println("Sounds error");
        }
    }
}

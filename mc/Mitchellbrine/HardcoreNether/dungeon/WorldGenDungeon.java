package mc.Mitchellbrine.HardcoreNether.dungeon;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import mc.Mitchellbrine.HardcoreNether.common.core.MainMod;
import net.minecraft.block.Block;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;

public class WorldGenDungeon extends WorldGenerator
{
    public List<Integer> whitelist;
    public int xDim;
    public int yDim;
    public int zDim;
    public int solids;
    public int nonsolids;

    public WorldGenDungeon()
    {
        this.whitelist = this.getListOnArray(new int[] {MainMod.demonicStone.blockID});
        this.xDim = 9;
        this.yDim = 15;
        this.zDim = 9;
        this.solids = 80;
        this.nonsolids = 60;
    }

    public List<Integer> getListOnArray(int ... ints)
    {
        ArrayList intList = new ArrayList();
        int[] arr$ = ints;
        int len$ = ints.length;

        for (int i$ = 0; i$ < len$; ++i$)
        {
            int i = arr$[i$];
            intList.add(Integer.valueOf(i));
        }

        return intList;
    }

    public boolean checkSpawn(World world, int x, int y, int z)
    {
        int solidBlocksInPlatform = 0;
        int freeBlocksInSpace = 0;

        for (int i = 0; i < this.xDim; ++i)
        {
            for (int k = 0; k < this.zDim; ++k)
            {
                if (world.getBlockId(x + i, y - 1, z + k) != 0 && this.whitelist.contains(Integer.valueOf(world.getBlockId(x + i, y - 1, z + k))) && !Block.blocksList[world.getBlockId(x + i, y - 1, z + k)].blockMaterial.isReplaceable())
                {
                    ++solidBlocksInPlatform;
                }

                for (int j = 0; j < this.yDim; ++j)
                {
                    if (world.isAirBlock(x + i, y + j, z + k) || Block.blocksList[world.getBlockId(x + i, y + j, z + k)].blockMaterial.isReplaceable())
                    {
                        ++freeBlocksInSpace;
                    }
                }
            }
        }

        return Math.round((float)(freeBlocksInSpace / (this.xDim * this.zDim * this.yDim)) * 100.0F) > this.solids && Math.round((float)(solidBlocksInPlatform / this.xDim * this.zDim) * 100.0F) > this.nonsolids;
    }

    public boolean generate(World par1World, Random par2Random, int par3, int par4, int par5)
    {
        if (this.checkSpawn(par1World, par3, par4, par5))
        {
            (new dungeon1()).generate(par1World, par2Random, par3, par4, par5);
            System.out.println("GENERATED DUNGEON1!");
            return true;
        }
        else
        {
            return false;
        }
    }
}
